#include "AjustmentInfoScene.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"	//シーン切り替えを行う


#include "Controller.h"
#include "FishMetaAI.h"
#include "Ground.h"

#include "NumberPlate.h"
#include "ESC.h"
#include "Help.h"
#include "HelpImage.h"

//コンストラクタ
AjustmentInfoScene::AjustmentInfoScene(GameObject * parent)
	: GameObject(parent, "AjustmentInfoScene")	,
	currentFlag(true),
	controllerPosZ(-5.0f),
	controllerMoveX(5.0f)
{
	for (int i = 0; i < CHILDREN_MAX; i++)
	{
		pGameObject[i] = nullptr;
	}
}

//初期化
void AjustmentInfoScene::Initialize()
{


	pGameObject[CONTROLLER] =  (Controller*)Instantiate<Controller>(this);
	pGameObject[CONTROLLER]->transform_.position_ = XMVectorSet(0.0f, 0.0f, controllerPosZ, 0.0f);
	Controller* pController = (Controller*)pGameObject[CONTROLLER];
	pController->SetMaxXAndMinX(controllerMoveX * 9, 0);	//０の地点から、5.0の横幅を持つモデルが１０個並んでいる。であれば、横幅の移動値の上限は、、
										//ちなみに、１つ目のモデルは０の地点から横に2.5f そして,２つ目のモデルは2.5fの地点から7.5f(5.0fの横幅なので)= 2.5f + 5.0f * 9 = 45.7f = 45.7の地点から横に移動できなくすれば、ちょうど10個目のモデルの前で止まるように設定できる


	//魚生成と
	//魚操作を担うクラス
	pGameObject[FISH_META_AI] = Instantiate<FishMetaAI>(this);
	
	//関数のポインタを渡して、その関数のポインタだけの情報をクラスに渡したかった。
	//だが、関数のポインタをクラス側で受け取るには、
	//AjustmentInfoSceneの情報を知っていないといけない。（AjustmentInfoScene:：*functionPointa）として受け取らなければいけない。
		//これだと、結局AjustmentInfoSceneをインクルードしないといけないので、
		//私がやりたかった、インクルードなしで、　関数にアクセスするというやり方ができなくなる。
		//つまり、AjustmentInfoSceneの関数を使うには、どうであっても、クラスはインクルードしなくてはいけない。
	FishMetaAI* pFishMetaAI = (FishMetaAI*)pGameObject[FISH_META_AI];
	//pFishMetaAI->SetFunctionPointa(ChangeControllerPos);
	pFishMetaAI->SetAjustmentInfoScene(this);
		//自身のポインタを渡して初めて、InitDialogを行う（Set関数内ですでに行った。）



	pGameObject[GROUND] = Instantiate<Ground>(this);

	pGameObject[NUMBERPLATE] = Instantiate<NumberPlate>(this);
	
	pGameObject[ESC_] = Instantiate<ESC>(this);
	pGameObject[HELP] = Instantiate<Help>(this);

	pGameObject[HELP_IMAGE] = Instantiate<HelpImage>(this);
	HelpImage* pHelpImage = (HelpImage*)pGameObject[HELP_IMAGE];
	pHelpImage->Load("Assets/2DTexture/FishSceneInfo_1.jpg");
	//フラグの上下の設定
		//フラグを下す
	ChangeStatus(false, pHelpImage);

	




}

//更新
void AjustmentInfoScene::Update()
{
	//地面選択シーンに戻す
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_SELECT_GROUND);
	
	}

	//変更した内容の保存を行うのかのウィンドウを表示


	//ヘルプ画像の表示
	if (Input::IsKeyDown(DIK_H))
	{
		ChangeChildStatus();
	}





}

//描画
void AjustmentInfoScene::Draw()
{
}

//開放
void AjustmentInfoScene::Release()
{
}

void AjustmentInfoScene::ChangeStatus(bool flag, GameObject* pGameObject)
{
	if (flag)
	{
		pGameObject->EnableUpdate();
		pGameObject->EnableDraw();
	}
	else
	{

		pGameObject->DisableUpdate();
		pGameObject->DisableDraw();
	}
}

void AjustmentInfoScene::ChangeControllerPos(int type)
{
	//引数にてもらったタイプを使用して、
	//移動X値を設定

	//type = 0
	//vecX =　type * controllerMoveX;

	//type = 1以降
	//vecX =　type * controllerMoveX + 2.5f;

	//コントローラークラスが作成前に呼ばれてしまうこともあるかもしれないので、
	if (pGameObject[CONTROLLER] != nullptr)
	{
		if (type == 0)
		{
			//X座標を0にする
			pGameObject[CONTROLLER]->
				transform_.position_.vecX = type;
		}
		else if (type > -1 && type < 10)
		{
			//X座標を にする
			pGameObject[CONTROLLER]->
				transform_.position_.vecX =
				type * controllerMoveX;

		}
	}

}

void AjustmentInfoScene::ChangeChildStatus()
{
	ChangeStatus(currentFlag, pGameObject[HELP_IMAGE]);

	if (currentFlag)
	{
		currentFlag = false;
	}
	else
	{
		currentFlag = true;
	}

	for (int i = 0; i < CHILDREN_MAX; i++)
	{
		if (i != HELP_IMAGE)
		{
			ChangeStatus(currentFlag, pGameObject[i]);
		}
	}

}