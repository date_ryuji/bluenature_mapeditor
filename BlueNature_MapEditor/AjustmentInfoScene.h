#pragma once
#include "Engine/GameObject.h"




//■■シーンを管理するクラス
class AjustmentInfoScene : public GameObject
{
	enum CHILDREN
	{
		CONTROLLER , 
		FISH_META_AI,
		GROUND,
		NUMBERPLATE,
		ESC_,
		HELP,
		HELP_IMAGE,

		CHILDREN_MAX,
	};


	GameObject* pGameObject[CHILDREN_MAX];

	bool currentFlag;

	//ControllerのZ値（const値）
	const float controllerPosZ;

	//ControllerのX移動値（0番目の魚に注目したいときには、X値＊０とする）
	const float controllerMoveX;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	AjustmentInfoScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//シーン内の子供オブジェクトの、
	//Update、Drawのフラグを下す、上げる
	void ChangeChildStatus();

	//特定オブジェクトのステータス（Update、Drawのフラグを下す、上げる）を引数のflagにて、判断する
	void ChangeStatus(bool flag, GameObject* pGameObject);

	//Controllerクラスの
	//位置を変更する
		//引数にてもらった、番号（０〜１０）をもとにコントローラークラスのX値を変更する
	void ChangeControllerPos(int type);


};