#include "BlueTang.h"

//コンストラクタ
BlueTang::BlueTang(GameObject * parent)
	: FishBase::FishBase(parent, "BlueTang")
{


}

BlueTang::~BlueTang()
{
	BaseRelease();

}

//初期化
void BlueTang::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();


}

//更新
void BlueTang::Update()
{
}

//開放
void BlueTang::Release()
{
}