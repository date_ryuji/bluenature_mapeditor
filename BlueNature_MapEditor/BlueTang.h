#pragma once

#include "FishBase.h"

//■■シーンを管理するクラス
class BlueTang : public FishBase
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	BlueTang(GameObject* parent);
	~BlueTang() override;

	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//解放
	void Release() override;
};