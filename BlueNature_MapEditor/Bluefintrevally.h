#pragma once
#include "Engine/GameObject.h"

#include "FishBase.h"

//■■シーンを管理するクラス
class Bluefintrevally : public FishBase
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Bluefintrevally(GameObject* parent);
	~Bluefintrevally() override;

	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//解放
	void Release() override;
};