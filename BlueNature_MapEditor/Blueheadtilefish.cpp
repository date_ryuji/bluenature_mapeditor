#include "Blueheadtilefish.h"

//コンストラクタ
Blueheadtilefish::Blueheadtilefish(GameObject * parent)
	: FishBase::FishBase(parent, "Blueheadtilefish")
{


}

Blueheadtilefish::~Blueheadtilefish()
{
	BaseRelease();
}

//初期化
void Blueheadtilefish::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();

}

//更新
void Blueheadtilefish::Update()
{
}

//開放
void Blueheadtilefish::Release()
{
}