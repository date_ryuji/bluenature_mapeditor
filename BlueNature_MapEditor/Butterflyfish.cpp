#include "Butterflyfish.h"

//コンストラクタ
Butterflyfish::Butterflyfish(GameObject * parent)
	: FishBase::FishBase(parent, "Butterflyfish")
{


}

Butterflyfish::~Butterflyfish()
{
	//３Dポリゴン用の動的確保したポインタの解放
	BaseRelease();
}

//初期化
void Butterflyfish::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();


}

//更新
void Butterflyfish::Update()
{
}

//開放
void Butterflyfish::Release()
{

}