#pragma once

#include "FishBase.h"

//■■シーンを管理するクラス
class Butterflyfish : public FishBase
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Butterflyfish(GameObject* parent);

	~Butterflyfish() override;

	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//解放
	void Release() override;
};