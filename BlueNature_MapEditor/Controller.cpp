#include "Controller.h"
#include "Engine/Camera.h"
#include "Engine/Input.h"

//コンストラクタ
Controller::Controller(GameObject * parent)
	:GameObject(parent, "Controller"),
	maxX_(9999.0f),minX_(-9999.0f)
{
}

//デストラクタ
Controller::~Controller()
{
}

//初期化
void Controller::Initialize()
{
	//モデルはいらない

}

//更新
void Controller::Update()
{
	
	//横向きのベクトル
	XMVECTOR right = XMVectorSet(0.3f, 0.0f, 0.0f, 0.0f);	//右に進むベクトル


	if (Input::IsKey(DIK_D))
	{
		//上限値を超えていない間進む
		if (transform_.position_.vecX < maxX_)
		{
			//右に進むので、
			//＋X方向に進むベクトルを、現在の回転具合で回転させたベクトルを使用

			transform_.position_ += right;
		}
	}
															
	if (Input::IsKey(DIK_A))
	{
		if (transform_.position_.vecX > minX_)
		{
			transform_.position_ -= right;
		}
	}



//カメラの追尾
	{

		//カメラ位置を確定
		XMVECTOR moveVec = XMVectorSet(0.0f, 0.0f, -10.0f, 0.0f);

		//コントローラー位置と回転行列を掛けたカメラ位置ベクトルを足す
		Camera::SetPosition(
			transform_.position_ + moveVec);

		//カメラの焦点
		Camera::SetTarget(transform_.position_);

	}

	//カメラのズーム
	//カメラの視錐台の大きさの変更
		//カメラの画角を変更→プロジェクション行列に与える画角の角度の変更
	if (Input::IsKey(DIK_RSHIFT))
	{
		//ズームアウト
		Camera::AddAngleForZoomOut(0.01f);
	}
	if (Input::IsKey(DIK_RETURN))
	{
		//ズームイン
		Camera::SubAngleForZoomIn(0.01f);
	}
	
	


}

//描画
void Controller::Draw()
{
	//モデルは読み込まないのでDrawもなし
}

//開放
void Controller::Release()
{
}

//移動の上限、下限X値の設定
void Controller::SetMaxXAndMinX(float maxX, float minX)
{
	maxX_ = maxX;
	minX_ = minX;
}
