#pragma once
#include "Engine/GameObject.h"
//Controllerを管理するクラス
class Controller : public GameObject
{
	int hModel_;

	//移動の上限、下限X値
	float maxX_;
	float minX_;

public:
	//コンストラクタ
	Controller(GameObject* parent);

	//デストラクタ
	~Controller();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//移動の上限、下限X値の設定
	void SetMaxXAndMinX(float maxX , float minX);

};