#include "Crownfish.h"

//コンストラクタ
Crownfish::Crownfish(GameObject * parent)
	: FishBase::FishBase(parent, "Crownfish")
{


}

Crownfish::~Crownfish()
{
	BaseRelease();
}

//初期化
void Crownfish::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();


}

//更新
void Crownfish::Update()
{
}

//開放
void Crownfish::Release()
{
}