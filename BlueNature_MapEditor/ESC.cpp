#include "ESC.h"
#include "Engine/Image.h"

//コンストラクタ
ESC::ESC(GameObject * parent)
	: GameObject(parent, "ESC"),
	hImage_(-1)
{
}

//初期化
void ESC::Initialize()
{


	//地面番号の値をファイルのパスにする
	std::string fileName = "Assets/2DTexture/ESC.jpg";


	hImage_ = Image::Load(fileName);
	assert(hImage_ > -1);	//-1より大きいはずだ


	transform_.position_ = XMVectorSet(1.0f - 0.58f, 1.0f - 0.18f, 0, 0);
	transform_.scale_ = XMVectorSet(0.5f, 0.5f, 0.1f, 0);


}

//更新
void ESC::Update()
{



}

//描画
void ESC::Draw()
{
	Image::SetTransform(hImage_, transform_);
	Image::Draw(hImage_);

}

//開放
void ESC::Release()
{
}