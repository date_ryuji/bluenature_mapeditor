#include "BoxCollider.h"
#include "SphereCollider.h"
#include "OrientedBoxCollider.h"
#include "Model.h"
#include "GameObject.h"


//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
BoxCollider::BoxCollider(XMVECTOR basePos, XMVECTOR size)
{
	center_ = basePos;
	size_ = size;

	//XMVECTORからXMFLOAT3にそれぞれ変換
	XMStoreFloat3(&colliderCenter_, basePos);
	XMStoreFloat3(&colliderSize_, size);

	pCollider_ = std::make_unique<BoundingBox>(colliderCenter_, colliderSize_);

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	hDebugModel_ = Model::Load("DebugCollision/boxCollider.fbx");
#endif
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool BoxCollider::IsHit(Collider* target)
{
	if (auto* targetBox = dynamic_cast<BoxCollider*>(target))
		return IsHitSame<BoxCollider>(this, targetBox);

	if (auto* targetSphere = dynamic_cast<SphereCollider*>(target))
		return IsHitWrong<BoxCollider,SphereCollider>(this, targetSphere);

	if (auto* targetOriBox = dynamic_cast<OrientedBoxCollider*>(target))
		return IsHitWrong<BoxCollider, OrientedBoxCollider>(this, targetOriBox);
	
	return false;
}

void BoxCollider::UpdateState()
{
	XMVECTOR pos = center_ + pGameObject_->GetPosition();
	XMStoreFloat3(&pCollider_->Center, pos);
}

void BoxCollider::Rotation() 
{
	XMMATRIX mat = XMMatrixIdentity();
	//pCollider_->Transform(pCollider_, mat);
}
