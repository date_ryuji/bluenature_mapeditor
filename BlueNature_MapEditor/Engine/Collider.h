#pragma once
#include <d3d11.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <memory>


using namespace DirectX;

//クラスの前方宣言
class GameObject;
class BoxCollider;
class SphereCollider;
class OrientedBoxCollider;


//-----------------------------------------------------------
//あたり判定を管理するクラス
//-----------------------------------------------------------
class Collider
{
	//それぞれのクラスのprivateメンバにアクセスできるようにする
	friend class BoxCollider;
	friend class SphereCollider;
	friend class OrientedBoxCollider;

protected:
	GameObject*		pGameObject_;	//この判定をつけたゲームオブジェクト
	XMVECTOR		center_;		//中心位置（ゲームオブジェクトの原点から見た位置）
	XMVECTOR		size_;			//判定サイズ（幅、高さ、奥行き）
	XMVECTOR		orient_;		//回転
	int				hDebugModel_;	//デバッグ表示用のモデルのID

	
	XMFLOAT3 colliderCenter_;	//コライダーの中心位置
	XMFLOAT3 colliderSize_;		//コライダーのサイズ
	XMFLOAT4 colliderOrient_;

	//同じ種類のコライダー同士の衝突判定
	//params : 判定対象のコライダー二つ
	//return : 衝突しているかどうか
	template<class T>
	bool IsHitSame(T* collA, T* collB)
	{
		return collA->pCollider_->Intersects(*collB->pCollider_);
	}

	//違う種類同士のコライダー同士の衝突判定
	//params : 判定対象のコライダー二つ
	//return : 衝突しているかどうか
	template<class T1, class T2>
	bool IsHitWrong(T1* collA, T2* collB) 
	{
		return collA->pCollider_->Intersects(*collB->pCollider_);
	}

public:
	//コンストラクタ
	Collider();

	//デストラクタ
	virtual ~Collider();

	//接触判定（継承先のSphereColliderかBoxColliderでオーバーライド）
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	virtual bool IsHit(Collider* target) = 0;

	
	//テスト表示用の枠を描画
	//引数：position	位置
	void Draw(XMVECTOR position,XMVECTOR rotate);

	void Draw(XMVECTOR position);

	//セッター
	void SetGameObject(GameObject* gameObject) { pGameObject_ = gameObject; }

	//コライダーのステータス(中心、回転)を更新
	//params : NULL
	//return : NULL
	virtual void UpdateState() = 0;


};

