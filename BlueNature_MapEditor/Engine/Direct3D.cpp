#include <d3dcompiler.h>
#include "Direct3D.h"
#include "Camera.h"


//変数
namespace Direct3D

{
	//クラスのオブジェクト作成
//オブジェクトを作成するときは、ポインタで作成すると決めているので、ポインタで
	ID3D11Device*           pDevice = nullptr;		//デバイス
													//監督	
	ID3D11DeviceContext*    pContext = nullptr;		//デバイスコンテキスト
													//絵を描く人
	IDXGISwapChain*         pSwapChain = nullptr;		//スワップチェイン
														//裏で書いた紙と、表の紙を入れ替える
	ID3D11RenderTargetView* pRenderTargetView = nullptr;	//レンダーターゲットビュー
	
	ID3D11Texture2D*	pDepthStencil;			//深度ステンシル（Zバッファ法（隠面消去を行う））
												//ウィンドウに表示する描画を、カメラから、前面にあるものは前面に塗り、後面にあるものは、塗らずに前面の色を残す処理（そのようにすれば、カメラから全面の色だけが残り、後面の色は棒がされなくなる）
	ID3D11DepthStencilView* pDepthStencilView;		//深度ステンシルビュー（深度ステンシルとの仲人）

															
//デバイスコンテキストとレンダーターゲットとの橋渡し		
	//→Main.cppにて、使うものを、この中で宣言

	//全体に応じて、使用するものなら、ヘッダに、（他クラスにおいても使用するものなどは、ヘッダで、）
	//この中でのみ使用するなら、cppにのみで使用するなら、ここで宣言

	//cpp内で、名前空間に、以上のポインタを追加している


	int scrWidth = 0;
	int scrHeight = 0;
	

	//シェーダーをコンパイルする
	//コンパイルしたものを入れるためのポインタ
		//２D用、３D用のために配列を使用する
		//→だが、それだったら、一つの構造体にして、その配列にしたほうがきれいに見える
	struct ShaderBundle {

		ID3D11VertexShader*	pVertexShader = nullptr;	//頂点シェーダー
		ID3D11PixelShader*	pPixelShader = nullptr;		//ピクセルシェーダー
		ID3D11InputLayout*	pVertexLayout = nullptr;	//頂点インプットレイアウト	
		ID3D11RasterizerState*	pRasterizerState = nullptr;	//ラスタライザー
	};
	ShaderBundle shaderBundle[SHADER_MAX];
			//構造体の変数を取得して、　shaderBundle[0]の頂点シェーダと複数取得ができた
				//→構造体にてまとまっていたほうが、それぞれ複数取得するのでまとまっていてわかりやすい
				//+名前で管理したいので、enumの名前で２D,3Dで指定できるように

	//構造体内のポインタを使用
			//構造体自体はぽいんたではないので　構造体-> メソッドではない
	//shaderBundle[0]内の ポインタpVertexShaderを　アドレスで渡したいときは、
			//


}

//同じ名前で、使ってしまっているが、
//同じ名前で、ネームスペースをとれば、すでに存在するネームスペースに統合されるので、問題ない


//クラスとの違い
//→変数を宣言したときに、初期化
//コンストラクタ、デストラクタが存在しない


//いかに、namespaceの、関数の中身を書く→これらを見ると、クラスと変わらない
//Main.cppに書いていた、DirectX関係の処理をこの中にまとめる

//初期化
//★Main.cppによって、初期化でしていたもの、〜の設定して、（最初にしなくてはいけないもの）
HRESULT Direct3D::Initialize(int winW, int winH, HWND hWnd)

{
	//ウィンドウスクリーンのサイズを設定
	scrWidth = winW;
	scrHeight = winH;



	/*******ウィンドウ表示したものに、***************************************************************************/
	///////////////////////////いろいろ準備するための設定///////////////////////////////
	//いろいろな設定項目をまとめた構造体
	DXGI_SWAP_CHAIN_DESC scDesc;

	//とりあえず全部0
	ZeroMemory(&scDesc, sizeof(scDesc));

	//描画先のフォーマット（画面のサイズ→ウィンドウサイズを画面のサイズにするので、そのまま）
	//バックバッファのサイズを指定している。→これを小さくすると→描画のウィンドウサイズの画面の大きさに引き伸ばされる
	scDesc.BufferDesc.Width = winW;		//画面幅
	scDesc.BufferDesc.Height = winH;	//画面高さ
	scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 何色使えるか
								//８bit	＝　２５５　（RGBそれぞれ使える領域）
								//０〜２５５色（Rだけで、２５６色）→フルカラー

	//FPS（1/60秒に1回）
	scDesc.BufferDesc.RefreshRate.Numerator = 60;
	scDesc.BufferDesc.RefreshRate.Denominator = 1;

	//その他
	scDesc.Windowed = TRUE;			//ウィンドウモードかフルスクリーンか
									//Trueにすると、ウィンドウのサイズのみ、Falseにするとフルスクリーン
	scDesc.OutputWindow = hWnd;		//ウィンドウハンドル
	scDesc.BufferCount = 1;			//バックバッファの枚数（裏で絵を描くものの枚数　１枚で）
	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//バックバッファの使い道＝画面に描画するために
	scDesc.SampleDesc.Count = 1;		//MSAA（アンチエイリアス）の設定（ギザギザを滑らかに）
	scDesc.SampleDesc.Quality = 0;		//　〃


	////////////////上記設定をもとにデバイス、コンテキスト、スワップチェインを作成////////////////////////
	D3D_FEATURE_LEVEL level;

	//ポインタのクラスオブジェクトを生成しなくてはいけない
		//newの代わりに、DirectXで用意されているもの↓


	HRESULT hr;	//結果を入れる変数（HRESULT型）//if文のFAILE()の（）の中に入れるには、長すぎると思ったため、見やすく変数でとった
	hr = 
	D3D11CreateDeviceAndSwapChain(
		nullptr,				// どのビデオアダプタを使用するか？既定ならばnullptrで
								//グラフィックボードどれを使うか→nullptrでデフォルト

		D3D_DRIVER_TYPE_HARDWARE,		// ドライバのタイプを渡す。ふつうはHARDWARE
										//ハードウェアに直接指定するが、→対応していないハードウェアにやろうとすると→、対応してなかったら、ソフトウェアにする
										//→ハードウェアで対応しなかったあ、ソフトウェアにすることができるが、→それだと遅い（なので、）

		nullptr,				// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
		0,					// 何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
		nullptr,				// デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
		0,					// 上の引数でレベルを何個指定したか
		D3D11_SDK_VERSION,			// SDKのバージョン。必ずこの値

		//出来上がったものを、オブジェクトに適用
		&scDesc,				// 上でいろいろ設定した構造体
		&pSwapChain,				// 無事完成したSwapChainのアドレスが返ってくる
		&pDevice,				// 無事完成したDeviceアドレスが返ってくる
		&level,					// 無事完成したDevice、Contextのレベルが返ってくる
		&pContext);				// 無事完成したContextのアドレスが返ってくる
	
	if (FAILED(hr))
	{
		//エラーのメッセージボックスを返す（Windows機能）
		MessageBox(nullptr, "デバイス、コンテキスト、スワップチェイン作成失敗", "エラー", MB_OK);
		//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
		return E_FAIL;		//呼び出し元の関数へエラーを送る
	}


	///////////////////////////レンダーターゲットビュー作成///////////////////////////////
	//スワップチェーンからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
	ID3D11Texture2D* pBackBuffer;

	if (FAILED(pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
	{
		//エラーのメッセージボックスを返す（Windows機能）
		MessageBox(nullptr, "スワップチェインのバッファーの取得失敗", "エラー", MB_OK);
		//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
		return E_FAIL;		//呼び出し元の関数へエラーを送る
	
	}
	//pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

	//仮の紙を用意して、　対応するレンダーターゲットを設定して・・・
	//レンダーターゲットビューを作成
	if (FAILED(pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView)))
	{
		//エラーのメッセージボックスを返す（Windows機能）
		MessageBox(nullptr, "レンダーターゲットビュー作成失敗", "エラー", MB_OK);
		//上記のHRESULT型の戻り値が、失敗（E_~）を返したなら
		return E_FAIL;		//呼び出し元の関数へエラーを送る
	}
	//pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView);

	//一時的にバックバッファを取得しただけなので解放
	pBackBuffer->Release();


	///////////////////////////ビューポート（描画範囲）設定///////////////////////////////
	//遠くのもの小さく見えて、近くのもの大きく見える
	//視野で、見える範囲が、広がっていく
		//→それを広がっているものを、視点から、前に長方形に切り取ったものとしてみさせる
			//→なので、遠くのものは小さく見える

	//レンダリング結果を表示する範囲
	D3D11_VIEWPORT vp;
	vp.Width = (float)winW;	//幅
	vp.Height = (float)winH;//高さ
	vp.MinDepth = 0.0f;	//手前
	vp.MaxDepth = 1.0f;	//奥
						//長方形に変形したものを、１ｍの範囲にギュッとつぶした

	vp.TopLeftX = 0;	//左
	vp.TopLeftY = 0;	//上


	//深度ステンシルビューの作成(Zバッファ（前のものは前、後ろのものは後ろに描画）)
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = winW;
	descDepth.Height = winH;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
	pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView);



	//データを画面に描画するための一通りの設定（パイプライン）
	pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // データの入力種類を指定
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);            // 描画先を設定
															//第３引数に深度ステンシルを入れて、Zバッファ（いんめん消去）を渡す
	pContext->RSSetViewports(1, &vp);



	//シェーダーの準備
	if (FAILED(InitShader()))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "シェーダの初期化の失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	//InitShader();
	//エラーが帰ってくるとする→すると、このInitShaderを呼び出している関数自体は、
		//→一番上の関数ではない→Direct3DのInitializeを読んでいる関数がいるはず→なので、　その呼び出し元に、またエラーと送る
		//呼び出し元でエラー処理してもらう（ここでは、とにかくエラーですと送る）


	//カメラの初期化
	Camera::Initialize();	


	//カメラに位置、焦点を渡してやって、
	//それで、行列を作ってもらう
	//カメラの位置を送る
	//Camera::SetPosition(XMVectorSet(1, 0, 0, 0));	//カメラの位置をセット
	//この方式と、もう一つ、ｘ、ｙ、ｚだけを送り、それを受け取ってもらう関数も欲しい



	return S_OK;
}


//シェーダー準備
//シェーダー３D,２D　どちらにも対応できるように
	//ラスタライザなど、が、２Dにおいても、特に設定はひつようないが、　
	//同じものでも、２D用３D用を作っておく（構造体）
HRESULT Direct3D::InitShader()

{
	InitShader3D();
	InitShader2D();
	

	//それぞれをデバイスコンテキストにセット
	//こうゆう、シェーダにしたよと送る
	//デバイスコンテキスト→書き込む人（バックバッファに）

	//使用するシェーダーを選択している
	//★シェーダを、２Dなら、２D、３Dなら、３Dを選択したい
	//シェーダは、あらかじめコンパイルしたい、しておきたい
		//その都度、以下の送るシェーダーのポインタで、使うものを２D,3Dで送ればいい


	//シェーダーを与えるとき、
		//どっちを使うかを、わからないので、　
		//→２Dを使うか、３Dを使うかを選べるように、２つの関数化にする


	//どちらを使うのかを、判断するために、
		//SHADER_3Dのところに、3Dか、２D化、を判断できる値を入れるようにしたい
	//SHADER_TYPE type;	//Typeという、SHADER_TYPEという、enumの値（unsigend int）の値を取得
						//値は、unsiged intだが、　enumの値であれば、何を表すunsiged intなのかが、わかりやすくする

	//３D,2Dを、決めるために、どちらを扱うかを送り、
		//使用するシェーダーを、コンテキストのポインタにセット
	SetShaderBundle(SHADER_3D);



	//エラー処理なし
	return S_OK;

}


//シェーダーポインタに、３D,２D　シェーダーを選んでセット
void Direct3D::SetShaderBundle(SHADER_TYPE type)
{
	pContext->VSSetShader(shaderBundle[type].pVertexShader, NULL, 0);	//頂点シェーダー

	pContext->PSSetShader(shaderBundle[type].pPixelShader, NULL, 0);	//ピクセルシェーダー

	pContext->IASetInputLayout(shaderBundle[type].pVertexLayout);	//頂点インプットレイアウト

	pContext->RSSetState(shaderBundle[type].pRasterizerState);		//ラスタライザー
}

//３Dシェーダーの初期化
HRESULT Direct3D::InitShader3D()
{
	// 頂点シェーダの作成（コンパイル）
	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム

	ID3DBlob *pCompileVS = nullptr;	//コンパイル


	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "３Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
	//グラボのバージョンは、特に難しいことをしないのであれば、小さい値でOK
	//L"Simple~"	Lは？
	//→メモにて
	//
	assert(pCompileVS != nullptr);
	//上記のファイル読み込みも、HRESULTである
	//→この時、HRESULTでも、エラーを返して、メッセージボックスが出て、
	//あさーとでもエラーのボックスを出してくる

	//アサーとは、デバック用と考えて、最終的にはなくしていく
	//アサーとは、プログラマのミスで起こりえる、ファイル名チェックなどのものは、アサーと
	//→プレイヤーが、ファイルを消してしまったなどの問題がある→それは、エラー処理で？
	//エラー処理はif分なので、if文を入れれば入れるほど、重くなる
	//だから、→すべてにエラー処理を入れればいいのかといえば、そうでもないとは思うが、
	//→現段階では、マメなのかと思われるかも？→とにかく、HRESULT物は、エラー処理をする


	//pVertexShaderを作成（頂点シェーダー）
	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pVertexShader)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "３Dコンパイルしたシェーダを代入失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &pVertexShader);
	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）

	//★★頂点インプットレイアウト(3D用（光のための法線）)★★//２Dは法線いらない
	//頂点の情報
	//1頂点に、位置、法線、色　の情報を持たせる
	//Release前に、
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		//layoutを配列にしているのは、
		//頂点に位置情報、法線などなどいろいろ情報を入れなくてはいけないので、
		//法線などが増えるなら、この配列に増やしていく

		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
																								//RGBの領域にｘｙｚを入れる（フォーマットで位置を示す）
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
																									  //テクスチャコーディネートの略
																									  //UVはｘ、ｙの位置二つだけでよいので、RGの2つ
																									  //先頭の情報、メモリの中に、頂点の位置、UVという情報が入っている　という頂点情報が連続している
																									  //UVのサイズの位置はどこからとるの？→位置は、はじめからなので、　０から→UVは位置の次なので、位置のサイズ（XMVECTOR）の次の次からなので、XMVECTORサイズの後からサイズをとります　という意味
																									  //これ以降にUVの下に書くのがあれば、サイズは、3つ目から〜
																									  //頂点が、位置とUV座標の情報を持っている
																									  //頂点に法線情報の追加
	{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },//法線

	};

	//要素数を取得したので、
	//全体のサイズ /  要素の型（計算結果は、unsigned int）
	//unsignedint size = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);
	//（UINT）でキャスト

	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_3D].pVertexLayout)))
		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &pVertexLayout);
	//インプットレイアウトの、情報が確定される
	//ポジションだけなので１つ＝１										//ポインタを入れる


	pCompileVS->Release();
	//VSを開放　コンパイル完了したので



	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;
	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	assert(pCompilePS != nullptr);
	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pPixelShader);
	pCompilePS->Release();


	//★２Dにおいて、らすたらいざ　
	//ラスタライザ、頂点の置ける、その頂点を埋めるための、ものなので、塗りつぶすことができなくなる
	//なので、ラスタライザは必要
	//２Dにおいて、かリングモード→裏から見ることはないので、FRONTを消すようにしなければ、いい
	//ラスタライザーに関しては、子リジョンの、ワイヤーフレームなどを使うとき以外で設定は必要ない


	//ラスタライザ作成
	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
	//→そのピクセルごと、ピクセル化みたいなもの
	D3D11_RASTERIZER_DESC rdc = {};

	rdc.CullMode = D3D11_CULL_BACK;
		//三角形とのレイ当たり判定の時も、逆回りの三角形では反応しない。（逆回りも反応するようにすれば別だが、）		
		//→つまり、裏側が、見えていないほうが、レイ当たり判定を行って、当たっているはずなのに、当たってないという事態にならない。

	//かリングモード　の略
	//★裏のポリゴンを表示しますか、表示しませんか
	//→ポリゴンのいる、いらない
	//→ポリゴンには裏があるはず、→さいころの１の面を見ているとき→６の面の裏面のポリゴンは見えないはず
	//→なので、かリング＝　BACK　裏のポリゴンはいらないよとしている
	//定義に移動
	//FRONT(表いらない)BACK(裏いらない)NULL（いらないものなし）


	//FRONTとすると、→表面が表示されないようになる
	//→なので、中がくりぬかれたように見える（部屋の、正面の壁を追っ払ったような）
	//半透明の場合は、両面表示するので、かリングにはどっちも入れない

	rdc.FillMode = D3D11_FILL_SOLID;
	//ソリッド→中身も埋めて　塗りつぶす
	//ワイヤーフレーム→枠だけ、
	//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
	//当たり判定表示、取り合えず表示されているのか知りたいときなど

	rdc.FrontCounterClockwise = FALSE;
	//カウンター　反撃
	//時計
	//FRONT　正面

	//時計回りが、普通表、
	//反時計回りが表にしますか？という設定
	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）


	//MAYAで、三角化しないでFBXにすると
	//→長方形を作って、三角化しないと、FBXにすると
	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり

	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_3D].pRasterizerState)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);

	return S_OK;
}

//２Dシェーダーの初期化
//シェーダーを、２D用、シェーダーのポインタも２D用を使用する
HRESULT Direct3D::InitShader2D()
{
	// 頂点シェーダの作成（コンパイル）
	//シェーダーは、インクルードとは違い、ファイル別にプログラムを用意している（シェーダー専用のファイル）、コンパイルさせないといけないので、そのコンパイルのためのプログラム
	ID3DBlob *pCompileVS = nullptr;	//コンパイル


	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "２Dシェーダファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL);
	//ファイルをコンパイルする　ファイル名				名前	グラボのバージョン		コンパイルするポインタ
	//グラボのバージョンは、特に難しいことをしないのであれば、小さい値でOK
	//L"Simple~"	Lは？
	//→メモにて
	//
	assert(pCompileVS != nullptr);
	//上記のファイル読み込みも、HRESULTである
	//→この時、HRESULTでも、エラーを返して、メッセージボックスが出て、
	//あさーとでもエラーのボックスを出してくる

	//アサーとは、デバック用と考えて、最終的にはなくしていく
	//アサーとは、プログラマのミスで起こりえる、ファイル名チェックなどのものは、アサーと
	//→プレイヤーが、ファイルを消してしまったなどの問題がある→それは、エラー処理で？
	//エラー処理はif分なので、if文を入れれば入れるほど、重くなる
	//だから、→すべてにエラー処理を入れればいいのかといえば、そうでもないとは思うが、
	//→現段階では、マメなのかと思われるかも？→とにかく、HRESULT物は、エラー処理をする


	//pVertexShaderを作成（頂点シェーダー）
	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pVertexShader)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "コンパイルしたシェーダを代入失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &pVertexShader);
	//コンパイルしたもの（pCompileVS）を入れる（クリエイト）

	//★★頂点インプットレイアウト(3D用（光のための法線）)★★//２Dは法線いらない
	//頂点の情報
	//1頂点に、位置、法線、色　の情報を持たせる
	//Release前に、
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		//layoutを配列にしているのは、
		//頂点に位置情報、法線などなどいろいろ情報を入れなくてはいけないので、
		//法線などが増えるなら、この配列に増やしていく

		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },	//位置
																								//RGBの領域にｘｙｚを入れる（フォーマットで位置を示す）
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },//UV座標
																									  //テクスチャコーディネートの略
																									  //UVはｘ、ｙの位置二つだけでよいので、RGの2つ
																									  //先頭の情報、メモリの中に、頂点の位置、UVという情報が入っている　という頂点情報が連続している
																									  //UVのサイズの位置はどこからとるの？→位置は、はじめからなので、　０から→UVは位置の次なので、位置のサイズ（XMVECTOR）の次の次からなので、XMVECTORサイズの後からサイズをとります　という意味
																									  //これ以降にUVの下に書くのがあれば、サイズは、3つ目から〜
																									  //頂点が、位置とUV座標の情報を持っている
																									  //頂点に法線情報の追加
	
		//法線は２Dには必要なし（光関係ない）

	};

	//要素数を取得したので、
	//全体のサイズ /  要素の型（計算結果は、unsigned int）
	//unsignedint size = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);
	//（UINT）でキャスト

	if (FAILED(pDevice->CreateInputLayout(layout, (UINT)(sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC)), pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_2D].pVertexLayout)))
		//頂点インプットレイアウトの情報を２つにしたので、　上記の数も２にしなければエラーになる
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "頂点インプットレイアウトの情報を確定失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ

	}
	//pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &pVertexLayout);
	//インプットレイアウトの、情報が確定される
	//ポジションだけなので１つ＝１										//ポインタを入れる


	pCompileVS->Release();
	//VSを開放　コンパイル完了したので



	// ピクセルシェーダの作成（コンパイル）
	ID3DBlob *pCompilePS = nullptr;
	//D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL);
	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "シェーダーファイルのコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	assert(pCompilePS != nullptr);
	pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pPixelShader);
	pCompilePS->Release();


	//★２Dにおいて、らすたらいざ　
	//ラスタライザ、頂点の置ける、その頂点を埋めるための、ものなので、塗りつぶすことができなくなる
	//なので、ラスタライザは必要
	//２Dにおいて、かリングモード→裏から見ることはないので、FRONTを消すようにしなければ、いい
	//ラスタライザーに関しては、子リジョンの、ワイヤーフレームなどを使うとき以外で設定は必要ない


	//ラスタライザ作成
	//→ラスタ、→ピクセルのマスによってできている（近づくと、マスでできているので、凸凹になってしまうもの）
	//→そのピクセルごと、ピクセル化みたいなもの
	D3D11_RASTERIZER_DESC rdc = {};

	rdc.CullMode = D3D11_CULL_BACK;
	//かリングモード　の略
	//★裏のポリゴンを表示しますか、表示しませんか
	//→ポリゴンのいる、いらない
	//→ポリゴンには裏があるはず、→さいころの１の面を見ているとき→６の面の裏面のポリゴンは見えないはず
	//→なので、かリング＝　BACK　裏のポリゴンはいらないよとしている
	//定義に移動
	//FRONT(表いらない)BACK(裏いらない)NULL（いらないものなし）


	//FRONTとすると、→表面が表示されないようになる
	//→なので、中がくりぬかれたように見える（部屋の、正面の壁を追っ払ったような）
	//半透明の場合は、両面表示するので、かリングにはどっちも入れない

	rdc.FillMode = D3D11_FILL_SOLID;
	//ソリッド→中身も埋めて　塗りつぶす
	//ワイヤーフレーム→枠だけ、
	//枠だけなので、三角形にて表現しているので、三角形のつながりとなる
	//当たり判定表示、取り合えず表示されているのか知りたいときなど

	rdc.FrontCounterClockwise = FALSE;
	//カウンター　反撃
	//時計
	//FRONT　正面

	//時計回りが、普通表、
	//反時計回りが表にしますか？という設定
	//どっちの回転を表面とするか（三角形があった時、頂点を時計回りに見るか？ということ）


	//MAYAで、三角化しないでFBXにすると
	//→長方形を作って、三角化しないと、FBXにすると
	//→MAYAが、勝手に三角化をする（四角形を２つの▲の作りに変換→三角化）→すると、　面を三角化したとき、勝手に、一方を時計回り、もう一方を反時計回りにする
	//→すると、一方を表面、もう一方を裏面とする→すると、透明になったり

	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_2D].pRasterizerState)))
	{
		//エラー処理
		//エラー原因の　メッセージボックスを表示
		MessageBox(nullptr, "ラスタライザの作成失敗", "エラー", MB_OK);
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	}
	//pDevice->CreateRasterizerState(&rdc, &pRasterizerState);

	return S_OK;
}


//描画開始
//描画のバックバッファの絵を用意
void  Direct3D::BeginDraw()

{
	//ゲームの処理
			//DirectXは、OSを、飛ばすといったが、Windowで動いているので、
				//→Windowのメッセージを優先
				//m着ウィンドウを閉じろと言っているのに、ゲームが動いたら大変なので、
				//→ウィンドウがわのメッセージを優先

			//ウィンドウのメッセージがなかった時にゲームの描画


	//https://www.peko-step.com/tool/getcolor.html
	//https://note.cman.jp/color/create_color.cgi

	//★
	//背景の色（色情報）
						//float clearColor[4] = { 0.0f, 0.5f, 0.0f, 1.0f };//R,G,B,A（A=透明）	//緑
	//float clearColor[4] = { 0.0f, 0.6f, 1.0f, 1.0f };//R,G,B,A（A=透明）	//水色に近い青
	float clearColor[4] = { 0.19f, 0.64f, 0.80f, 1.0f };//R,G,B,A（A=透明）	//緑が入った青

	//★



	//コンテキスト、　絵を描く人→バックバッファを塗りつぶす
	//画面をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor);


	//↓本来なら以下で、キャラの描画などを行って。。。
	


	//カメラのUpdate
	//毎フレーム呼ばれるようにしたい（カメラは、ゲームの中のことなので、Direct3Dに、入れる(CameraのInitializeも、ゲームに関することなので、)）
	//毎フレーム呼ばれているのは、Beginなので、
	//画面を切り替えた後に、カメラを更新すれば、移動の後を見ることができる
	Camera::Update();		//EndDrawで、入れ替えるので、その前にカメラを更新


	//深度バッファクリア
	//初期化
	//カメラの距離を登録させるZバッファの塗った配列の初期化
	//→９９９９でカメラからの距離を登録する配列初期化をする、クリアを行わなければいけない
	//	→だが、→９９９９というのは、この距離より前だったら、書き込みとするやつなので、→この値を、カメラの見える最大距離にしておけば、
	//	→それよりも遠いものは、描画しないといった、形で描画をすることが可能
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}



//描画終了
//スワップチェインによって、バックバッファを入れ替える
//（描画されたものと、前回の画面を入れ替える→描画）
HRESULT Direct3D::EndDraw()

{
	//描画処理
	//ここで、キャラなどの描画


	//スワップチェインが、入れ替える
	//スワップ（バックバッファを表に表示する）
	if (FAILED(pSwapChain->Present(0, 0)))
	{
		MessageBox(nullptr, "スワップチェイン失敗", "エラー", MB_OK);
		//Presentという処理をおこなって、　その処理が成功したら、HRESULTの型で、S_OK 失敗したら、E_~と帰ってくる
		return E_FAIL;	//エラーを返す	//エラーを返さないといけないので、関数の型名をHRESULTへ
	
	}
	//pSwapChain->Present(0, 0);

	//→これでDirectXの広大な世界が、広がっている状態
	//これを１秒間のうちに、裏で描いて、入れ替えてと６０回繰り返している（FPS６０）


	return S_OK;	//問題なくHRESULTの型の関数が成功したとき→OKですと返す（OKなら、問題なく次に進める）
}



//解放処理
void Direct3D::Release()

{
	//後に、確保したものを、先に開放
	//構造体配列ポインタの解放
	//０から
	//シェーダーの個数　enumの最後まで
	//１ずつ
	for (int i = 0; i < SHADER_MAX; i++)
	{
		SAFE_RELEASE(shaderBundle[i].pRasterizerState);
		SAFE_RELEASE(shaderBundle[i].pVertexLayout);
		SAFE_RELEASE(shaderBundle[i].pPixelShader);
		SAFE_RELEASE(shaderBundle[i].pVertexShader);
	}

	//クラスのオブジェクトを動的生成するために
	//newの代わりに〜Deviceを使用した、→なので、Deleteするときも、専用の開放関数が存在する

	//解放処理　Relese()
	SAFE_RELEASE(pRenderTargetView);
	SAFE_RELEASE(pSwapChain);
	SAFE_RELEASE(pContext);
	SAFE_RELEASE(pDevice);
	

	//作った順と逆に消していく→作るときは偉い人（監督）から

}