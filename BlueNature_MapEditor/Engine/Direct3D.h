#pragma once
#include <d3d11.h>	//Direct３Dの略
#include <assert.h>

//リンカ
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")	//シェーダーをコンパイルするために、追加
	//ビルドされて、アプリにするながれ、プログラムをコンパイルされて、
	//リン化して、コンパイルをしたものに、あらかじめ用意されているものをくっつける（先にコンパイル済みのものを、くっつける）
	//インクルード	は、コンパイル前にやっている
//★→これはどこのソースに書いても一つ書いてしまえばいい
	//ソース内でなくても、プロジェクトの中に書いてしまうこともできるが、→ソースに書いたほうが忘れないので、
//これをすれば、DirectXSDKの、ライブラリが追加される


#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
//マクロ
//→deleteの際に、ポインタが、領域のアドレスを覚えているときに、そのアドレスを忘れさせる
	//→これによって、deleteした後に、

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//Releseも同じく
//ポインタに使用されているポインタのReleaseを呼んであげる。


enum SHADER_TYPE
{
	SHADER_3D, SHADER_2D, SHADER_MAX
};	//SHADER_ENDという名前の要素を、enumの最後につけておくことで、
	//配列の要素数をとるときに、SHADER_ENDにすれば、必要分のEND前までの要素数を取得できる

//外部からも、読み込めるように、enumは、外に宣言しておく

namespace Direct3D

{
	//関数を創ったら、必ず引数と、戻値を書く
	//コメントを書けば、→呼び出すとき、つながるコメントを表示してくれる
	//→そこに、引数などを何かを書いておけば、いちいちヘッダを開く必要もない


	extern ID3D11Device*   pDevice;		//デバイス
		//extern　にしないと、これを、インクルードしたときに、仮に、pDevice単体であった時に、→Direct3Dをインクルードしたところで、新しく領域がとられる
		//なので、気共通のものを使いたいときは、→externでプロトタイプ宣言のようなもの、→後でどこかで宣言するので、インクルード先では、その宣言したものをきょうつうしてつかいます　


	extern ID3D11DeviceContext*    pContext;
				//コンテキストを、他ソースからも見れる、形に
				//cppにて、宣言し、インクルード先で、その宣言したものを使えるように

	//externは、これを作っておくよという、宣言のようなもの
		//cppにて、値の初期化を行わなければいけない（どこかで宣言）
	extern int scrWidth;	//ウィンドウの横幅
	extern int scrHeight;	//ウィンドウの縦幅

	//初期化
	//引数：winW ウィンドウ幅
	//引数：winH ウィンドウ高さ
	//引数：hWnd ウィンドウハンドル
	//戻値：なし
	HRESULT Initialize(int winW, int winH, HWND hWnd);


	//シェーダーの初期化(準備)
	//
	//戻値：エラー処理結果（エラーなし：S_OK、エラーあり：E_FAIL）
	HRESULT InitShader();


	void SetShaderBundle(SHADER_TYPE type);

	//３Dシェーダーの初期化　
	HRESULT InitShader3D();

	//２Dシェーダーの初期化
	HRESULT InitShader2D();


	//描画開始
	//引数：なし
	//戻値：なし
	void  BeginDraw();



	//描画終了
	//引数：なし
	//戻値：処理が成功か（HRESULT型の関数が成功した場合、最終的にS_OK、失敗の時その都度E_FAIL（どんなエラー化は、気にせずエラーです）
	HRESULT EndDraw();



	//解放
	//引数：なし
	//戻値：なし
	void Release();

};

//Direct3Dに必要なものを、namespaceでまとめた
//→書き方としては、クラスと変わらない


