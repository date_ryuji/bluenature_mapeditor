#pragma once

#include <d3d11.h>
#include <fbxsdk.h>
#include <string>
#include "Transform.h"
#include "Direct3D.h"


#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")

//３Dモデルの描画を行った
//→Quadのクラスとやることは変わらず
//→頂点情報を受け取って、（位置、法線）
//→頂点情報の三角形の形成（インデックス情報）をもらって
//→それを組み合わせることができれば、→FBXの形を形成することが可能である
	//→頂点数、ポリゴン数は、FBXの形でまちまちなので、→FBXを読み込んでから出ないとわからない→FBXから取得する形に

//プロトタイプ宣言のように
//クラスの前方宣言
class Texture;

//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル
	float		dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか
};


class Fbx
{
	//コンスタントバッファ
	//シェーダーに渡す情報（シェーダー内のコンスタントバッファに変数として持っている情報に渡す情報を、構造体として取得しておく（渡す情報をまとめておく））

	
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//ワールド行列＊ビュー行列＊プロジェクション行列
		XMMATRIX	matW;	//ワールド行列

		XMFLOAT4 diffuseColor;	//マテリアルの色（テクスチャが使用されていない、部分に使用されているマテリアルの色を保存しておく変数）
								//MATERIALにて使用されている、FLOAT4の型を使用して、型を合わせる
		int isTexture;	//テクスチャが張られているか
		//フラグは、Ｃ＋＋側で普通に登録
						//ただのフラグだが、C++でのbool型のフラグの取り方と、HLSLシェーダーファイルでのbool型のフラグの取り方では違いがある
						/*
							C++は、boolの１バイト（８ビット）における、先頭１ビットにフラグの値０，１を登録
								→そして、取得するときも、bool型の８ビットから、先頭の１ビットに入っていることがわかっているので、
								→先頭１ビットからフラグ状況を受け取っている（残りの７ビットは使用しない）
						
							HLSLは、逆にboolの８ビットから
								→お尻の１ビットからフラグの０，１を取得する

							見るところが違うため→boolをそのまま使用することは不可能
								→ちなみにbool型ではなく、int型であれは、きちんとフラグとしてtrue,false入れたところと、HLSLで見るところは一緒
									→なので、int型でフラグをとってしまえば、その場で解決はする
							
							〇boolで何個も作成して、フラグ管理に用いるのはもったいない
								→フラグに使用するのが、１ビットならば、
								→１バイトの中には、１ビットが８つ→８つのフラグが管理できるということでは？
								→以下の「他の解決策として」へ
						*/

						/*
							その他の解決策として
							BYTEの型を使用する（BYTE=unsigned char）

							詳しく、参考例
							Input.cppのIsKey関数を参照
						
						
							★論理演算にて、ビットの計算を使用してフラグに使用されているビットの位置を指定して取得する
							取り出す時に、逆ならば、→先頭の１ビットからビットを取得する様に書けばいい
								→登録に使用、先頭の１ビットは、2真数にしたときの、0オリジンで、7桁目
								→10進では、128、16進では0x80（これらと、論理和にて、登録）
								→お尻の１ビットから取り出す。2進数では、1桁目。
　　							→10進では、1、16進では、〜〜〜
								それを論理積にて、フラグが立っているかの確認

						*/
	};

	//頂点情報
		//現段階ではポジションだけの頂点情報（のちに法線）
	struct VERTEX
	{
		XMVECTOR position;	//位置情報（頂点の位置）
		XMVECTOR uv;		//UV情報	（頂点のテクスチャの座標）
		XMVECTOR normal;		//法線情報	（頂点の陰）
	};



	//マテリアル
		//今後、テクスチャのほかにも、マテリアルそのものの色も取得しておく
	struct MATERIAL
	{
		Texture*	pTexture;	//テクスチャのマテリアル
		XMFLOAT4	diffuse;	//テクスチャの貼られていないマテリアルの色
	};

	//各頂点ごとの頂点情報を入れる配列
	VERTEX* pVertices_;

	//マテリアルごとの
	//インデックス情報を入れる２次元配列
		//ポインタのポインタ　＝　ポインタのポインタに複数個のポインタ動的確保　（ppIndex = new int*[5]）
		//ポインタのポインタ[0] = ポインタのポインタを複数個に分けた、その「０」に、複数個要素動的確保  (ppIndex[0] = new int[10]) 
		//ポインタのポインタで、　ポインタを複数個確保することになるので、　その、確保した、複数個分解放してから、ポインタのポインタを解放しないといけない
	int** ppIndex_;


	//頂点情報を受け渡し
	ID3D11Buffer *pVertexBuffer_;
	//頂点の順番→インデックス情報を持っておく情報
	//ポインタを複数持たないといけないので、→ポインタのポインタ＊＊
	ID3D11Buffer **ppIndexBuffer_;
	//コンスタントバッファ→渡すため
	ID3D11Buffer *pConstantBuffer_;
	//上記の情報があれば、→FBX,頂点情報、インデックス情報を受け取れば、
		//→Quadクラスト同じように大量に三角形を作れば表示は可能なはず
	
	//マテリアルの構造体ポインタ
	MATERIAL* pMaterialList_;	//マテリアル数分の構造体を取得する
								//ノードに存在するマテリアルの個数分要素を動的確保



	//FBXを読み込むので、あらかじめわからないので、
	//メンバとして宣言しておく
	int vertexCount_;	//頂点数（FBXファイルより取得）
	int polygonCount_;	//ポリゴン数（FBXファイルより取得）
	int materialCount_;	//マテリアルの個数（マテリアルのLambert１、２などを入れるための数）（ノードに存在するマテリアルの個数）

	//マテリアルごとのインデックス情報の個数を保存する配列
	//マテリアル数分、要素を取得
	int *indexCountEachMaterial_;	//動的に要素を確保するためにポインタ（materialCount_という変数で要素を確保するためにポインタを使う）

	//頂点バッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitVertex(fbxsdk::FbxMesh * mesh);
	//インデックスバッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT InitIndex(fbxsdk::FbxMesh * mesh);

	//コンスタントバッファ作成
	//引数：FBXモデルのノードのメッシュ
	//戻値：エラーの有無
	HRESULT IntConstantBuffer();

	//FBX使用のマテリアルの作成
	//引数：FBXモデルのノード（メッシュではない→メッシュにはマテリアルは存在しない）
	//戻値：エラーの有無
	HRESULT InitMaterial(fbxsdk::FbxNode* pNode);

public:

	//コンストラクタ
	//引数：なし
	//戻値：なし
	Fbx();

	//FBXファイルのロードを行う（引数のファイル名より）
	//引数：fileName ファイル名
	//戻値：なし
	HRESULT Load(std::string fileName);
	
	//FBXモデルの描画を行う
	//引数：&transform	描画時の変形情報（移動、拡大、回転）
	//戻値：なし
	HRESULT    Draw(Transform& transform);
	//ポインタの解放処理
	//引数：なし
	//戻値：なし
	void    Release();

	//レイキャスト
	//レイと、FBXとの衝突判定
	//引数：レイキャスト用の構造体ポインタ
	//戻ち：なし
	void RayCast(RayCastData* rayData);
};