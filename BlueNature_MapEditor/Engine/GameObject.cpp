#include "GameObject.h"
#include "Global.h"			//SAFE_DELETEなどのマクロ関数を所有しているヘッダ
							//どこでも使うようなマクロを持っておく
#include "SphereCollider.h"

#include "Direct3D.h"


//オーバーロード
//引数なしのコンストラクタが呼ばれたときに、何も要素を持たない引数にして、引数ありのコンストラクタを呼ぶ
//引数なしのコンストラクタ
	//親もない、名前もない
	//→これは、親もない、名前もないという引数で、引数ありのコンストラクタを呼んでやればいい
	//→これをすれば、GameObjectの引数ありのコンストラクタを呼べるので、→同じことをできる
GameObject::GameObject()
	:GameObject(nullptr , "")
{


}

//引数ありのコンストラクタが呼ばれたとき
	//引数に親のGameObjectのポインタ、
	//      自身の名前
//それぞれ初期化
GameObject::GameObject(GameObject * parent, const std::string & name)
	:pParent_(parent),
	objectName_(name),
	isDead_(false)
{


	//ベクターの初期化（クリア）
	collDatas_.clear();

	//リストの初期化（クリア）（宣言部には書けないので、）
	childList_.clear();

}

//GameObjectをオブジェクトを消すとき、子供を消さないといけない
//何を消せばいいのか
	//自分が死んだとき
		//その子供は消すのか？＝消す
		//→自分が親だったら、その親が消えたら、子供を消さないといけない
	//親は消すのか＝消さない
		//子供が消えても、親は消えない（プレイシーンの子供の敵が消えても、プレイシーンは消えない）
//自分のReleaseは、自身のオブジェクトの中で動的確保したものなどをReleaseするものなので、
//自身が消えるときにデストラクタで子供の解放を行わなければいけない。
GameObject::~GameObject()
{
	//すべての子供の解放
	AllChildrenKill();

	//コライダー情報の解放（自身のコライダー）
	AllColliderKill();

	
}

//すべての子供の解放を行う
	//デストラクタ
	//SceneManagerは、シーンを切り替えるときに、自身の子供を全消去を行う
void GameObject::AllChildrenKill()
{
	//子供のReleaseをすべて呼んであげないと
	//そして、Releaseを呼んだら、delete
	//リストをすべて回すためのイテレータ

	//イテレータの型名：std::list<GameObject*>::iterator
	//長くなるので、autoで（自動型）

	//リストの最初から、最後まで、
	//リストを指してくれる、イテレータを回す
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//イテレータのさしている子供のReleaseを呼ぶ
		//イテレータのさしているアドレスにある、GameObject
		(*itr)->Release();

		//そして、delete
		SAFE_DELETE(*itr);


	}

	//動的確保したポインタを解放したら
	//リストの中身をクリアする
	childList_.clear();
}

//コライダー情報の解放（自身のコライダー）
void GameObject::AllColliderKill()
{
	//配列要素分回して

	for (int i = 0; i < collDatas_.size(); i++)
	{
		//解放
		SAFE_DELETE(collDatas_[i]);
	
	}

}

//衝突時にされる処理
//オーバーライドされる関数
void GameObject::OnCollision(GameObject * pTarget)
{
}

////自身を消去するフラグを立てる（フレームの最後に自身が消去されるフラグを立てる）
//void GameObject::KillMe()
//{
//	//実際に自分の消去を行う（自身の消去と、自身の子供の消去を呼び出す）
//	//のは、自身の親のクラスである。（自分で、自分を消去するのは怖い）
//
//	//そして、KillMeから親の子供リストから直接、フレーム途中に消されると、Updateに行う処理もろもろが（消されるオブジェクト以外のところ）消去前のオブジェクトを参照しようとしてエラーになる可能性も
//
//
//	//なので、フラグを立てて、フレームのUpdateなども終わった後に、消去するようにする
//	isDead_ = true;
//
//
//}


//自分の子供から引数にもらった名前のオブジェクトがいるか探す
	//子供の子供も網羅する
GameObject * GameObject::FindChildObject(std::string name)
{
	//子供から引数の名前と同じ名前の人がいたら返す（自身の子供なので、自身の子供リストから）
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//イテレータは、各要素を指しているもの
		//イテレータの中身を見たいなら* をつける

		//イテレータの示す子供（GameObject*型）の持つ名前と引数の名前が等しいなら
		if ((*itr)->objectName_ == name)
		{
			//子供のポインタは
			return (*itr);
		}
		//子供が該当のオブジェクトでなかった(returnされなかった)
		//その子供の子供も、さらに探すために（その子供の子供も）
		
			//再帰（自分で自分の関数を呼ぶ）
				//itrの指しているオブジェクトの子供から探したい（itrの指しているオブジェクトもGameObjectなので、当然FindChildObjectが存在する。）
				//(*itr)->FindChildObject(name);
				//見つかったら、return でその子供のオブジェクトが帰ってくる（FindChildObjectより）
				//だが、現段階では、仮に子供の子供リストの中で見つけたとしても、Findから返り値が帰ってきても返せない
				//だが、見つからなかったら、forを抜けてnullを返す可能性もあるため、ここで一気に返してはいけない
			
			GameObject* pObj = (*itr)->FindChildObject(name);
				//探しているオブジェクトがなかったら、nullptrが帰ってくる
				//探しているオブジェクトだったら、その探しているポインタが入ってくる

			//FindChildOjbectによって返ってきた値が
			if (pObj != nullptr)
			{
				//ヌルでないならば、
				//探していたオブジェクト見つかったということなので、ポインタを返す
				return pObj;
			}
				
				

		/*
			PlaySceneからのFindChildObject（"B"）
			Bが欲しい


			PlayScene →　Player　→　A
								　
					　→　Player１→　B

			PlayScene forにてPlayerを見て、Bではない
			→(子供)PlayerのFindをよぶ
			↓
			（子供）Playerが
			Aを見て、Bではない
			→（子供）AのFindを呼ぶ
			↓
			（子供）Aが
			子供いないので、（for抜けて）
			→nullptrを返す
			↓
			Playerがnullptrを受け取って、
			次の子供がいない（for抜けて）
			→nullptr返す
			↓
			PlaySceneがnullptrを受け取って、FindChildによる結果がnullのため、
			forが回って、PlaySceneの次の子供へ
			→(子供)Player1を呼ぶ
			↓



		*/

		
	}

	//子供が見つからなかったのでnullptrを返す
	return nullptr;
}

//自分の親がいなかったら自身がRootJobオブジェクトなので、
//自身のRootJobのポインタを返す
GameObject * GameObject::GetRootJob()
{
	//自分の親を参照して
	//親がいなかったら、
	if (pParent_ == nullptr)
	{
		//自身がRootJobである。
		//→自身のポインタを返す
		return this;
	}
	//自身の親がいたら
	else
	{
		//（再帰）親のGetRootJobを呼ぶ
		return pParent_->GetRootJob();
	}
	//ゲームオブジェクトである以上、（誰かから親子付されてゲームオブジェクトとして出現した以上）
	//RootJobが一番上にいないということはあり得ない
	//一番上に到達したら→それがRootJob
	//なので、GetRootJobを呼んだところに戻って、必ず最後にはRootJobのポインタが帰ってくる
	/*
		（A）親が
			→nullptrでなかったら
		（A）親がいるので
			→（B）親のGetRootJob()
		（B）親がいないので自身がRootJob
			→（B）関数の戻り値で自身のポインタ返す
		 (A)　


	
	*/
}


//全オブジェクトの中から、指定の名前を持つオブジェクトを探す
GameObject * GameObject::FindObject(std::string name)
{
	//全オブジェクトの中から、引数の名前を持つオブジェクトを探したい
		//全オブジェクトの中から探す＝RootJobからFindChildObjectを呼べばいい
		//＝RootJobを探すことができれば、　RootJobのFindChildObject呼ぶだけで、
		//＝全オブジェクトの中から引数の名前を持つオブジェクトを探せる（ここでnullptrが帰ってくるなら、全オブジェクトの中にオブジェクトが存在しないということになる）
	return GetRootJob()->FindChildObject(name);
}

//自身の所有するコライダー配列（ベクター）に
//当たり判定を登録
void GameObject::AddCollider(SphereCollider * addCollider)
{
	//自身の所有しているコライダー配列（ベクター）にコライダーを追加
	collDatas_.push_back(addCollider);

}

//大本のWinMainにて、DrawSubを読んであげることで、
	//その下の子供のDrawSubを呼んで→さらに子供のDrawSub呼んでと続くので、すべての子供のDrawを網羅できる
//自身のDrawと
//子供のDrawSubを呼んであげる
void GameObject::DrawSub()
{

	{
		if (status.isDraw_)
		{
			//自分のDrawを呼ぶ
			Draw();
		}

	}

	//自分のDrawが終わった後に
	//子供全員のDrawSubを呼ぶ
	for (auto itr = childList_.begin();
		itr != childList_.end(); itr++)
	{
		//イテレータにて、リストの全要素を回して
		//指定された子供のDraw"Sub"関数を呼ぶ
			//Subのほうを呼ぶことで、→子供は自身のDrawとともに、さらに、子供のDrawSubを行うようになる
			//子供のクラスでは、Drawしか書かないが、実際は親からDrawSubのほうが呼ばれて、→その中でDrawが呼ばれて、子供のDrawSubが呼ばれる（これだと、子供をすべてDraw呼ぶことができる）
		(*itr)->DrawSub();	
	}

}

void GameObject::UpdateSub()
{
	if (status.isUpdate_)
	{
		//自分のUpdateを呼ぶ
		Update();
	}

	//親が動けば、子も動く
	//親が動いている時点で、子供も動いている
		//なので、子供のUpdateSubが呼ばれる前に、親のTransformの計算がされている必要がある
	//Transformの行列計算
	transform_.Calclation();


	//自身のUpdateが終わった後に当たり判定
		//どこでCollisionを呼ぶかは、衝突して、
		//・衝突後、移動後に移動分だけ戻すのか
		//・衝突する前に、移動先が衝突する位置なのか
		//このどちらで判断させるかにもよってくる。
		//★今回は→移動してから、移動分だけ戻すという方式をとる衝突判定をとる。（移動後に自身の位置がほかのコライダーと衝突しているか）
	//当たり判定の検証（自身のコライダーと、ほか全体のコライダーとの当たり判定）
	Collision();


	//自分のUpdateが終わった後に
	//子供全員のUpdateSubを呼ぶ
	for (auto itr = childList_.begin();
		itr != childList_.end(); itr++)
	{
		//イテレータにて、リストの全要素を回して
		//指定された子供のDraw"Sub"関数を呼ぶ
			//Subのほうを呼ぶことで、→子供は自身のDrawとともに、さらに、子供のDrawSubを行うようになる
			//子供のクラスでは、Drawしか書かないが、実際は親からDrawSubのほうが呼ばれて、→その中でDrawが呼ばれて、子供のDrawSubが呼ばれる（これだと、子供をすべてDraw呼ぶことができる）
		(*itr)->UpdateSub();
	}




	/*消去フラグの立っている子供の消去******************/
		/*
			UpdaeSubにて
			すべてのこどもの
			UpdateSubを呼んだあと(Updateが終わる前に消してはいけない)
		
		*/
	//すべての子供をループして、
	//isDeadのフラグが立っているかの確認
	//イテレータにて、リストの全要素を回して
	for (auto itr = childList_.begin();
		itr != childList_.end();)
	{
		//消すフラグが立っていたら、
		if ((*itr)->isDead_ == true)
		{
			//子供のReleaseSub（子供のすべての解放）
			(*itr)->ReleaseSub();

			//Releaseを読んだら、SAFE_DELETEでポインタの解放
			SAFE_DELETE(*itr);

			//子供リストの中から削除
			itr = childList_.erase(itr);
			/*
				リストの要素を消すとき
				イテレータの削除

				eraseでその場で消してしまうとダメ。

				A,B,C,D
				Aさんは、Bさんの電話番号を知っている（他を知らない）
				Bさんは、Cさんの電話番号を知っている（他を知らない）
			

				回覧板を渡す（その回覧板をイテレータとして）
				イテレータのbeginによって、Aさんに回覧板（イテレータ）回る
				イテレータ＋＋によって、　Bさんに回覧板

				Bさんが途中でいなくなるとする。
					→Bさんがいなくなって、回覧板を持っている状態でいなくなる

				CさんにBさんが持っていた回覧板を渡さなければいけない（BさんだったイテレータがCさんにわたる）

				消えたときCさんがBさんの位置のイテレータの位置に位置することになる
					→なので、for分で回った時に、Cさんがやらずに、次に行ってしまうことになる

				それを防ぐために、
				→イテレータを消したら、イテレータBさんの位置がCさんになる
					→★なので、次に回していけない

				だったら、イテレータを消さなかったら、イテレータを回して（itr++）
				消したらイテレータを回さない(×)
			
			*/
			//イテレータを消されたら、戻り値として
			//消した次の人のイテレータが入ってくる
				//それを次のイテレータとする


		}
		else
		{
			//イテレータが消されなかったらイテレータを回す
			itr++;
		
		}
	}

	
}

void GameObject::ReleaseSub()
{
	//自分のUpdateを呼ぶ
	Release();

	//自分のUpdateが終わった後に
	//子供全員のUpdateSubを呼ぶ
	for (auto itr = childList_.begin();
		itr != childList_.end(); itr++)
	{
		//イテレータにて、リストの全要素を回して
		//指定された子供のDraw"Sub"関数を呼ぶ
			//Subのほうを呼ぶことで、→子供は自身のDrawとともに、さらに、子供のDrawSubを行うようになる
			//子供のクラスでは、Drawしか書かないが、実際は親からDrawSubのほうが呼ばれて、→その中でDrawが呼ばれて、子供のDrawSubが呼ばれる（これだと、子供をすべてDraw呼ぶことができる）
		(*itr)->ReleaseSub();
	}
}

//全オブジェクトのコライダーと、自身の全コライダーとの衝突判定
	//全オブジェクトとの総当たり
void GameObject::Collision()
{

	//総当たりのため、全オブジェクトを見るためにRootJobから
		//RootJobから、子供がいるなら
		//→その子供のコライダーとの衝突判定、
		//→子供がいるなら、その子供との衝突判定
	//自身のコライダーを送って、
	//再帰的に、もらったコライダーと呼ばれた側の持っているコライダーとで、衝突判定
		//衝突していたら、
		//→trueが帰ってくる
		//→だが、これでは、１つのコライダーにあたった、時点で帰ってきてしまう
		//→１つのコライダーとの接触でよいのであれば、問題は無いが、→総当たりに行うとは、そうゆうことではない気がする
		//→★衝突のコライダーに接触しているときに、片方の衝突はなかったことになるので、注意

	//自身の所有するコライダー数分回す
	for (int i = 0; i < collDatas_.size(); i++)
	{
		//RootJobから総当たりで
		//自身（Collisionを呼んでいるオブジェクト）と他コライダーとで、衝突しているかをもらう（RootJobから見れば、全オブジェクトと総当たりに判定できる）

		//コライダーに自身のオブジェクトの位置を送り保存してもらう
			//→コライダーの位置を、オブジェクトの位置＋コライダーの離れ具合でとるので、
		collDatas_[i]->SetObjectPos(transform_.position_);


		//RootJobから、全オブジェクトのコライダーとと自身のコライダーとの衝突判定
			//RootJobのCollisionChildObjectを呼ぶ
		//衝突していた場合、衝突した相手のGameObject*がもらえる
		GameObject* pCollTarget = GetRootJob()->CollisionChildObject(collDatas_[i]);


		//nullptrでないなら
		if (pCollTarget != nullptr)
		{
			//衝突しているので、
			//衝突の相手を送って、衝突時の処理を行う
			OnCollision(pCollTarget);

		}
		//nullptrなら、衝突していないので何もしない。
	}





	//返り値で衝突していると判定が来たら
	//自身の衝突されたときに呼ぶ関数を呼びこむ（引数に当たった先のコライダーを渡して）

}

void GameObject::StopChildUpdate()
{
	//子供リストの子供一つ一つに
	//Updateを無効とするフラグを下す処理
		//構造体メンバのisUpdateを下す
	//子供リストの子供1人1人に
	//Updateを無効とする
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//条件なしに
		//フラグを下す
		(*itr)->DisableUpdate();

	}

}

void GameObject::StopChildDraw()
{
	//子供リストの子供1人1人に
	//Drawを無効とする
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		(*itr)->DisableDraw();

	}
}


void GameObject::StartChildUpdate()
{
	//子供リストの子供1人1人に
	//Updateを有効とする
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//条件なしに
		//フラグを上げる
		(*itr)->EnableUpdate();

	}
}

void GameObject::StartChildDraw()
{
	//子供リストの子供1人1人に
	//Drawを有効とする
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		(*itr)->EnableDraw();

	}
}
void GameObject::EnableUpdate()
{
	status.isUpdate_ = true;
}
void GameObject::EnableDraw()
{
	status.isDraw_ = true;
}
void GameObject::DisableUpdate()
{
	status.isUpdate_ = false;
}
void GameObject::DisableDraw()
{
	status.isDraw_ = false;
}


//再帰を行って、自身の子供に引数のコライダーと当たっているオブジェクト(オブジェクトの持つコライダーからすべて調べる)を探す
GameObject* GameObject::CollisionChildObject(SphereCollider* collider)
{
	//自身の子供の数分回して
	//衝突の計算を行って、
		//衝突していたら、trueが返されるので
		//trueだったら、衝突したオブジェクトを返す
	for (auto itr = childList_.begin(); itr != childList_.end();
		itr++)
	{
		//"子供"の所有するコライダー分回す
			//示している子供の
		for (int i = 0; i < (*itr)->collDatas_.size(); i++)
		{

			//引数のコライダーと衝突判定を行う子供のコライダーが同じでない（ポインタなのでアドレスが同じ）
			// &&
			//引数にてもらったコライダーと
			//イテレータで示している子供のコライダーと衝突しているか
			if (collider != (*itr)->collDatas_[i] &&
				collider->CollisionDetection(
				(*itr)->collDatas_[i]->GetWorldPos(),
				(*itr)->collDatas_[i]->GetRadiusSize()))
			{
				//衝突しているなら、子供のオブジェクトを返す
				return (*itr);
			
			}

		
		}
		//returnされずに、forを抜けた　＝　”子供のコライダーには”衝突するものはなかった
		//→子供のさらに子供のコライダーから探す
		//↓


		//”子供”の所有するコライダーと目的のコライダーと衝突をしなかった場合
		//（再帰）その子供の子供に同じ衝突判定を呼ぶ込む処理を行う（子供のCollChildObで、子供リストのコライダーと再帰的に判定）
		GameObject* pCollTarget =
			(*itr)->CollisionChildObject(collider);

		//戻り値がnullptrでないなら→衝突者が見つかった
		//→返す
		if (pCollTarget != nullptr)
		{
			//衝突したコライダーを持つオブジェクトを返す
			return pCollTarget;
		}
		//nullptrなら見つからなかった
		//→そうでないならfor回す
	}


	//引数のコライダーと、相手のコライダーが同じ場合
	//自分自身となってしまうので、
	//当然接触してしまう。
	//この場合は、接触判定を行わないように。

	//上記の判定では、自分の他のコライダーとも衝突判定が起きる




	return	nullptr;
}



