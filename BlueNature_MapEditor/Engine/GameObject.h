#pragma once

/*
	すべてのゲームオブジェクトが継承するクラス
	純粋仮想関数

	ゲームに登場するWin以外のクラスは、すべて継承
		→なので、ゲームオブジェクトが、それぞれ持っていなくてはいけない情報
			→子供リスト、Transform、なまえ、親のポインタをメンバとして宣言しておく
*/


/*
	インクルードの順番も
	自分で作ったものは、下に

*/

#include <string>	//文字列を使用するためにインクルード
#include <list>		//リストを使用するためにインクルード（途中で中身を消去する可変長配列）
#include <vector>   //ベクターを使用するためにインクルード（途中で中身を消さない、配列と同じように使用できる可変長配列）
//各ヘッダは、GameObjectのヘッダと、同じフォルダに存在している（実際のプロジェクトフォルダ内のパス位置）
//なので、インクルードのパスも、ヘッダのパスだけでよい（違う位置ならば、"フォルダ名/Transform.h"など）
#include "Transform.h"

//クラスのプロトタイプ宣言
class SphereCollider;

class GameObject
{

private : 
	//オブジェクト（自身）の名前
	//名前を持っているので、
	std::string	objectName_;





//継承先にてのみpublic（ゲームエンジン下のクラスは、すべてGameObjectを継承するので、protectedにしておけば、それぞれ、の子供は参照することができる）
protected:

	//誰が、親かを持っておく
	//親もGameObjectなので、
	//GameObjectのポインタ
		//Instantiateにて、子供を作成するときに、自身の親をその子供の親としてあげたいとき。
		//自身の親を引数として送らなければならない、→なので、参照できるようにprotectedのアクセス修飾子にしておく
	GameObject*	pParent_;

	//子供リスト
	//途中で減ったり、増えたりする→どうする？→リスト
		//可変長配列のリスト
	//ポインタのリスト
	std::list<GameObject*> childList_;

	//自身を消去するかのフラグ（フレームの最後に、自身を消去するフラグが立っていたら（親から確認）消去される）(いきなり消されると困る)
	bool isDead_;



	
public:


	//すべてが、Transformの型を持っていなくてはいけない
		//→Transformの型をインクルードして、宣言
			//→すべての継承先にて、Transformの型を使用可能
	//GameObjectを継承した、継承先に限らず、Instantiateにて子供を作成して、戻り値でそのポインタを受け取った先においても、子供の位置を移動したいこともある→その時に、publicでないと、動かせない
	Transform	transform_;

	//コライダーを入れるベクター配列（可変長）
		//すべてのオブジェクトから、自身の持っているコライダーと当たっているかの判定をとる。
		//→そのため、どこからでも見れるpublicに置いておく。
	std::vector<SphereCollider*> collDatas_;




	//コンストラクタ２種類
	//引数なし　//親、オブジェクト名指定なし
	//引数：なし
	//戻値：なし
	GameObject();
	//引数あり　//親、オブジェクト名指定
	//引数：parent 親となるGameObjectのポインタ
	//引数：name オブジェクトの名前
	//戻値：なし
	GameObject(GameObject* parent, const std::string& name);
	
	//デストラクタ
		//継承されるクラスのデストラクタはVirtual（デストラクタは継承先においては、オーバーロードされない、deleteしたら呼ばれるのは親のみ（なのでオーバーライドしてキャストしても継承先のクラスが呼ばれるようにする））
	//引数：なし
	//戻値：なし
	virtual ~GameObject();

	//全子供の解放
	//引数：なし
	//戻値：なし
	void AllChildrenKill();

	//全コライダー（自身所有のコライダー）の解放
	void AllColliderKill();

	//各継承先
	//純粋仮想関数にしたいので、
		//このクラスでは、関数の中身は持たず、中身は、各継承先にて、実装する
	//頭にvirtual ,　お尻に　=0
	
	//初期化
	//純粋仮想関数
	//引数：なし
	//戻値：なし
	virtual void Initialize() =0;	
	//更新
	//純粋仮想関数
	//引数：なし
	//戻値：なし
	virtual void Update() =0;		
	//描画
	//純粋仮想関数
	//引数：なし
	//戻値：なし
	virtual void Draw() =0;
	//解放
	//純粋仮想関数
	//引数：なし
	//戻値：なし
	virtual void Release() =0;

	//衝突した際に呼ばれる関数
	//引数：自身のコライダー衝突した相手のポインタ
	//戻値：なし
	virtual void OnCollision(GameObject* pTarget);


	//自分の消去するフラグを立てる（実際に消去されるのはフラグを立てた、フレームの最後（Updateの途中などで、消えても困る（Updateの途中で、本来３個あるものが、3個目のオブジェクトが存在しないとなる）））
		//フラグをセットするセッターみたいなやつ
	//引数：なし
	//戻値：なし
	void KillMe() { isDead_ = true; };


	/*
		該当オブジェクトを探す

		�@自分の子供以下に
		　指定した名前がいないかを調べる

		�ARootObjectを探す関数

		�B見つかったRootJobの中に、RootObjectの中に、指定したオブジェクトがいないか
			→RootJobの子供から、さらに子供を探索して、
			→目的のオブジェクトがいないか、、
	
	*/

	//自身の子供リストから指定の子供を探す
		//子供を探して、探した子供のポインタが欲しいので、GameObject*型
	//引数：探したい子供オブジェクトの名前
	//戻値：引数の名前にて見つかったオブジェクトのポインタ
	GameObject*  FindChildObject(std::string name);

	//RootJobを探す
		//Findで、オブジェクトを探す際にRootJobから全オブジェクトを網羅して呼び込まないといけない（でないと、全オブジェクト内から探すことが不可能）
	//引数：なし
	//戻値：RootJobのGameObject型のポインタ
	GameObject* GetRootJob();
		//今のオブジェクトから、
		//親がRootJobかな？
		//さらに親がRootJobかな？と探す。
		//RootJobの定義は？→親がいなかったらRootJobであるといえる。（オブジェクトは、必ず親がいる、その中で親がいないのであれば、一番上の親）
		//RootJobが存在しないということはあり得ない（GameObjectとして、誰かから親子付されたのであれば、）

	//ゲームオブジェクトを探す（全オブジェクトの中から）
		//どのオブジェクトから呼ばれても、目的のオブジェクトを探したい
		//どこから呼ばれても、全部のオブジェクトの中から探したい＝RootJobという一番上のオブジェクトから探すように
		//RootJobをもらって、そのRootJobのFindChildObjectを呼べば、、、→RootJobから子供のオブジェクトを網羅して探セル（ここで見つからなければ、全オブジェクト内に存在しないということ）
	//引数：探すオブジェクトのオブジェクト名（コンストラクタにて設定した名前に限る）
	//戻値：引数の名前で見つかったオブジェクトのポインタ
	GameObject* FindObject(std::string name);


	//自身のオブジェクトへコライダーを追加する
	//引数：自身の可変長配列に登録するコライダーのインスタンス
	//戻値：なし
	void AddCollider(SphereCollider* addCollider);




	/*継承先においても同じ処理をさせたいので、継承も必要なし*/
	//自身の描画と、子供のDrawを呼ぶ処理（子供のDrawは、親であるGameObjectが呼んであげないといけない）
	//引数：なし
	//戻値：なし
	void DrawSub();
	//自身の更新と、子供の更新を呼ぶ
	//引数：なし
	//戻値：なし
	void UpdateSub();
	//自身の解放と、子供の解放を呼ぶ
	//引数：なし
	//戻値：なし
	void ReleaseSub();
	//コライダー衝突判定の総当たり（コライダーを持つ全オブジェクトと総当たりさせる）
	//引数：なし
	//戻値：なし
	void Collision();

	//子供のUpdateを止める
		//子供が自身で持っている、Updateをするかのフラグ→これを下す
	void StopChildUpdate();
	void StopChildDraw();
	//子供のUpdateを始める。再開
	void StartChildUpdate();
	void StartChildDraw();

	//Update有効
	void EnableUpdate();
	//Draw有効
	void EnableDraw();
	//Update無効
	void DisableUpdate();
	//Draw無効
	void DisableDraw();




	
	//１つのコライダーが、全オブジェクトの持つコライダーと衝突しているか
	//自身の子供を総当たりに判定
	//引数：総当たりに衝突判定を行わせるコライダー
	//戻値：引数のコライダーと衝突したオブジェクトのポインタ
	GameObject* CollisionChildObject(SphereCollider* collider);



public : 

	struct STATUS
	{
		bool isUpdate_;
		bool isDraw_;

		STATUS():
			isUpdate_(true),
			isDraw_(true)
		{
		}
	}status;






	

	//関数のテンプレート（関数の宣言時には、なんの型が入るのかはわかっていない）
	//親子付けを作るときに様々なクラスに対応できるようにしたい（やることは決まっているが、型は指定したくない（様々な型に使いたい））
	template <class T>// = GameObject> 
						//これで、テンプレートとして、あとで、Tは、何になるのか、型は使用するときに指定するよ！！！という宣言
						//以降に書かれるTというのは、この宣言した型（後に宣言されるであろうクラスの型が入ってくる）
						/*
							C#においては、
							テンプレート型に条件を付けることが可能
							
							<T> where T : GameObject のように
							これで、違う型が入れられる可能性を省き、また、以下でGameObject型のメソッド、関数を使用することも可能になる（条件を付けないと、メソッドを使用することができない。）
						*/
						/*
							以上のように、
							型を指定して、GameObject型でないと受け付けないようにしたい（<class T>このままだとintもテンプレートの方として指定できる）

							<class T = GameObject>

							結果：型を仮に指定しても、テンプレート＜＞に型を代入時にエラーを出したりはしてくれない
						
						*/
	//親子付の処理
	//テンプレート：GameObject型を継承したクラス
	//引数：親とするオブジェクトのポインタ
	//戻値：作成したオブジェクトのポインタ
	GameObject* Instantiate(GameObject* parent)
	{
		//以降Tは、関数呼び出し時に引数？＜＞に送ったクラスが入る

		T* p;	//指定されたTの型のポインタ
		p = new T(parent);	//動的確保（引数として渡された、親のポインタをコンストラクタの引数として送る（コンストラクタは各クラス内の処理にて、親のオブジェクトを受け取るようにしている））
		p->Initialize();	//クラスのInitializeを呼び込む
		childList_.push_back(p);	//自身の子供リストに追加（push_back()リストの一番後ろに追加）
									//子供のリストは、呼び込む、各クラスはGameObjectを継承している、なので、継承先は、自身のchildList_を持っている
									//なので、自身の持つ、childList_に子供を登録

		//新たに宣言したオブジェクト
		//の親のTransformを入れてあげる
			//Instantiateは、必ずオブジェクト宣言時に通るので、その時に、
			//自身のTransform内の親のTransformに親のTransformをセット。(参照渡しでアドレスをセット)
		p->transform_.pParent_ = &p->pParent_->transform_;		//Transformの関数内にて、親のTransformを考慮した行列の計算を行う→親のTransformが変更されたら、自身も変更する。
			//pParentは、GameObjectのこのクラスのparentになるので、
			//新規に作成したオブジェクトの親を指定したいときは、→p->pParent_としないといけない
			

		return p;	//作成したオブジェクトを返す
					//戻り値としてGameObjectのポインタをもらうことで、Instantiateを読んだクラスの中で、子供の位置を変えたり、参照ができるようにする
	}
};