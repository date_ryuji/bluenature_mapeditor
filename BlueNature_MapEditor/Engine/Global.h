//いろいろなクラスから呼ばれる可能性のあるマクロ関数などをまとめておくヘッダ


#pragma once

//ポインタの中身の解放と、ポインタ自身の解放
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
//マクロ
//→deleteの際に、ポインタが、領域のアドレスを覚えているときに、そのアドレスを忘れさせる
	//→これによって、deleteした後に、


#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}




//Release
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//Releseも同じく
//ポインタに使用されているポインタのReleaseを呼んであげる。


