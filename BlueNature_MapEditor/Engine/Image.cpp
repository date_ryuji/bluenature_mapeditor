#include "Image.h"
#include "Global.h"

namespace Image
{
	//モデルのデータを入れておく構造体の動的配列（可変長配列）
	//ImageDataの可変長配列（vector型：途中で要素を消したりしない）
	std::vector<ImageData*> datas;	//要素登録の使い方はListと同じ
										//vectorは、普通の配列のように番号で扱えるので楽。（追加もpush_back）



	//モデルのロード
	int Load(std::string fileName)
	{
		/*
			�@構造体のインスタンスを作成。
				まず、１つの構造体のインスタンスを生成
				→この中に、必要データを突っ込んで、最後に、本当のDB（ImageDataにつこむ）

			�Aそのメンバのファイル名に引数の内容を代入

			�BSpriteオブジェクトを作成し、ロードする

			�C構造体の中身が埋まったので動的配列に突っ込む
				//Modelにて確保した動的な配列（可変長配列）に作成して情報を生成した構造体を登録

			�D番号（配列の要素数-1）を返す
				//配列の要素数を出して、−１→つまり、２個入っていれば、１が入る(０オリジン、作成した構造体が追加された要素番号)

		*/

		//モデルのデータを入れる構造体のインスタンス生成
		ImageData* pData = new ImageData;	//動的確保

		//構造体メンバのファイル名に引数の内容を登録
		pData->fileName = fileName;


		/*
		すでにロードしているから、
			→
			だったら、
			Spriteのロードをするところに
			→datasの中に、Spriteが存在するならば、
			→そいつを入れてやるだけ、
			→newをしないで、Loadしないで、
			→新規に作成した構造体の中のSpriteポインタに、既に存在するSpriteのぽいんたのあドレスを入れてやる
		*/
		//vectorの動的配列を頭から回す(回し方はイテレータ必要なし（なぜなら、要素数もわかるし、vectorは、配列と同じ扱い方なので）)
		for (int i = 0; i < datas.size(); i++)
		{
			//すでに同一のファイルが存在していたらロードしない
				//同一ファイルの判断は、ファイル名から判断
			if (datas[i]->fileName == fileName)
			{
				//イテレータによって示されているdatasのメンバであるfileNameと
				//引数のfileNameが同じなら　＝　すでにSpriteファイルはロードされている

				//作成した構造体のメンバpSpriteに、ファイル名が同じのdatasのpSpriteのアドレスを代入
				pData->pSprite = datas[i]->pSprite;

				//抜ける
				break;
			}

		}

		//上記のfor分を抜けてもなお、pSpriteのポインタがnullptrならば
		//上記のfor文によって、同様のSpriteファイルを見つけられなかった
		if (pData->pSprite == nullptr)
		{
			//Spriteオブジェクトの新規作成

			//Spriteオブジェクトのインスタンス生成（ポインタによって動的確保）
			pData->pSprite = new Sprite;
			//Spriteオブジェクトのロード（メンバのファイル名を使用して）
			if (FAILED(pData->pSprite->Initialize(pData->fileName.c_str())))
			{
				//エラーの原因をメッセージボックスとして表示
				MessageBox(nullptr, "Spriteファイルのロードの失敗", "エラー", MB_OK);
				//−１を返す（ロードしたときにー１が帰ってきたら、Load側で、assartにてエラーを返すようにする。）
				return -1;
			}
		}

		//作成した構造体のインスタンスを動的配列に突っ込む
		datas.push_back(pData);

		//動的配列のサイズ（要素数）−１
			//今回作成した構造体が動的配列に登録された際の添え字が返される
		return datas.size() - 1;
	}

	//指定されたモデルデータのトランスフォームをセット
	void SetTransform(int handle, Transform & transform)
	{
		//ハンドルによって、示された、動的配列のモデルデータを選択
		//そのモデルデータの構造体が持っているTransformに引数のTransformを更新

		datas[handle]->transform = transform;

	}

	//指定されたモデルデータのSpriteを描画
	void Draw(int handle)
	{
		//ハンドルによって示されたモデルデータ
		//そのSpriteを描画させる

		//SpriteをDraw（引数にてTransformをおくる（この際のTransformは、モデルデータの自身の構造体が持っているTransform））
		datas[handle]->pSprite->Draw(datas[handle]->transform);

	}

	//モデルデータの全消去
	void AllImageDataRelease()
	{
		//動的配列内の全データの解放
		for (int i = 0; i < datas.size(); i++)
		{
			//まだ解放されていなければ
			if (datas[i] != nullptr)
			{
				//データ配列の動的確保したポインタの解放
				//まだ、他で使用されているSpriteデータなどは、解放しないようにする（以下の関数にて）
				Release(i);

			}


			//１つ１つの構造体の中身で動的確保したポインタの解放
			//SAFE_RELEASE(datas[i]->pSprite);
				//→これは、すでに、上記のRelease関数にて解放済みである。

			//動的確保した構造体自体を解放（登録した要素を解放）
			//SAFE_DELETE(datas[i]);
							//→これは、すでに、上記のRelease関数にて解放済みである。
		}
		//配列内の解放（要素の解放）
		//動的配列が、中身を持たずにどこも示さないように（宣言もされていない状態へ）
				//他のシーンにおいても使えるようにしておく

		//配列の中身をクリア
		datas.clear();
	}

	void Release(int handle)
	{

		//考えられるエラーの際に解放を行わずに帰る
		if (handle < 0 || handle >= datas.size() || datas[handle] == nullptr)
		{
			return;
		}

		//自身の所有している、
		//まだ、解放されてないSpriteのポインタをまだ持っているか
		//同じモデルを他でも使っていないか
		bool isExist = false;
		for (int i = 0; i < datas.size(); i++)
		{
			//すでに開いている場合
			if (datas[i] != nullptr &&	//まだ解放されていない
				i != handle &&			//自身と同じでない
				datas[i]->pSprite == datas[handle]->pSprite)	//Spriteクラスポインタが同じ
			{
				//解放しないとする
					//→まだ、解放されていないデータの中でSpriteポインタを所有している場合消さない
					//→データの中で、配列の一番最後に使われているSpriteファイルが解放を行うようにする。
						//→自分のデータよりも要素が前、後ろのデータの中で同じデータを持っている場合は×。

				//他での同じ画像データを使用している
				isExist = true;
				break;
			}
			//最後まで全データを回して、
			//フラグにtrueが入らなかった場合、
			//まだ、解放されていない中で同じSpriteポインタを持つものがいない　＝　Spriteポインタを解放
		}

		//解放されていない、他データ内で
		//Spriteポインタを使ってなければモデル解放
		if (isExist == false)
		{
			datas[handle]->pSprite->Release();
			SAFE_DELETE(datas[handle]->pSprite);
		}

		//自分のデータを解放（配列の要素を動的確保しているので、解放）
		SAFE_DELETE(datas[handle]);
	}


}
