#pragma once
//Spriteファイルを扱う
		//疑似フライウェイトパターンを扱う
#include <string>
#include <vector>
#include "Transform.h"
#include "Sprite.h"


//Model.h　、　Model.cppと同様に
//２Dオブジェクトのデータのロード、描画、Transformのセット
//１シーンにおける２Dオブジェクトの画像データを保存しておく動的配列を持つ
namespace Image
{
	//１シーンにて扱う画像データ
	//構造体の作成
			//使用する画像を保存しておくもの（ロードは１回しかしないとは別の話）
			//使用する画像１つ１つのデータを持っておくもの
	struct ImageData
	{
		Transform transform;
		std::string fileName;
		Sprite* pSprite;

		//構造体もコンストラクタが使用可能
		ImageData() : pSprite(nullptr)
		{}	//コンストラクタにて初期化
	};

	//ロード
	//画像のFBXをロードして、ロードを完了したら画像番号になる値を返して、
		//ロードできなかったらー１を返す
	//引数：fileName ロードする画像ファイル名
	//戻値：handle	画像を読み込んだら、その画像の番号。失敗したらー１
	int Load(std::string fileName);

	//指定された画像データのTransfromをセットする
	//トランスフォームの位置、サイズ、拡大率を受け取る
	//引数：handle トランスフォームを変更する画像番号
	//引数：transform セットするトランスフォーム(&　基本型以外のデータを受け取るときは＆を使用)
	//戻値：なし
	void SetTransform(int handle, Transform& transform);

	//指定された画像データのFbxの描画を行う
	//引数：handle 描画する画像番号
	//戻値：なし
	void Draw(int handle);

	//画像データの全消去
		//画像データは、シーンが切り替えられたときに行えばよい（１っ回１っ回クラスオブジェクトが解放と同時にFbxファイルなどのデータが解放されても困る（せっかく画像データを保存しておいた意味がなくなる））
		//１つ１つの球をロードする際に、毎回ロードしたら困るので画像データを持っておく動的配列を宣言した。（１つの球が解放時に、画像データを動的配列から消したら。意味ない）
	void AllImageDataRelease();

	//指定ハンドルのデータ解放
	void Release(int handle);

};

