#include "Model.h"
#include "Global.h"


//名前空間なので関数は、
	//Modelの名前空間に入れてもいいし、
	//クラスのように、Model::Loadの形でもどちらでも可能
	//★どちらでもいいが、自分ルールを決めたなら、プロジェクトの中では「そのルールにのっとる」
namespace Model 
{
	//モデルのデータを入れておく構造体の可変長配列
	//ModelDataの可変長配列（vector型：途中で要素を消したりしない）
	std::vector<ModelData*> datas;	//要素登録の使い方はListと同じ
										//vectorは、普通の配列のように番号で扱えるので楽。（追加もpush_back）



	//モデルのロード
	int Load(std::string fileName)
	{
		/*
			�@構造体のインスタンスを作成。
				まず、１つの構造体のインスタンスを生成
				→この中に、必要データを突っ込んで、最後に、本当のDB（ModelDataにつこむ）

			�Aそのメンバのファイル名に引数の内容を代入

			�BFbxオブジェクトを作成し、ロードする

			�C構造体の中身が埋まったので動的配列に突っ込む
				//Modelにて確保した動的な配列（可変長配列）に作成して情報を生成した構造体を登録

			�D番号（配列の要素数-1）を返す
				//配列の要素数を出して、−１→つまり、２個入っていれば、１が入る(０オリジン、作成した構造体が追加された要素番号)

		
		
		*/

		//モデルのデータを入れる構造体のインスタンス生成
		ModelData* pData = new ModelData;	//動的確保

		//構造体メンバのファイル名に引数の内容を登録
		pData->fileName = fileName;


		/*
		すでにロードしているから、
			→
			だったら、
			Fbxのロードをするところに
			→datasの中に、Fbxが存在するならば、
			→そいつを入れてやるだけ、
			→newをしないで、Loadしないで、
			→新規に作成した構造体の中のFbxポインタに、既に存在するFbxのぽいんたのあドレスを入れてやる
		*/
		//vectorの動的配列を頭から回す(回し方はイテレータ必要なし（なぜなら、要素数もわかるし、vectorは、配列と同じ扱い方なので）)
		for(int i = 0; i < datas.size(); i++)
		{
			//すでに同一のファイルが存在していたらロードしない
				//同一ファイルの判断は、ファイル名から判断
			if (datas[i]->fileName == fileName)
			{
				//イテレータによって示されているdatasのメンバであるfileNameと
				//引数のfileNameが同じなら　＝　すでにFbxファイルはロードされている

				//作成した構造体のメンバpFbxに、ファイル名が同じのdatasのpFbxのアドレスを代入
				pData->pFbx = datas[i]->pFbx;
			
				//抜ける
				break;
			}
			
		}

		//上記のfor分を抜けてもなお、pFbxのポインタがnullptrならば
		//上記のfor文によって、同様のFbxファイルを見つけられなかった
		if (pData->pFbx == nullptr)
		{
			//Fbxオブジェクトの新規作成

			//Fbxオブジェクトのインスタンス生成（ポインタによって動的確保）
			pData->pFbx = new Fbx;
			//Fbxオブジェクトのロード（メンバのファイル名を使用して）
			if (FAILED(pData->pFbx->Load(pData->fileName.c_str())))
			{
				//エラーの原因をメッセージボックスとして表示
				MessageBox(nullptr, "Fbxファイルのロードの失敗", "エラー", MB_OK);
				//−１を返す（ロードしたときにー１が帰ってきたら、Load側で、assartにてエラーを返すようにする。）
				return -1;
			}
		}

		//作成した構造体のインスタンスを動的配列に突っ込む
		datas.push_back(pData);

		//動的配列のサイズ（要素数）−１
			//今回作成した構造体が動的配列に登録された際の添え字が返される
		return datas.size() - 1;
	}

	//指定されたモデルデータのトランスフォームをセット
	void SetTransform(int handle, Transform & transform)
	{
		//ハンドルによって、示された、動的配列のモデルデータを選択
		//そのモデルデータの構造体が持っているTransformに引数のTransformを更新

		datas[handle]->transform = transform;

	}

	//指定されたモデルデータのFBXを描画
	void Draw(int handle)
	{
		//ハンドルによって示されたモデルデータ
		//そのFBXを描画させる

		//FBXをDraw（引数にてTransformをおくる（この際のTransformは、モデルデータの自身の構造体が持っているTransform））
		datas[handle]->pFbx->Draw(datas[handle]->transform);

	}

	//モデルデータの全消去
	void AllModelDataRelease()
	{
		//動的配列内の全データの解放
		for (int i = 0; i < datas.size(); i++)
		{
			//まだ解放されていなければ
			if (datas[i] != nullptr)
			{
				//データ配列の動的確保したポインタの解放
				//まだ、他で使用されているFbxデータなどは、解放しないようにする（以下の関数にて）
				Release(i);
			
			}

			/*
				ひとつづつ、
				動的確保した記憶のある、FBXのポインタを解放しようとすると
				→エラーになる


				なぜか。。。
				→FBXポインタは、すでにロードされているものは、ロードされているオブジェクトのポインタを持つようにしている
				→なので、どこかで１度解放したものまで解放しようとしてしまう

				すでに解放したものは、解放しないようにする。
				→だが、ポインタというのは、あくまでアドレスを持っているだけで、
				→配列でそれぞれ同様の位置を指すポインタを持っていても、

				１つのポインタを解放しても、
				違うポインタ変数で持っているものは、まだ、アドレスを指し続ける。（すでに解放されているのに、、、）

				ポインタはただ、「アドレスを持っているだけ。」

				であれば、
				他で解放されても、
				→そのポインタの示すアドレスは解放されている→これは、、分からない。

				
				だったら、どうするか。
				×→最初に解放して、後に解放しようとするものは、それは解放されていますか？
				〇→解放する前に、このFBXファイルは、まだ解放されていない配列のデータで使われていますか？
						→使われているならば、解放しません。
						→使われていないならば、解放します。
			
				★上記の方法をとることで、
					ポインタが解放されたアドレスを解放しようとすることはなくなる。
			*/

			//FBXファイルのReleaseを呼ぶ
			//datas[i]->pFbx->Release();
			//SAFE_RELEASE(datas[i]->pFbx);

			//１つ１つの構造体の中身で動的確保したポインタの解放
			//SAFE_DELETE(datas[i]->pFbx);

			//動的確保した構造体自体を解放（登録した要素を解放）
			SAFE_DELETE(datas[i]);
		}
		//配列内の解放（要素の解放）
		//動的配列が、中身を持たずにどこも示さないように（宣言もされていない状態へ）
				//他のシーンにおいても使えるようにしておく
		
		//可変長配列のクリア
		datas.clear();


	}

	void Release(int handle)
	{
		//考えられるエラーの際に解放を行わずに帰る
		if (handle < 0 || handle >= datas.size() || datas[handle] == nullptr)
		{
			return;
		}

		//自身の所有している、
		//まだ、解放されてないFBXのポインタをまだ持っているか
		//同じモデルを他でも使っていないか
		bool isExist = false;
		for (int i = 0; i < datas.size(); i++)
		{
			//すでに開いている場合
			if (datas[i] != nullptr &&	//まだ解放されていない
				i != handle &&			//自身と同じでない
				datas[i]->pFbx == datas[handle]->pFbx)	//FBXクラスポインタが同じ
			{
				//解放しないとする
					//→まだ、解放されていないデータの中でFBXポインタを所有している場合消さない
					//→データの中で、配列の一番最後に使われているFBXファイルが解放を行うようにする。
						//→自分のデータよりも要素が前、後ろのデータの中で同じデータを持っている場合は×。
					
				isExist = true;
				break;
			}
			//最後まで全データを回して、
			//フラグにtrueが入らなかった場合、
			//まだ、解放されていない中で同じFBXポインタを持つものがいない　＝　FBXポインタを解放
		}

		//解放されていない、他データ内で
		//Fbxポインタを使ってなければモデル解放
		if (isExist == false)
		{
			datas[handle]->pFbx->Release();
			SAFE_DELETE(datas[handle]->pFbx);
		}

		//自分のデータを解放（配列の要素を動的確保しているので、解放）
		SAFE_DELETE(datas[handle]);
	}

	//レイとの衝突判定
		//特定モデルとのレイとのしょとつ判定を行う
	void RayCast(int handle, RayCastData * rayData)
	{
		//方向ベクトルの正規化
		rayData->dir = XMVector3Normalize(rayData->dir);

		//保存用の変数
		XMVECTOR initStartPos = rayData->start;
		XMVECTOR initDirection = rayData->dir;

		////何も変更しない引数のままのレイデータだと、
		////


		////モデルは、自身の持っているTransform値によって、
		////移動、回転、拡大を行っている、
		////そのため、そのTransformに合うように、レイも移動、回転、拡大させなければいけない。

		////頂点ごとに回転などさせるよりも、
		////レイを回転させたほうが処理数少なくて済む


		////★ワールド行列の逆行列を求める
		////回転
		////回転行列
		//	//引数にて、
		//	//該当モデルは出てきているので、そのモデルのTransformに該当するようにレイもTransformする
		//		//モデルが、右に９０度回転したならば（モデルから見て）、レイはモデルから見て左回転をしなくてはいけない
		//		//＝逆行列
		//
		//XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(datas[handle]->transform.rotate_.vecX));	//X軸回転の回転行列
		//XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(datas[handle]->transform.rotate_.vecY));	//Y軸回転の回転行列
		//XMMATRIX matZ = XMMatrixRotationZ(XMConvertToRadians(datas[handle]->transform.rotate_.vecZ));	//Z軸回転の回転行列
		////３つの軸回転の行列を１つに合わせる（掛け算）
		//	//掛ける順番を守る（掛け算の順番で結果が変わってくる）
		//XMMATRIX matRotate_ = matZ * matX * matY;

		//

		////移動
		////移動行列
		//	//逆行列だが、
		//	//★初期位置を行列で計算しても、そのままのベクトル方向では正しくない。（開始位置を移動させると、方向ベクトルは、０，０、−１が方向とされるので、斜めの方向ベクトルになる。このままではまずいので、移動後の方向ベクトルを確実に出す。）

		//	//レイ発射位置と、発射位置に方向ベクトルを足した
		//	//上記の２点にそれぞれ逆行列を掛けて
		//	//移動後のレイ発射位置と、移動後の発射位置に方向ベクトルとの差ベクトルを出せば、方向ベクトルを出すことが可能である。
		//XMMATRIX matTranslate_ = XMMatrixTranslation(
		//	datas[handle]->transform.position_.vecX,
		//	datas[handle]->transform.position_.vecY,
		//	datas[handle]->transform.position_.vecZ);
		//	//初めに、通過点（発射方向に方向ベクトルを足したベクトル）のベクトルを求めて置き、
		//	//そのベクトルに行列を掛け、それと、開始位置とで差ベクトルを出せば、ワールド行列計算後の方向を出せる。


		////拡大
		////拡大行列
		//XMMATRIX matScale_ = XMMatrixScaling(
		//	datas[handle]->transform.scale_.vecX,
		//	datas[handle]->transform.scale_.vecY,
		//	datas[handle]->transform.scale_.vecZ);

		
		/*
		
		�@レイの通過点を求める（startとdirのベクトルを足したもの）
		�Aワールド行列の逆行列を求める(移動、回転、拡大を含む)
		�BrayDataのstartを�Aで変形
		�C�@を�Aで変形
		�DrayDataのdirに�Bから�Cに向かうベクトルを入れる（移動行列を行うと、単純に逆行列では方向は求められない。なので、ワールド行列を掛けた移動方向＋初期位置と、ワールド行列を掛けた初期位置のベクトルとで、差ベクトルを出せば方向を出せる）
		
		
		*/

		//�@
		//初期位置　＋　方向ベクトル
		XMVECTOR passing = rayData->start + rayData->dir;


		//�Aワールド行列の逆行列
		XMMATRIX inverceMat = XMMatrixInverse(nullptr, 
			datas[handle]->transform.GetWorldMatrix());

		//�B
		rayData->start = XMVector3TransformCoord(rayData->start, inverceMat);

		//�C
		passing = XMVector3TransformCoord(passing, inverceMat);

		//�D
		rayData->dir = XMVector3Normalize(passing - rayData->start);



		//モデルデータ内の、
		//handleにて示されるデータ内の
		//FPXデータの
		//レイキャストを呼びこむ（これで、FBXの全ポリゴンと衝突判定を行う。）
		datas[handle]->pFbx->RayCast(rayData);


		//元のレイ情報に戻す
			//レイの衝突判定のために
			//FBXのTransform値分、回転、移動、拡大を行ったので、元の情報に戻す。
		rayData->start = initStartPos;
		rayData->dir = initDirection;

	}

}