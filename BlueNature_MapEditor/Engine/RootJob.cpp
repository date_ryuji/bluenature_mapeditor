#include "RootJob.h"
/*
#include "../PlayScene.h"	//RootJobは、同じフォルダに
							//PlaySceneのヘッダファイルは、Engineの一つ上のフォルダに存在している
								//一つ上のフォルダ../
*/


//SceneManagerをインクルードして、
//SceneManagerの子供に各Sceneを作る
#include "SceneManager.h"


RootJob::RootJob()
{
}

RootJob::~RootJob()
{
}

void RootJob::Initialize()
{
	//SceneManagerを実装せずに、直接RootJobクラスの子供にSceneを作る場合
	{
		/*
		{
			/*仮でプレイシーンを作る
			//RootJob内にて、PlaySceneを作る
			//

			//プレイシーンを作る（ポインタ宣言）
			PlayScene* pPlayScene;
			//プレイシーンの親を引数として送る（コンストラクタにて、親を受け取れる形のコンストラクタとしてPlaySceneとして宣言しておく→その引数のポインタを）
			//プレイシーンの親＝自分（宣言している自分だ＝tihs）
			pPlayScene = new PlayScene(this);


			pPlayScene->Initialize();	//初期化を呼ぶ

			//プレイシーンは、自分の子供だ、
				//だったら、自分の子供ならば、子供のリストに登録する
				//GameObjectを継承しているので、protectedにある、子供のリストに追加
			childList_.push_back(pPlayScene);
			//childList_は、GameObjectの型だが、
			//RootJobの子供は、ゲーム下のゲームオブジェクトは、すべてGameObjectのクラスを継承している
			//であれば、PlaySceneも、GameObjectを継承している、
			//→引数にPlaySceneを入れることで、引数をGameObjectで受け取ることで、PlaySceneをGameObjectでアップキャストする
		}
		*/
		//以上の書き方は、
		//どのクラスでも、プレイヤーを作るときでも、同じ書き方をしている

		//去年はInstantiate<>();という関数で１行で終わらせていた
			//→つまり、→この１行で、以上の４行の処理を行っていた

		//そのまま関数にしよう
		//→親子関係を作る関数というのは、PlaySceneだけではない
		//これは関数のテンプレート
		//＜＞に自身のクラスを入れて、（）に親を入れると、どの方にも対応できる関数となる

		//＜＞に好きなクラス


		//＜＞にクラスを入れることで、
		//template<class T>のTに、関数の呼び出し時に設定した＜クラス名＞を設定することで、
		//→呼び出し時に入力した＜クラス＞が、Tに入る
		//→template以下の関数にて、Tは、＜クラス＞内のクラスとして扱われることになる


		//↓
		//親子関係を築く
		//＜＞にクラス名を入れて、
		//後から、GameObjectのクラスに書かれているtemplate<class T>
			//のTの部分にPlaySceneを入れることができるようになる
			//→これで、Tは、PlaySceneと指定することができる
		//★Tは何もわからないけど、あとから指定するので、
		//★どの型にでも対応できるInstantiateのクラスとなる
		
		{
			//Instantiate<PlayScene>(this);
		}
	}

	Instantiate<SceneManager>(this);
}

void RootJob::Update()
{
	GameObject* test = FindChildObject("Bullet");

}

void RootJob::Draw()
{
}

void RootJob::Release()
{
}
