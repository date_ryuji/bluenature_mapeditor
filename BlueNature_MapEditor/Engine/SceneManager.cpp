#include "SceneManager.h"
#include "Model.h"
#include "Image.h"

//SceneManagerにて、シーンをインクルードして親子付け

#include "../TestScene.h"
#include "../SelectGroundScene.h"
#include "../AjustmentInfoScene.h"


//親のInstantiateの関数から
	//親のポインタをもらうコンストラクタをもらって、（継承元）GameObjectクラスの引数２つのコンストラクタを呼ぶ
SceneManager::SceneManager(GameObject * parent)
	:GameObject(parent, "SceneManager")
{
}

SceneManager::~SceneManager()
{
}

void SceneManager::Initialize()
{
	//プレイシーンの親子付け
	//Instantiate<PlayScene>(this);

	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_SELECT_GROUND;
	//次のシーンも現在のシーンに、→これで、Updateが呼ばれてもシーンが切り替わることはない
	nextSceneID_ = currentSceneID_;
	//インスタンスを生成（SceneManagerの子供に親子付）
	Instantiate<SelectGroundScene>(this);


}

//シーン切り替えの条件がそろっていたら
//シーン切り替えが行われる
void SceneManager::Update()
{
	//シーンIDが違うならシーンを切り替える
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
			//GameObjectの所有する関数で、
			//自身の持っている子供リストの子供を全消去
		//シーンのオブジェクトが消去されるので、現在のシーンはいなくなる
		AllChildrenKill();

		//ロードしたデータを全削除
		Model::AllModelDataRelease();
		Image::AllImageDataRelease();


		//次のシーンを作成
		//次フレームからプレイされるシーン
		switch (nextSceneID_)
		{
		case SCENE_ID_TEST: Instantiate<TestScene>(this); break;
		case SCENE_ID_SELECT_GROUND : Instantiate<SelectGroundScene>(this); break;
		case SCENE_ID_AJUST_INFO: Instantiate<AjustmentInfoScene>(this); break;

		}

		//現在のシーンを次のシーンに代入
			//次フレームからは、ChangeSceneが呼ばれない限り
			//現在のシーンと次のシーンが同じなので、Updateが呼ばれても、シーンが切り替わることはない。
		currentSceneID_ = nextSceneID_;

	}

}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID changeSceneID)
{
	//次のシーンIDに引数の値を代入
		//次のフレームのUpdateにてシーン切り替えの実行
	nextSceneID_ = changeSceneID;

}
