#include "SphereCollider.h"

//コンストラクタによる初期化
SphereCollider::SphereCollider(XMVECTOR position, float size):
	position_(position) , 
	objectPos_(XMVectorSet(0,0,0,0)),
	radius_(size)
{
}

XMVECTOR SphereCollider::GetWorldPos()
{
	//オブジェクト位置＋コライダー位置
	return objectPos_ + position_;
}

void SphereCollider::SetObjectPos(XMVECTOR objectPosition)
{
	//オブジェクト位置の更新
	objectPos_ = objectPosition;
}

//自身のコライダーとの衝突判定
bool SphereCollider::CollisionDetection(XMVECTOR anotherCollPos,
	float radius)
{
	//今回採用する衝突判定の方法
	/*
		・ベクトル型を使用した衝突判定の計算

		自身のベクトルをA、引数のベクトルをBとする。
		�@２つのベクトルの差分をとって、AからB、あるいはBからAに延びるベクトルを求める。
		�Aベクトルの長さを求める（ベクトルで）
		
		�B求めたベクトルの、長さと、A・Bの半径の合計より
			・以下の時
			　→衝突しているので、trueを返す
			・大きいとき
			　→衝突していないので、falseを返す
	*/

	//�@
	//anotherCollPos = 自身と衝突判定を行うコライダーの位置
	//GetWorldPos() = 自身のコライダーのワールド位置を取得（外部に渡すための関数だったが、使えるなら使う）
	XMVECTOR length = anotherCollPos - GetWorldPos();
	
	//�A
	length = XMVector3Length(length);

	//２つのコライダーの半径の合計を出す
		//（２つの半径より、２コライダー間の距離が長いなら）　＝　衝突していない
		//（２つの半径より、２コライダー間の距離が短いなら）　＝　衝突している
	float sumRadius = radius_ + radius;


	//�B
		//×長さは、ベクトルで出てくるのでX、Y、Zで判断を行い、
		/*
			if (length.vecX <= sumRadius || length.vecY <= sumRadius ||
				length.vecZ <= sumRadius)
		*/
		//〇Lengthの時点で、→ｘｙｚすべてに共通の値が入る、xyzどの値を使っても問題なし
		/*
			if (length.vecX <= sumRadius)		
		
		*/
	//半径の合計より小さければ、衝突していることになる
	if (length.vecX <= sumRadius)
	{
		//１つでも小さいものがあるなら、
		//衝突している
		return true;
	
	}
	/*
		・半径 1の球コライダー
		・半径 5の球コライダー

		上記の２つが衝突されているかを判定しろ

		・上記２つの直線距離は４である

		２つの半径の合計は６
		２つの直線の距離は４

		〇２つが衝突しているときの最高の距離は６（２つの半径の合計が６なので）
		〇２つの直線距離は４
		〇２つの半径合計より距離が小さいので

		★上記の２つの球は衝突している

	
	
	*/

	//衝突しなかった
	return false;
}
