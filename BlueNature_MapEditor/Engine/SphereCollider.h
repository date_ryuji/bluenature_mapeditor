//当たり判定を行うクラス

#pragma once
#include <DirectXMath.h>

#define vecX m128_f32[0]
#define vecY m128_f32[1]
#define vecZ m128_f32[2]

using namespace DirectX;

class SphereCollider
{
private :
	//オブジェクトからのコライダーの位置（ワールドではない）
	XMVECTOR position_;

	//オブジェクトの位置
	XMVECTOR objectPos_;

	//コライダーのサイズ（半径の長さ）
	float radius_;





public : 
	

	//コンストラクタ
	//引数：位置
	//引数：サイズ
	//戻値：なし
	SphereCollider(XMVECTOR position , float size);


	//コライダーのワールド座標を返す
		//戻値は、コライダーをつけたオブジェクト位置＋オブジェクトからのコライダーの位置（つまりコライダーのワールド座標が出せる）
	//引数：なし
	//戻値：自身のコライダーのワールド座標
	XMVECTOR GetWorldPos();

	//コライダーのサイズ（半径）を返す
	//引数：なし
	//戻値：自身のコライダーの半径
	float GetRadiusSize(){ return radius_;}

	//コライダーをつけた親の位置をセットする（毎フレーム）
		//毎フレーム衝突判定を行う前に、
		//コライダーを所有しているオブジェクトの位置をセットしてもらう（コライダーのワールド座標は、オブジェクトの位置＋オブジェクトからのコライダーの位置）
	//引数：コライダーを付加しているオブジェクトのワールド座標
	//戻値：なし
	void SetObjectPos(XMVECTOR objectPosition);



	//衝突判定
		//自身の所有しているコライダー位置（オブジェクト位置+コライダー離れ具合位置）と、
		//引数にてもらった、他コライダー位置とが、衝突しているかの判定
	//引数：自身のコライダーと衝突判定を行うコライダーの位置（コライダーのワールド座標（コライダーの中心））
	//		（標準の型（intなど）以外の型は、＆をつける）			
	//引数：衝突判定を行うコライダーの半径
	//戻値：衝突しているか
	bool CollisionDetection(XMVECTOR anotherCollPos , float radius);

};

