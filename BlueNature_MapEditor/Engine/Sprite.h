#pragma once
//Sprite = 画像

//★★画像であっても、２Dのポリゴンを表示させるだけ★★★★
//Quadの３D情報のクラスを、そのままコピーしてきて、
	//３Dにおける、２Dにいらない情報→法線など、（合成行列）ビュー行列などなど　を、削除していく


//Main.cppから、Spriteを呼べばいい
	//その時、位置などを、行列で送ってもらう（ワールドぎゅれつ）


#pragma once

#include <DirectXMath.h>
#include "Direct3D.h"
#include "Texture.h"
#include "Transform.h"	//Transformクラスの、移動、回転、拡大行列を使用するため
using namespace DirectX;


#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}
//解放




class Sprite
{
protected:
	//Quadでも、Spriteでも、こぴーしたので、　同じ構造体を使用している→なので、　→クラスの中に入れればいい
			//だが、継承先でも、Quadの構造体はしようしたいので、　継承先でも自由に使えるようにprotectedへ
	struct CONSTANT_BUFFER
	{
		//合成行列いらず	（カメラのどの位置などないので）

		XMMATRIX	matW;	//ワールド行列
	};
	//シェーダーにて、受け取るもののために、構造体で取得
		//現在は、一つしかもっていないが、

	//頂点情報
	//頂点に、位置と、UVがひつようなので、　構造体で取得

	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;

		//法線いらず
	};


	//継承先で使用

	ID3D11Buffer *pVertexBuffer_;	//頂点バッファ
	ID3D11Buffer *pIndexBuffer_;	//インデックスバッファ
	ID3D11Buffer *pConstantBuffer_;	//コンスタントバッファ

		//ポインタのアスタリスクの場所
		//Class *class1;
		//Class* class2;		//どちらも同じ、だが、どっちかにするなら、書き方は共通した書き方をする
	Texture*	pTexture_;


	int vertex_;	//	頂点数
					//継承先ごとで、違う頂点数
	int *index;

public:

	Sprite();

	~Sprite();

	//継承先でオーバーライドする関数
	HRESULT Initialize(std::string fileName);//Spriteの初期化（モデル描画のための初期化）
	virtual HRESULT InitVertex();//頂点情報の初期化

	virtual HRESULT TextureLoad(std::string fileName);

	//オーバーライドの必要はないため、Virtualなし
	//HRESULT Draw(XMMATRIX& worldMatrix);	//モデル描画
	HRESULT Draw(Transform transform);		//Transform型を引数としたDraw関数（Transformよりそれぞれ、行列を取得する）
	void Release();
};


