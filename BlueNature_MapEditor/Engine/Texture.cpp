#include <wincodec.h>
#include "Texture.h"
#include "Direct3D.h"



#pragma comment( lib, "WindowsCodecs.lib" )


	Texture::Texture()
	{
		//初期化
		pSampler_ = nullptr;	
		pSRV_ = nullptr;
		imgWidth_ = 0;
		imgHeight_ = 0;

	}

	Texture::~Texture()
	{
	}

	/*
	HRESULT Texture::Load(std::string fileName)
	{
		return S_OK;	//HRESULT型で、とりあえず、OK返しておく
	}
	*/

	


	HRESULT Texture::Load(std::string fileName)
	{
		//stringで受け取ったものを、ワイド文字で受け取る
		wchar_t wtext[FILENAME_MAX];
						//ワイドのキャラ型、FILENAME_MAXは、ただの２６０という値→（２６０文字以上にファイル名が多くなることは内だろうという）
		size_t ret;
		mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());
			//マルチバイトをワイド文字に変換する関数
			//multi byte string to wide ~ ？
				//第一引数は必要なもの　fileNameを　wtextに、入れて、どのぐらいの長さになるのか


		//COM使うため(WIC)
		CoInitialize(nullptr);	//COMを使うなら、これが必要

		IWICImagingFactory *pFactory = nullptr;	//読み込んで、
		IWICBitmapDecoder *pDecoder = nullptr;	//デコーダーがあって、
		IWICBitmapFrameDecode* pFrame = nullptr;
		IWICFormatConverter* pFormatConverter = nullptr;
		if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&pFactory))))
		{
			//失敗時の処理
			MessageBox(nullptr, "WICのインスタンス生成の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		
		}
		
		
		HRESULT hr = pFactory->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder);
		//ファイル名を、自分で好きなものを受け取ったものを入れる		
			//だが、ファイル名の型は→、ワイド文字でないといけない（ワイド文字→どの文字も２バイトで受け取る）
			//関数の頭で、Stringのファイル名をワイド文字にする　wtext そのワイド文字を使用
		if (FAILED(hr))
		{
			MessageBox(nullptr, "画像ファイルの読み込み失敗", "エラー", MB_OK);
			//ここにおける、エラーというのは、ファイルを読み込めなかった、ファイルが存在しなかった可能性が高い
			return E_FAIL;	//エラーを返す
		}


		if (FAILED(pDecoder->GetFrame(0, &pFrame)))
		{
			//失敗時の処理
			MessageBox(nullptr, "フレーム取得の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		if (FAILED(pFactory->CreateFormatConverter(&pFormatConverter)))
		{
			//失敗時の処理
			MessageBox(nullptr, "コンバーターの初期化失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		if (FAILED(pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut)))
		{
			//失敗時の処理
			MessageBox(nullptr, "WIC初期化の失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		
		}
		
		CoUninitialize();	//最後に、COMを使うなら、これを使う


		//画像のサイズを調べる
		//UINT　unsigned int の略
		//UINT imgWidth;		//ヘッダにて、画像サイズを、送るために、メンバで取得しておく
		//UINT imgHeight;
									//メンバの変数に、画像のサイズを取得
		if (FAILED(pFormatConverter->GetSize(&imgWidth_, &imgHeight_))) 
		{
			//失敗時の処理
			MessageBox(nullptr, "画面サイズ取得失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		

		

		//テクスチャ生成
		ID3D11Texture2D*	pTexture;	//テクスチャを入れる型
		D3D11_TEXTURE2D_DESC texdec;
		texdec.Width = imgWidth_;	//画像の横幅
		texdec.Height = imgHeight_;	//画像の縦幅
		texdec.MipLevels = 1;	//Mip　奥に行けば小さくなる→テクスチャ、ポリゴンを、近くにいるときは、高解像度のテクスチャを用意して、それを距離ごとに入れていけば、処理を見た目的によく見せて、軽く（遠くに同じきれいな、テクスチャを使ってはもったいない）
		texdec.ArraySize = 1;
		texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texdec.SampleDesc.Count = 1;
		texdec.SampleDesc.Quality = 0;
		texdec.Usage = D3D11_USAGE_DYNAMIC;
		texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		texdec.MiscFlags = 0;

		if (FAILED(Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, &pTexture)))
		{
			//失敗時の処理
			MessageBox(nullptr, "テクスチャの生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		


		//コンテキスト（絵を描く人）に渡す準備
		D3D11_MAPPED_SUBRESOURCE hMappedres;
		if (FAILED(Direct3D::pContext->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres)))
		{
			//失敗時の処理
			MessageBox(nullptr, "コンテキストのマップ失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		if (FAILED(pFormatConverter->CopyPixels(nullptr, imgWidth_ * 4, imgWidth_ * imgHeight_ * 4, (BYTE*)hMappedres.pData)))
			//横一列で何ピクセルか、何バイトか、幅＊RGBAのための＊４
		{

			//失敗時の処理
			MessageBox(nullptr, "ピクセル数（列）サイズ取得失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}								
		Direct3D::pContext->Unmap(pTexture, 0);

		//サンプラー
		//テクスチャを貼れてから確認するとわかりやすい
		D3D11_SAMPLER_DESC  SamDesc;
		ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));
		SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		/*
		if (FAILED(Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "サンプラーの生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		*/
		Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_);


		//シェーダーリソースビュー
			//シェーダーへの橋渡し（〜ビューは橋渡し）
		D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
		srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srv.Texture2D.MipLevels = 1;
		if (FAILED(Direct3D::pDevice->CreateShaderResourceView(pTexture, &srv, &pSRV_)))
		{
			//失敗時の処理
			MessageBox(nullptr, "シェーダー生成失敗", "エラー", MB_OK);
			return E_FAIL;	//エラーを返す
		}
		pTexture->Release();

		return S_OK;
	}

	


	void Texture::Release()
	{
		//逆で解放
		//ID3D11型なので、RELEASEにて解放
		SAFE_RELEASE(pSRV_);
		SAFE_RELEASE(pSampler_);

	}



