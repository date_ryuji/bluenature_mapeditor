#pragma once
#include <d3d11.h>
#include "string"

class Texture
{
	ID3D11SamplerState*	pSampler_;	//サンプラー　テクスチャを入れる〜
	ID3D11ShaderResourceView*	pSRV_;	//~ビュー　絵描きが、紙に書くための橋渡し、（プリンターのような人）
										//ピクセルシェーダーに「橋渡しをする人」

	//画像のサイズ保存
	UINT imgWidth_;	//画像の横幅
	UINT imgHeight_;	//画像の縦幅

public:
	Texture();
	~Texture();
	HRESULT Load(std::string fileName);
					//標準を使うためにstd

	//ゲッター
	ID3D11SamplerState* GetSampler() { return pSampler_; };	//サンプラーを送る
	ID3D11ShaderResourceView* GetSRV() { return pSRV_; };	//サンプラーの橋渡しを送る

	UINT GetWidth() { return imgWidth_; };	//画像の横幅を送る
	UINT GetHeight() { return imgHeight_; };	//画像の縦幅を送る

	void Release();
};