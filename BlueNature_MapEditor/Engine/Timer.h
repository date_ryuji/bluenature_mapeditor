﻿#pragma once

//シーンの経過時間・デルタタイムを管理するクラス
namespace Timer 
{
	
	// シーンがスタートしてからの経過時間(sec)
	float	GetElapsedSecounds();

	// フレームのデルタタイム(sec)
	float	GetDelta();

	// シーンタイマーのリセット
	void	Reset();

	// デルタタイムの更新
	void	UpdateFrameDelta();
};
