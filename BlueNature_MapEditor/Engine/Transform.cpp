#include "Transform.h"
#include "Direct3D.h"	//ウィンドウの広さを取得するため
						//Direct３Dは、名前空間→なので、publicもprivateも存在しない
						//Direct3D::　と書けばアクセス可能


using namespace Direct3D;	//以下では、Direct３D::を省略できる


Transform::Transform(): 
	pParent_(nullptr)
{
	//何もしないベクトル、行列にする
	//ポジションと回転は０００
	//拡大率は１１１

	position_ = XMVectorSet(0, 0, 0, 0);
	rotate_ = XMVectorSet(0, 0, 0, 0);	//何もしない回転
	scale_ = XMVectorSet(1, 1, 1, 0);	//拡大率１倍（拡大率なので１倍である（注意））

	matTranslate_ = XMMatrixIdentity();	//何もしない行列に
	matRotate_ = XMMatrixIdentity();	//回転行列	
	matScale_ = XMMatrixIdentity();
}

Transform::~Transform()
{
}

void Transform::Calclation()
{
	//行列を作成する

	//移動行列
	//position_を使用して、matTranslate_に移動行列を作成したい
		//position_.vecXでｘ分だけ移動させたい→でも１年生で使用したようなvecXというのが、存在していないのでエラーになる
		//position_.m128_f32[0] = ｘ成分が入っている
		//→本来なら、こう書かなくてはいけない→だが、いちいち書いていられないので、#defineで定数かしてしまう→
	matTranslate_ = XMMatrixTranslation(
		position_.vecX,
		position_.vecY,
		position_.vecZ);
		//＋X１移動の行列　＊　＋X１移動の行列
			//+X２移動の行列



	//回転行列
	//回転行列はｘ軸回転の行列、y軸回転の行列と３つ作って
		//→それぞれ３つを最終的に掛け算したものを入れないといけない
		//rotateには、度で入ってくるので、Radianに直すことも忘れず
	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));	//X軸回転の回転行列
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));	//Y軸回転の回転行列
	XMMATRIX matZ = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));	//Z軸回転の回転行列
	//３つの軸回転の行列を１つに合わせる（掛け算）
		//掛ける順番を考えないといけない（掛け算の順番で結果が変わってくる）
	matRotate_ = matZ * matX * matY;
		/*
			Unityだと、ｚ＊ｘ＊ｙの順番で掛けられている
			→★エンジンによって、掛ける順を替えないといけない→そもそも、一つの変数にまとめてしまっているのもよくないかも
				→１つ１つ、軸回転の行列をメンバとして持っておく必要があるかも
		*/

	//拡大行列（移動行列）
	matScale_ = XMMatrixScaling(
		scale_.vecX, 
		scale_.vecY, 
		scale_.vecZ);

}

//移動＊回転＊拡大　の３つの行列を掛けて返す
XMMATRIX Transform::GetWorldMatrix()
{
	//３つの行列を掛けるが、
	//ここでもかける順番が大事

	//回転は原点からの回転なので、先に移動してしまうとおかしなことになるのは明白
		//考えてみよう

	//親がいない場合は自身の行列のみ返す（一番上の親まで自身と親の行列を返してもらう）
	if (pParent_ == nullptr)
	{
		return matScale_ * matRotate_ * matTranslate_;	//自身のワールド行列（拡大＊回転＊移動）

	}
	return matScale_ * matRotate_ * matTranslate_ * pParent_->GetWorldMatrix();
		//拡大＊回転＊移動	→が標準の掛け方

		//再帰が非常に便利
		//子供＊親にしないと本来の目的の形を表現できない
		//子供　＊　pParent_->GetWorldMatrix()にすれば！
			//→親のGetWorldMatrixで、親の行列がもらえる。→さらに親のGetWorldMatrixをもらおうとするので、再帰的に一番上の親までの親の行列を掛けたWorld行列をもらえる
			//子供＊親の順番で行列の掛け算をしないといけないので、この再帰の形が便利になる
}
	

/*
	右に　１ｍ
	４５度　回転
	横	　　２倍

	移動してから、　回転すると→原点からの回転なので、違う
	回転してから、拡大すると→斜めの状態から、拡大すると求めるものとは違う

*/
