#include "FishBase.h"
#include "Engine/Camera.h"

#include "Engine/Global.h"

#include "Engine/Math.h"


//３角形２つの正方形で表現

//その正方形にテクスチャを張り付けて、
//ゲームのオブジェクトとして表現する





//コンストラクタ
FishBase::FishBase(GameObject * parent)
	:FishBase(parent , "FishBase")
{


}

//コンストラクタ
FishBase::FishBase(GameObject * parent , std::string objectName)
	: GameObject(parent, objectName),
	pVertexBuffer_(nullptr), pIndexBuffer_(nullptr),pConstantBuffer_(nullptr),
	indexCount_(6),
	objectName_(objectName)
{


}

FishBase::~FishBase()
{
	//BaseRelease();
}

//初期化
void FishBase::Initialize()
{

	

	InitQuad();


}

HRESULT FishBase::InitQuad()
{

	if (FAILED(InitVertex()))
	{
		MessageBox(nullptr, "モデルの頂点情報の初期化失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}


	if (FAILED(InitIndex()))
	{
		MessageBox(nullptr, "モデルのインデックス情報の初期化失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}


	if (FAILED(InitConstantBuffer()))
	{
		MessageBox(nullptr, "モデルのコンスタントバッファ情報の初期化失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}

	Load();

	return S_OK;
}


HRESULT FishBase::InitVertex()
{
	int vertexCount = 4;

	pVertex_ = new VERTEX[vertexCount];




	// 頂点情報
	//構造体で、　位置、UV
	VERTEX vertices[] =

	{
		{XMVectorSet(-2.5f,  2.5f, 0.0f, 0.0f),
		XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（左上）

	{XMVectorSet(2.5f,  2.5f, 0.0f, 0.0f),
	XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（右上）

	{XMVectorSet(2.5f, -2.5f, 0.0f, 0.0f),
	XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（右下）

	{XMVectorSet(-2.5f, -2.5f, 0.0f, 0.0f),
	XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下）		

	};

	memcpy(pVertex_, vertices, sizeof(VERTEX) * vertexCount);
	//メンバ変数のポインタのアドレスへ、　配列の先頭アドレスを　第三引数分のサイズコピー
		/*
		https://bituse.info/c_func/56

		void *memcpy(void *buf1, const void *buf2, size_t n);

		第一引数にコピー先のメモリブロックのポインタ
		第二引数にコピー元のメモリブロックのポインタ
		第三引数はコピーサイズ

		char buf[] = "ABCDDEFG";
		char buf2[] = "123456789";
		//3バイトだけコピー
			memcpy(buf,buf2,3);

		
		*/



	// 頂点データ用バッファの設定
	D3D11_BUFFER_DESC bd_vertex;

	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount;

	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	bd_vertex.CPUAccessFlags = 0;

	bd_vertex.MiscFlags = 0;

	bd_vertex.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA data_vertex;

	data_vertex.pSysMem = vertices;


	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;	//エラーですと返す。

	}


	//インデックス情報
	//メンバのインデックス情報を宣言
	//→インデックス情報では、各モデルごとに頂点は違うため、オーバーライドの関数内でそれぞれ作成してもらう
	pIndex_ = new int[indexCount_] {  0, 1, 3, 3, 1, 2 };	//時計回りに、頂点をとる

	return S_OK;
}



HRESULT FishBase::InitIndex()
{
	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;

	bd.Usage = D3D11_USAGE_DEFAULT;

	bd.ByteWidth = sizeof(int) * (indexCount_);

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	bd.CPUAccessFlags = 0;

	bd.MiscFlags = 0;



	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = pIndex_;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	//成功したかの判断
	//失敗したら、呼び出し元の関数にエラーを返す
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}
	//Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_);
	//インデックスバッファ（領域）を作成

	return S_OK;

}

HRESULT FishBase::InitConstantBuffer()
{

	D3D11_BUFFER_DESC cb;
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;

	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{

		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_YESNO);
		return E_FAIL;
	}
	// コンスタントバッファの作成

	return S_OK;
}


void FishBase::Load()
{
	//ファイルパス
	std::string filePath = "Assets/Fish/" + objectName_ + "/" 
		+ objectName_ + ".png";



	//ファイルのロード
	//テクスチャのロード
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(filePath)))
	{
		MessageBox(nullptr, "ファイルの読み込みを失敗", "エラー", MB_OK);
		return;
	}
}

//更新
void FishBase::Update()
{
}

//描画
void FishBase::Draw()
{
	//シェーダーを選択
	Direct3D::SetShaderBundle(SHADER_3D);

	CONSTANT_BUFFER cb;
	cb.matWVP = XMMatrixTranspose(transform_.GetWorldMatrix() 
		* Camera::GetViewMatrix() *Camera::GetProjectionMatrix());

	//法線を回転させるため、回転行列を送るために、worldMatrixを送る×（Mainから、回転行列を受け取っているので、）
	cb.matW = XMMatrixTranspose(transform_.matRotate_ 
		* XMMatrixInverse(nullptr, transform_.matScale_));



	D3D11_MAPPED_SUBRESOURCE pdata;
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))
	{
		MessageBox(nullptr, "コンスタントバッファのマップの書き込み失敗", "エラー", MB_YESNO);
		return;	
	}
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	// データを値を送る


	
	//テクスチャのサンプラーの取得
	ID3D11SamplerState* pSampler = pTexture_->GetSampler();	//テクスチャクラスあkら、サンプラーを取得
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);
	//シェーダーへの橋渡しのビューを取得
	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();	//テクスチャのゲッターから、橋渡しのビューを受け取る
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


	Direct3D::pContext->Unmap(pConstantBuffer_, 0);



						//頂点バッファ（頂点を持っておくための領域（確保場所））
		//複数の頂点バッファを持つことができるので、　どの頂点バッファを使うのか、
	UINT stride = sizeof(VERTEX);	//1頂点のサイズを指定する、→位置のみであれば、XMVECTORの構造体であったが、
								//位置とUVで構造体をとるので、その構造体のサイズで
	UINT offset = 0;
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	// インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//コンスタントバッファ
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用


	//描画
	//★頂点の個数を表す、→ここの頂点の数を、最終的な三角形の頂点の数
	//★この情報を追加しないと、頂点による、三角形を描画してくれない（たとえ頂点情報、インデックス情報を変更しても。。。）
	Direct3D::pContext->DrawIndexed(indexCount_, 0, 0);	//頂点が６つなので、６つ


}

void FishBase::BaseRelease()
{

	SAFE_DELETE(pTexture_);
	delete[] pIndex_;	
	delete[] pVertex_;
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

//開放
void FishBase::Release()
{


}

//レイとの衝突判定
		//自身のポリゴン群とレイとの衝突を行わせる
		//判定の前に、自身のTransformをレイに反映させてレイの発射としなくてはいけない。
void FishBase::RayCast(RayCastData * rayData)
{
	//方向ベクトルの正規化
	rayData->dir = XMVector3Normalize(rayData->dir);

	//保存用の変数
	XMVECTOR initStartPos = rayData->start;
	XMVECTOR initDirection = rayData->dir;


	//モデルは、自身の持っているTransform値によって、
	//移動、回転、拡大を行っている、
	//そのため、そのTransformに合うように、レイも移動、回転、拡大させなければいけない。

	//頂点ごとに回転などさせるよりも、
	//レイを回転させたほうが処理数少なくて済む


//	//★ワールド行列の逆行列を求める
//	//回転
//	//回転行列
//		//引数にて、
//		//該当モデルは出てきているので、そのモデルのTransformに該当するようにレイもTransformする
//			//モデルが、右に９０度回転したならば（モデルから見て）、レイはモデルから見て左回転をしなくてはいけない
//			//＝逆行列
//
//	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));	//X軸回転の回転行列
//	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));	//Y軸回転の回転行列
//	XMMATRIX matZ = XMMatrixRotationZ(XMConvertToRadians(transform_.rotate_.vecZ));	//Z軸回転の回転行列
//	//３つの軸回転の行列を１つに合わせる（掛け算）
//		//掛ける順番を守る（掛け算の順番で結果が変わってくる）
//	XMMATRIX matRotate_ = matZ * matX * matY;
//
//
//
//	//移動
//	//移動行列
//		//逆行列だが、
//		//★初期位置を行列で計算しても、そのままのベクトル方向では正しくない。（開始位置を移動させると、方向ベクトルは、０，０、−１が方向とされるので、斜めの方向ベクトルになる。このままではまずいので、移動後の方向ベクトルを確実に出す。）
//
//		//レイ発射位置と、発射位置に方向ベクトルを足した
//		//上記の２点にそれぞれ逆行列を掛けて
//		//移動後のレイ発射位置と、移動後の発射位置に方向ベクトルとの差ベクトルを出せば、方向ベクトルを出すことが可能である。
//	XMMATRIX matTranslate_ = XMMatrixTranslation(
//		transform_.position_.vecX,
//		transform_.position_.vecY,
//		transform_.position_.vecZ);
//	//初めに、通過点（発射方向に方向ベクトルを足したベクトル）のベクトルを求めて置き、
//	//そのベクトルに行列を掛け、それと、開始位置とで差ベクトルを出せば、ワールド行列計算後の方向を出せる。
//
//
////拡大
////拡大行列
//	XMMATRIX matScale_ = XMMatrixScaling(
//		transform_.scale_.vecX,
//		transform_.scale_.vecY,
//		transform_.scale_.vecZ);


	/*

	�@レイの通過点を求める（startとdirのベクトルを足したもの）
	�Aワールド行列の逆行列を求める(移動、回転、拡大を含む)
	�BrayDataのstartを�Aで変形
	�C�@を�Aで変形
	�DrayDataのdirに�Bから�Cに向かうベクトルを入れる（移動行列を行うと、単純に逆行列では方向は求められない。なので、ワールド行列を掛けた移動方向＋初期位置と、ワールド行列を掛けた初期位置のベクトルとで、差ベクトルを出せば方向を出せる）


	*/

	//�@
	//初期位置　＋　方向ベクトル
	XMVECTOR target = rayData->start + rayData->dir;


	//�Aワールド行列の逆行列
	XMMATRIX inverceMat = XMMatrixInverse(nullptr,
		transform_.GetWorldMatrix());

	//�B
	rayData->start = XMVector3TransformCoord(rayData->start, inverceMat);

	//�C
	target = XMVector3TransformCoord(target, inverceMat);

	//�D
	rayData->dir = target - rayData->start;



	//モデルデータ内の、
	//handleにて示されるデータ内の
	//FPXデータの
	//レイキャストを呼びこむ（これで、FBXの全ポリゴンと衝突判定を行う。）
	RayCastWithPolygons(rayData);


	//元のレイ情報に戻す
		//レイの衝突判定のために
		//FBXのTransform値分、回転、移動、拡大を行ったので、元の情報に戻す。
	rayData->start = initStartPos;
	rayData->dir = initDirection;

}


void FishBase::RayCastWithPolygons(RayCastData * rayData)
{

	//インデックス情報毎でもやることは出来るが、
	//今回はポリゴン毎に情報を取得しているので、
		//そのポリゴンごと判定していく形でいいのでは？
	//衝突したら、そのポリゴン番号を返せばよいので

/*
		for (DWORD j = 0; j < indexCountEachMaterial_[i] / 3; j++)
		{
			//indexは、マテリアルごとに取得している
				//つまり、indexの行はi
			//列は、ポリゴン事取得するので、
			//インデックス情報は、三角形を構成する頂点が時計回りに順番に並んでいる。ということは０番め、１番目、２番目が１つの三角形を示す。→この流れがポリゴン数　毎　続く
			// j = 0
				//０番目＝　j * 3
				//１番目＝　j * 3 +  1
				//２番目＝　j * 3 +  2
			//これでインデックス情報の頂点番号が取得できる
				//頂点番号を、頂点情報の引数にして、位置を取得
			// j = 1
				//３番目＝　j * 3
				//４番目＝　j * 3 +  1
				//５番目＝　j * 3 +  2


			//3頂点の位置情報を取得
				//インデックス情報は、
				//あらかじめ、時計回りで入っている。
			XMVECTOR v0 = pVertices_[ppIndex_[i][(j * 3) + 0]].position;
			XMVECTOR v1 = pVertices_[ppIndex_[i][(j * 3) + 1]].position;
			XMVECTOR v2 = pVertices_[ppIndex_[i][(j * 3) + 2]].position;

			//その三角形との判定
				//レイの情報は引数にてもらっているので、
				//その情報と、
				//三角形の頂点の情報を与えて、接触判定を行う。
			rayData->hit = Math::Intersect(
				rayData->start,		//レイ発射位置
				rayData->dir,			//レイの方向
				v0,					//三角形頂点０
				v1,					//三角形頂点１
				v2, 					//三角形頂点２
				&rayData->dist);		//レイと面の接触位置までの長さ


			//当たってたら終わり
			if (rayData->hit)
			{
				return;
			}
		}
	}


	*/

	//ポリゴンごと
	for (DWORD j = 0; j < indexCount_ / 3; j++)
	{
		//3頂点の位置情報を取得
				//インデックス情報は、
				//あらかじめ、時計回りで入っている。
			//インデックスの連続している3つの値は、三角形を作るインデックス情報なので。
				//その連続している値を、渡して三角形を作る頂点位置を取得
		XMVECTOR v0 = pVertex_[pIndex_[(j * 3) + 0]].position;
		XMVECTOR v1 = pVertex_[pIndex_[(j * 3) + 1]].position;
		XMVECTOR v2 = pVertex_[pIndex_[(j * 3) + 2]].position;

		//その三角形との判定
			//レイの情報は引数にてもらっているので、
			//その情報と、
			//三角形の頂点の情報を与えて、接触判定を行う。
		rayData->hit = Math::Intersect(
			rayData->start,		//レイ発射位置
			rayData->dir,			//レイの方向
			v0,					//三角形頂点０
			v1,					//三角形頂点１ 
			v2, 					//三角形頂点２
			&rayData->dist);		//レイと面の接触位置までの長さ


		//当たってたら終わり
		if (rayData->hit)
		{
			return;
		}
	}
}
