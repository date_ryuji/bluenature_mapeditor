#pragma once
#include <DirectXMath.h>
#include "Engine/Direct3D.h"
#include "Engine/Texture.h"

#include "Engine/GameObject.h"


//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル
	float		dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか

	struct originalData
	{
		//衝突したポリゴンの番号
		int polygonNum;
		originalData() :
			polygonNum(-1)
		{
		}
	}original;//originalという変数で構造体変数を取得

	RayCastData() :
		dist(9999999.0f)
	{
	
	}
};


//■■シーンを管理するクラス
class FishBase : public GameObject
{

protected :
	std::string objectName_;


	//継承先で使用
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;	//コンスタントバッファに送る、行列の情報を取得しておく
		XMMATRIX	matW;
	};
	//シェーダーにて、受け取るもののために、構造体で取得
		//現在は、一つしかもっていないが、

	//頂点情報
	//頂点に、位置と、UVがひつようなので、　構造体で取得

	struct VERTEX
	{
		XMVECTOR position;
		XMVECTOR uv;

		XMVECTOR normal;	//法線情報

		
	};

	VERTEX* pVertex_;



	ID3D11Buffer *pVertexBuffer_;	//頂点バッファ
	ID3D11Buffer *pIndexBuffer_;	//インデックスバッファ
	ID3D11Buffer *pConstantBuffer_;	//コンスタントバッファ

		//ポインタのアスタリスクの場所
		//Class *class1;
		//Class* class2;		//どちらも同じ、だが、どっちかにするなら、書き方は共通した書き方をする
	Texture*	pTexture_;


	int indexCount_;	//	頂点数
					//継承先ごとで、違う頂点数
	int *pIndex_;



	void Load();
	void BaseRelease();
	HRESULT InitQuad();


	//ポリゴン群とのレイキャスト実行
	void RayCastWithPolygons(RayCastData * rayData);

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	FishBase(GameObject* parent);

	FishBase(GameObject* parent , std::string objectName);

	virtual ~FishBase();


	//初期化
	virtual void Initialize();




	//更新
	virtual void Update() = 0;

	//描画
	void Draw() override;

	//解放
	virtual void Release();


	HRESULT InitVertex();//頂点情報の初期化
	HRESULT InitIndex();
	HRESULT InitConstantBuffer();

	
	//衝突判定
		//レイと三角ポリゴン群（頂点情報で作られたポリゴン）との判定
	void RayCast(RayCastData * rayData);



};