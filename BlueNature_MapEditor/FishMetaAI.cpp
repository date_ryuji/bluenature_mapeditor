#include "FishMetaAI.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"	//シーン切り替えを行う
#include "Engine/Camera.h"
#include "Engine/CsvReader.h"

#include "resource.h"
#include "Controller.h"


#include "FishBase.h"
//FishBaseクラスを継承した
//魚クラス
#include "Bluefintrevally.h"
#include "Blueheadtilefish.h"
#include "Butterflyfish.h"
#include "BlueTang.h"
#include "Crownfish.h"
#include "Scolopsisbilineat.h"
#include "RayFish.h"
#include "Moorishidol.h"
#include "SpottedGardenEel.h"
#include "WhaleShark.h"



/*
//関数のポインタを受け取るためにインクルードする
	//本来であれば、インクルードせずに、関数のポインタだけ受け取りたかったが、
	//AjustmentInfoSceneの関数のポインタを受け取るには、
	//そのポインタの変数を、AjustmentInfoScene型で宣言しなくてはいけない。（詳細はヘッダにて）
	*/
	//うまくいかなかったので、親のオブジェクトポインタをもらう形にする

#include "AjustmentInfoScene.h"
#include "TestBlock.h"






//コンストラクタ
FishMetaAI::FishMetaAI(GameObject * parent)
	: GameObject(parent, "FishMetaAI"),
	groundNumber_(-1),
	currentFish(FISH_BLUE_FINTREVALLY),
	pCsvReader_(nullptr),
	changeComboBox(false),
	//changePosFunc_(nullptr)	//関数ポインタヌルポインタで初期化
	pAjustmentInfoScne_(nullptr)
{
}

FishMetaAI::~FishMetaAI()
{
	//for (int i = 0; i < FISH_MAX; i++)
	//{
	//	//ポインタの複数確保なので、確保した分解放
	//	SAFE_DELETE(pFishBase[i]);
	//}

	//Instantiateしたものを配列に要素を入れているだけなので、
	//ここで開放してはいけない。


	delete[] pCsvReader_;
	


}

//初期化
void FishMetaAI::Initialize()
{
	//テキストファイル〜
	//前シーンにて触れた地面番号を取得
	GetCurrentGroundNum();



	//Instantiateしたオブジェクトのポインタをもらっているだけ
		//そのため、このクラスで解放してはいけない。
	pFishBase[FISH_BLUE_FINTREVALLY] = (FishBase*)Instantiate<Bluefintrevally>(this);
	pFishBase[FISH_BLUE_HEADTILE_FISH] = (FishBase*)Instantiate<Blueheadtilefish>(this);
	pFishBase[FISH_BLUE_TANG] = (FishBase*)Instantiate<BlueTang>(this);
	pFishBase[FISH_BUTTERFLY_FISH] = (FishBase*)Instantiate<Butterflyfish>(this);
	pFishBase[FISH_CROWN_FISH] = (FishBase*)Instantiate<Crownfish>(this);
	pFishBase[FISH_SCOLOPSIS_BILINEAT] = (FishBase*)Instantiate<Scolopsisbilineat>(this);
	pFishBase[FISH_RAY_FISH] = (FishBase*)Instantiate<RayFish>(this);
	pFishBase[FISH_MOORISHIDOL] = (FishBase*)Instantiate<Moorishidol>(this);
	pFishBase[FISH_SPOTTED_GARDEN_EEL] = (FishBase*)Instantiate<SpottedGardenEel>(this);
	pFishBase[FISH_WHALE_SHARK] = (FishBase*)Instantiate<WhaleShark>(this);


	std::string commonPath = "Assets/Csv_Save/";
	std::string csvFileName[CSV_MAX] =
	{
		commonPath + "Fish_Information.csv",
		commonPath + "FishCost_GroundENV.csv" ,
		commonPath + "FishHeight_GroundENV.csv" ,

	};

	//CSV読み取りクラスの確保
		//配列で実態を複数確保
	pCsvReader_ = new CsvReader[CSV_MAX];

	for (int i = 0; i < CSV_MAX; i++)
	{
		pCsvReader_[i].Load(csvFileName[i]);
	}


	//文字データの取得
	GetCsvString();

	//ダイアログの初期化
	//InitDialog();
		//ここで初期化はできない。
		//なぜならば、初期化の段階で、AjustmentInfoSceneのポインタを持っていないため
		//→何もないnullptrの関数にアクセスしようとしてしまう。これは避けなければいけない。
		//初期化が終わり、ポインタを取得した時点で、ダイアログの初期化を行うように変更する



	for (int i = 0; i < FISH_MAX; i++)
	{
		pFishBase[i]->transform_.position_.vecX = i * 5.0f;	//頂点の横幅分ずらす

	
	}




}

void FishMetaAI::GetCsvString()
{
	//メンバのdatasは、
	//１要素目に、CSVファイルの　FISH_TYPE_INFO,
	//２要素目に、FISH_COST_GROUND_ENV,
	//３様相目に、FISH_HEIGHT_GROUND_ENV,

	//の情報を取得する。


	//１行の文字列を取得しておく。
		//複数行確保のためにさらにvector
	//一行				std::vector < std::string >> str;	（一行の中の単語を第２要素[][ここ]で指定する）
	//一行を複数列		std::vector < std::vector < std::string >> str;
	std::vector < std::string > line;

	/*
	
	Fish_Type_Info

	取得ルール
	
	なし
	
	*/
	line.clear();
	data_.push_back(line);

	/*
	
	Fish_Cost_GroundENV

	取得ルール
	・列・・・魚タイプ別番号（１オリジンのため、enum番号の＋１）
	・行・・・地面番号（１オリジンのため、メンバ変数の＋１）
	
	*/
	line.clear();	//Vectorのベクターは、一番上のベクターを空にしたら、下も解放されるのか？（ポインタではないので、実体が残ることはないか？）

	for (int i = 0; i < FISH_MAX; i++)
	{



		//CSVの２要素目から、
		//GetStringで、情報の取得
			//１要素ずつ取得していって、それを文字列のlineとして取得し、
			//メンバ変数のdata_に保存、追加
		//x（左から〇番目）:魚
		//y（上から〇番目）:地面
		
		//最後の文字なら
		//if ( i != (FISH_MAX - 1))
		{
			//文字列を取得
			std::string s = pCsvReader_[FISH_COST_GROUND_ENV].GetString(i + 1, groundNumber_ + 1);

			//文字列の一番後ろに\0を付けているので、
			//それを外す。
			size_t size = s.size();

			std::string copyStr = "";

			
			//コピー先、コピー元、コピー先を先頭としてコピーするコピーサイズ
				//サイズをー１　＝　文字末についている'\0' のcharの１バイト分引く。
				//正しくは、sizeのサイズでよい、そうでないと文字化けする
			memcpy((void*)copyStr.c_str(), (void*)s.c_str(), size);




			line.push_back(copyStr);


			//文字列の結合を行うとエラーになる。
		}
		//書き込むときに、＋して文字を足せばよい

	/*	else
		{
			std::string s = pCsvReader_[FISH_COST_GROUND_ENV].GetString(i + 1, groundNumber_ + 1);
			size_t size = s.size();
			std::string copyStr = "";
			memcpy((void*)copyStr.c_str(), (void*)s.c_str(), size);
			line.push_back(copyStr);

		}*/

	}
	data_.push_back(line);


	/*

	Fish_Height_GroundENV

	取得ルール
	・列・・・魚タイプ別番号（１オリジンのため、enum番号の＋１）
	・行・・・地面番号（１オリジンのため、メンバ変数の＋１）

	*/
	line.clear();	//Vectorのベクターは、一番上のベクターを空にしたら、下も解放されるのか？（ポインタではないので、実体が残ることはないか？）

	for (int i = 0; i < FISH_MAX; i++)
	{



		//CSVの２要素目から、
		//GetStringで、情報の取得
			//１要素ずつ取得していって、それを文字列のlineとして取得し、
			//メンバ変数のdata_に保存、追加
		//x（左から〇番目）:魚
		//y（上から〇番目）:地面


			std::string s = pCsvReader_[FISH_HEIGHT_GROUND_ENV].GetString(i + 1, groundNumber_ + 1);
			size_t size = s.size();
			std::string copyStr = "";
			memcpy((void*)copyStr.c_str(), (void*)s.c_str(), size);
			line.push_back(copyStr);
		


	}
	data_.push_back(line);


}

void FishMetaAI::GetCurrentGroundNum()
{
	{
		HANDLE hFile;        //ファイルのハンドル
		hFile = CreateFile(
			"Assets/currentGroundNumber.txt",                 //ファイル名
			GENERIC_READ,           //アクセスモード（読み込み用）
			0,                      //共有（なし）
			NULL,                   //セキュリティ属性（継承しない）
			OPEN_EXISTING,           //作成方法	//ファイルを開く（存在してなければエラー）
			FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
			NULL);                  //拡張属性（なし）


		//ファイルのサイズを取得
			//１文字１バイトなので、そのサイズ分サイズを取得して、（バイトの数を取得）
		DWORD fileSize = GetFileSize(hFile, NULL);

		//読み込んだデータを入れる配列
		//ファイルのサイズ分メモリを確保
			//上記で取得したバイト分、charポインタの配列で取得
		char* data;
		data = new char[fileSize];

		DWORD dwBytes = 0; //読み込み位置

		//ファイル読み込み
		ReadFile(
			hFile,     //ファイルハンドル
			data,      //データを入れる変数
			fileSize,  //読み込むサイズ
			&dwBytes,  //読み込んだサイズ
			NULL);     //オーバーラップド構造体（今回は使わない）


		//ファイルの読み込みはもうしないので閉じる
		CloseHandle(hFile);


		

		//取得した文字を数値に変換
		groundNumber_ = atoi(data);
	}

	//データ取得を完了する
}

//Main関数内にて、
//初めにFishMetaAIが呼ばれた時、
//ダイアログ番号をセットするように呼び込む
void FishMetaAI::InitDialog(HWND hDlg)
{
	if (hDlg_ != hDlg)
	{

		hDlg_ = hDlg;
		//ダイアログ番号を取得した時点で、
		//初期化
		//コンボボックス（ダイアログ）に文字データとして取得した情報を適用させる
		SetComboBox(FISH_BLUE_FINTREVALLY);
	}

}

//更新
void FishMetaAI::Update()
{
	/*３Dオブジェクトクリックで、ダイアログ操作。*/

	//マウスの入力がされたら
	//一つ一つのオブジェクトに当たり判定を行う
	if (Input::IsMouseButtonDown(0))
	{

		//レイのデータを持つための構造体
			//→AutoGenerateFloorクラスに存在する構造体である。（Fbxにもあるが、AutoGenerateFloorは、Fbxクラスを継承しないため、個別に作成した。（＋オリジナルメンバ変数もある。））
		RayCastData rayData;

		FISH fishType = ShotRay(&rayData);	

		//マウスのクリック時の座標と、魚オブジェクトとの衝突判定
		if (fishType != FISH_MAX)	//返り値がFISH_MAX値でない（誰かに当たった）
		{
			//一番最初に当たったオブジェクト
			//魚を、ダイアログのコンボボックスに表示させる
			SetComboBox(fishType);




		}

		




		

		//この時、
		//現在、最新の魚情報をダイアログに表示する。（正しくは、ダイアログの状態をどこかで持っておいて、それに、ダイアログを合わせる？形）

		//そして、その魚に注目するr。（それなら、プレイヤーの移動をなくしたほうがよさそうだが、、）
	}


	//逆に、
	//ダイアログ操作されたら、
	//特に、現段階では、魚の種類がかえられたら、
	//その魚に注目する。


}

//描画
void FishMetaAI::Draw()
{
}

//開放
void FishMetaAI::Release()
{
}




BOOL FishMetaAI::DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//×→ダイアログに変化がされないとダイアログプロシージャは呼ばれない。→ダイアログが操作されていない時にも、操作したいので有効でない。
	//ダイアログ（コンボボックス）の操作を指定されている時（フラグが立っているとき）
	//こちらから操作。



	switch (msg)
	{
		//コントロールがクリックや選択された時
		//本来なら、コンボボックスの中身が変更されたときなどのコマンドがあると思うので、その時にだけ呼びたい
			//→でないと、無駄な処理を何回もすることになる。
	case WM_COMMAND:

		//引数のwpの、LOWORD（wp）にコントロールのIDが入ってくる
		//そのためこのコントロールのIDから、誰に変更が加えられたのか判断。処理を判断する。
		switch (LOWORD(wp))
		{
		//魚タイプのコンボボックスなら
				//�@優先度を魚タイプの情報に変更(Set)
				//�A高さを魚タイプの情報に変更(Set)
				//※変更前の魚の情報（�@，�Aの情報）は、変更時点で保存済みなので、
					//この時に改めて保存するという手間は行わない
		case TYPE_COMBO:
		{	//コンボボックスが現在選択している値を取得したい
			//引数：コントロールハンドル
			//引数：現在選択されている値を取得したい
			//引数：なし
			//引数：なし
			//戻値：現在選択している値（一番上の）（番号）（コンボボックスに登録した時点での登録順に０オリジンから値を入れている）
					//その登録順が、enumでMapに取得している番号と同じであるならば、その戻り値をそのまま、現在のタイプとして取得することができる
			//★選択された魚が誰なのか取得（コンボボックスの枠などが触られても反応するので、変更していないときも呼ばれてしまうが、、）
			currentFish =
				(FISH)SendMessage(
					GetDlgItem(hDlg, TYPE_COMBO),
					CB_GETCURSEL, 0, 0);

			if (currentFish == -1)
			{
				currentFish = FISH_BLUE_FINTREVALLY;
			}

			//魚のタイプに対する
			//�@、�Aをセットする
			SetComboBox(currentFish);
			return TRUE;
		}
			break;

		case PRIORITY_COMBO : 
		{
			//変更後のデータで保存
				//現在選択されている（一番上）の値を、魚別の情報に保存させる
			//コンボボックスが現在選択している値（一番上）を取得
			PRIORITY_RANK currentPriority =
				(PRIORITY_RANK)SendMessage(
					GetDlgItem(hDlg, PRIORITY_COMBO),
					CB_GETCURSEL, 0, 0);

			if (currentPriority == -1)
			{
				currentPriority = S_RANK;
			}

			//現在の魚タイプを送り
				//その魚タイプの、PRIORITYの情報を持っているデータを今回変更されたPriorityに変換
			SetPriority(currentFish , currentPriority);

			//優先度をセット
			SendMessage(
				GetDlgItem(hDlg_, PRIORITY_COMBO),
				CB_SETCURSEL, currentPriority, 0);

		}
		break;
		case HEIGHT_COMBO:
		{
			//変更後のデータで保存
				//現在選択されている（一番上）の値を、魚別の情報に保存させる
			//コンボボックスが現在選択している値（一番上）を取得
			SPAWN_HEIGHT currentHeight =
				(SPAWN_HEIGHT)SendMessage(
					GetDlgItem(hDlg, HEIGHT_COMBO),
					CB_GETCURSEL, 0, 0);

			if (currentHeight == -1)
			{
				currentHeight = HEIGHT_0_POINT_0;
			}

			//現在の魚タイプを送り
				//その魚タイプの、PRIORITYの情報を持っているデータを今回変更されたPriorityに変換
			SetHeight(currentFish, currentHeight);

			//優先度をセット
			SendMessage(
				GetDlgItem(hDlg_, HEIGHT_COMBO),
				CB_SETCURSEL, currentHeight, 0);

		}
		break;


		default:
			return TRUE;

		}

	}

	//何も処理していないなら、Falseだけ
	return FALSE;


}

void FishMetaAI::SetPriority(FISH fishType , PRIORITY_RANK currentPriority)
{


	//優先度の文字列を入れるchar型
	char priorityChar = ' ';


	//引数にてもらった優先度値（enum値）をもとに
	//優先度の値（文字）を取得
	switch (currentPriority)
	{
	case S_RANK:
		priorityChar = 'S';
		break;
	case A_RANK:
		priorityChar = 'A';
		break;
	case B_RANK:
		priorityChar = 'B';
		break;
	case C_RANK:
		priorityChar = 'C';
		break;
	case D_RANK:
		priorityChar = 'D';
		break;


	default:
		return;	//何もしない
		break;
	}

	//data_[FISH_COST_GROUND_ENV][fishType];	fishTypeの優先度（そもそもdata_は、１つの地面のデータになっているので地面番号を指定するなどは省かれる）
	//データの書き換え
		//第一引数：コピー先アドレス
		//&data_[FISH_COST_GROUND_ENV][fishType][0]	fisyTypeの優先度の文字列の1バイト目のアドレス
		//第二引数：コピー元アドレス
		//&priorityChar								書き換えるchar文字
		//第三引数：コピーサイズ（コピー元アドレスを先頭にどのぐらいコピーするのか）
	//OK
	memcpy(&data_[FISH_COST_GROUND_ENV][fishType][0], &priorityChar, 1);


}
void FishMetaAI::SetHeight(FISH fishType , SPAWN_HEIGHT currentHeight)
{

	//高さを入れる変数
	float height = 0.0f;


	//引数にてもらった高さ値（enum値）をもとに
	//高さの値（実際は割合値として使用する）（float型）を取得

	//0.0f ~ 1.0f
	switch (currentHeight)
	{
	case HEIGHT_0_POINT_0: 
		height = 0.0f; break;
	case HEIGHT_0_POINT_1:
		height = 0.1f; break;
	case HEIGHT_0_POINT_2:
		height = 0.2f; break;
	case HEIGHT_0_POINT_3:
		height = 0.3f; break;
	case HEIGHT_0_POINT_4:
		height = 0.4f; break;
	case HEIGHT_0_POINT_5:
		height = 0.5f; break;
	case HEIGHT_0_POINT_6:
		height = 0.6f; break;
	case HEIGHT_0_POINT_7:
		height = 0.7f; break;
	case HEIGHT_0_POINT_8:
		height = 0.8f; break;
	case HEIGHT_0_POINT_9:
		height = 0.9f; break;
	case HEIGHT_1_POINT_0:
		height = 1.0f; break;



	default:
		return;	//何もしない
		break;
	}

	//高さの値は、
	//優先度のように1文字の1バイトではない、
		//floatを文字にしたとしても、
		//文字バイト数がそれぞれ、float値ごとに違う可能性もある。
		//そのため、バイトを指定して代入するわけにもいかない。

	//文字列をそのまま入れ替えるような代入方法で入れ替えが可能なのか？
		//配列で要素を決めてしまっているので、サイズが帰られたりするとエラーが起こってしまうのでは？
		//だが、もともとstringは、どんどん文字バイトが増えていくものなので、たとえ配列であっても問題ないか？

	//Vectorなので、元のデータを削除して、
	//その削除の位置に新たに作成した（float値を文字にした新データ）データを
	//追加する？


	//少数第2位以下を切り捨てるための処理
	
	////int 型１００の値と掛け算をして
	////int 型にキャスト
	//	//そうすれば、元　少数第一位までの値がint型として残る
	//height = (int)(height * 100);

	////そしてかけた分割って（今度はfloat型（そうしないと少数にならないので、））
	////切り捨て処理完了
	//height = height / 100.0f;
	


	//データの書き換え
	//std::string writeStr = std::to_string(height);

	//heightの値を4文字にして代入する
		//実数２
		//.
		//少数１　で計4バイト（実数が1桁の場合3バイト）

		//\0も入れれば5バイト
	char writeStr[5];

	//桁数を少数第1位までにしたい
	//http://www.c-tipsref.com/reference/stdio/sprintf.html

	//https://monozukuri-c.com/langc-funclist-sprintf/
	//第一引数：コピー先文字列　アドレス（char*）
	//第二引数：フォーマット	"%1.1f" = 実数1桁.少数1桁
	//第三引数：フォーマットに割り当てる変数
	sprintf(writeStr, "%2.1f", height);



	
	
	//新規文字列を代入する
		//この場合、仮にwriteStrのほうのデータサイズのほうが多かった場合。うまくいくのか
	data_[FISH_HEIGHT_GROUND_ENV][fishType] = writeStr;



}

BOOL FishMetaAI::MapSave()
{
	//現在の魚情報でCSVファイルを作成する。
	//これは、このクラスが持っているDataだけでなく、元あるデータ。（CSVに読み込んだ）
		//をこのクラスが持っているDataで上書きして、
		//そのデータを書き込まなければいけない。



	//�@魚の優先度
		//保存ルール
	/*

		�@優先度　（S~D）　	１バイト
		�A ,				１バイト

		行末　\n			１バイト

	*/
	SavePriority();


	SaveHeight();


	return TRUE;
}

void FishMetaAI::SaveHeight()
{
	//ファイルを作成
	//からのファイルを作り、　hfileにハンドルを入れる。
	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		"Assets/Csv_Save/FishHeight_GroundENV.csv",                 //ファイル名
		GENERIC_WRITE,           //アクセスモード（書き込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		CREATE_ALWAYS,           //作成方法
		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）




								//確保用文字サイズ（最終的に確保されるサイズではない。）
								//FISH_MAX文の　値5 バイト（実数２桁＋少数１桁＋カンマ１桁）　＋　１（最後の\nを入れるため） + 2(地面番号＋カンマ)
	int charSize = FISH_MAX * 5 + 1 + 2;

	//最大サイズ（これ以上は入らないだろうという要素で初期化された配列）
	char str[FISH_MAX * 5 + 1 + 2];

	//書き込んだ文字サイズ
	int myDataCounter = 0;

	GetMyHeightData(&myDataCounter, charSize, str);





	//CSVリーダーから元データを取得し、
	//その情報と自身のデータ部分を結合して（該当部分だけ更新）書き込みデータとする
	std::vector<std::vector<std::string>>* csvData
		= pCsvReader_[FISH_HEIGHT_GROUND_ENV].GetStringData();


	//上記のデータの中から、自身が編集したデータ部をまるっきり書き換えて
	//それを最終的な書き込みのCSVデータとする

	//この時、自身が編集したものは、groundNumber + 1（上の余計な一行分足して）の行



	int test = pCsvReader_[FISH_HEIGHT_GROUND_ENV].GetCsvFileSize() * 3;

	//データとともに、一つ一つカンマを入れるので、3倍ぐらいのデータが欲しい
	char* writeWrite = new char[pCsvReader_[FISH_HEIGHT_GROUND_ENV].GetCsvFileSize() * 3];


	//最終的な配列に書き込んだデータ数（配列の要素数）
	int allDataCounter = 0;
	GetAllHeightCsvData(writeWrite, &allDataCounter, str, myDataCounter, csvData);




	//テスト代入
		//全てCSVファイルのデータを取得
	//OK
	std::string ssss = writeWrite;

	//文字サイズテスト
		//サイズが配列で確保した要素数よりも小さくなるので注意
		//（おそらく空のデータがデータサイズとして入ってこないと考える）

		//なのでカウンターとして使用していたデータを使用する。
	int writeSize = std::strlen(writeWrite);



	//ファイルにデータを書き込む
	//そのどこから保存するかの、位置
	DWORD dwBytes = 0;  //書き込み位置（０＝一番頭から）
	WriteFile(
		hFile,						//ファイルハンドル

									//const char*型で、データを取得するので、
									//文字列をconstchar*型に変換、そのための関数c_str()
		writeWrite,						//保存するデータ（文字列）
									//const char*型に変換し、
									//文字列を取得する関数に送り返り値で文字サイズを取得
		(DWORD)allDataCounter,		//書き込む文字数(サイズ)；文字数を出す関数＝strlen
		&dwBytes,                //書き込んだサイズを入れる変数(頭から保存して、dwBytesに文字数、書き込んだデータのサイズが足されるので、　dwBytesには、次の書き込む位置が入ることになる。
								 //次に同じ関数を呼んでも、書き込み位置はいじらずとも追記できるようになる。)
		NULL);                   //オーバーラップド構造体（今回は使わない）


								 //ハンドル閉じる
	CloseHandle(hFile);

	delete[] writeWrite;

}

//引数：自身のデータに書き込んだデータバイト数（書き込んだ要素数になる）
//引数：配列の要素数
//引数：書き込む文字配列（ポインタ）
void FishMetaAI::GetMyHeightData(int* counter, int charSize, char str[])
{
	//地面番号を
	//文字列に変換
	std::string s1 = std::to_string(groundNumber_);

	//文字列の先頭１バイト取得＝intの値の先頭１バイトを文字として代入
	AddChar(str, counter, s1[0]);
	AddChar(str, counter, ',');



	int currentNum = 0;
	while (((*counter) < charSize) && (currentNum < FISH_MAX))
	{

		//１単語（１値（float値））
		std::string word = data_[FISH_HEIGHT_GROUND_ENV][currentNum];


		//文字列をfloat型へ変換
		float value = atof(word.c_str());

		//２度手間だが、
		//数値にしたものから新たに書き込み用の文字列にフォーマット
		//\0も入れれば5バイト
		char writeStr[5];

		//桁数を少数第1位までにしたい
		//http://www.c-tipsref.com/reference/stdio/sprintf.html

		//https://monozukuri-c.com/langc-funclist-sprintf/
		//第一引数：コピー先文字列　アドレス（char*）
		//第二引数：フォーマット	"%1.1f" = 実数1桁.少数1桁
		//第三引数：フォーマットに割り当てる変数
		sprintf(writeStr, "%2.1f", value);


		//文字サイズ(1バイトずつなので、この場合は文字数になる)を取得
		int wordSize = strlen(writeStr);



		//strの変数に
		//書き込み用の変数をコピーする

		//最終書き込み用文字列の書き込みを始めるアドレス
		//今回書き込む文字列
		//書き込む文字列のサイズ
		memcpy(&str[(*counter)], writeStr, wordSize);

		//書き込んだ分だけ添え字を進める
		(*counter) += wordSize;



		//カンマの代入
		AddChar(str, counter, ',');
		
		//カウントを一つ進める
			//見る魚のタイプを進める
		currentNum++;




		//while(k != wordSize)
		//{
		//	//文字列の１バイトずつ取得
		//	char ch = word[k];

		//	if (ch == '\0')
		//	{
		//		break;
		//	}


		//	//1バイトの文字の取得
		//	//少数点を文字として持っている、１文字を取得
		//	str[i + k] = ch;

		//	k++;


		//} 



		//if (k == 0)
		//{
		//	//k==0ならば、
		//	//何もデータがなかったというもの
		//	//そのため、NULLを入れて何もないと表示

		//	str[i + k] = NULL;
		//	k++;
		//}



		//文字列の結合
		//sprintf(str, "%1c", data_[FISH_COST_GROUND_ENV][i]);

	}
	//改行
	AddChar(str, counter, '\n');

	//余計な要素を削って、
	//書き込み用の文字列を作成
	//必要のない値以下はNULLで初期化、何も表示させないようにする。
	for (int k = (*counter); k < charSize; k++)
	{
		str[k] = NULL;

	}
}

void FishMetaAI::SavePriority()
{

	//ファイルを作成
	//からのファイルを作り、　hfileにハンドルを入れる。
	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		"Assets/Csv_Save/FishCost_GroundENV.csv",                 //ファイル名
		GENERIC_WRITE,           //アクセスモード（書き込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		CREATE_ALWAYS,           //作成方法
								 /*
								 ・作成方法

								 〇新しくファイルを作る（同名のファイルがあるとエラー）：CREATE_NEW （新規保存）

								 〇新しくファイルを作る（同名のファイルがあると上書き）：CREATE_ALWAYS (上書き保存)

								 □ファイルを開く（その名前のファイルがなければエラー）：OPEN_EXISTING （）

								 □ファイルを開く（その名前のファイルがなければ作る）   ：OPEN_ALWAYS


								 */

		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）



	//自身のデータを文字配列に代入、取得する
	//このデータをもともと持っているCSVのデータ（CsvReaderクラスが持っているデータ）に該当部分だけ更新して、
	//それを最終的にCSVに書き込む



								//書き込むための文字列の取得
								//全部の魚の情報を一行で取得
	//魚＊２(優先度の文字＋カンマ)　＋　\n + 配列頭の地面番号 , 
	int charSize = FISH_MAX * 2 + 1 + 2;
	char str[FISH_MAX * 2 + 1 + 2];

	GetMyPriorityData(str, charSize);







	//CSVリーダーから情報を取得し、
	//その情報と合わせて、ファイルに書き込み


	std::vector<std::vector<std::string>>* csvData
		= pCsvReader_[FISH_COST_GROUND_ENV].GetStringData();

	//上記のデータの中から、自身が編集したデータ部をまるっきり書き換えて
	//それを最終的な書き込みのCSVデータとする

	//この時、自身が編集したものは、groundNumber + 1（上の余計な一行分足して）の行



	
	//データとともに、一つ一つカンマを入れるので、3倍ぐらいのデータが欲しい
	char* writeWrite = new char[pCsvReader_[FISH_COST_GROUND_ENV].GetCsvFileSize() * 3];


	//最終的な配列に書き込んだデータ数（配列の要素数）
	int allDataCounter = 0;
	GetAllPriorityCsvData(writeWrite, &allDataCounter, str, charSize, csvData);




	//テスト代入
		//全てCSVファイルのデータを取得
	//OK
	std::string ssss = writeWrite;

	//文字サイズテスト
		//サイズが配列で確保した要素数よりも小さくなるので注意
		//（おそらく空のデータがデータサイズとして入ってこないと考える）

		//なのでカウンターとして使用していたデータを使用する。
	int writeSize = std::strlen(writeWrite);





	//ファイルを書き込むために、
	//string型をconst char*の形式に変更
	//const char* data = str.c_str();

	//const char* str
	//= "Map.cppの関数”MapSaveから”保存されました。\nよ\n";

	//ファイルにデータを書き込む
	//そのどこから保存するかの、位置
	DWORD dwBytes = 0;  //書き込み位置（０＝一番頭から）
	WriteFile(
		hFile,						//ファイルハンドル

									//const char*型で、データを取得するので、
									//文字列をconstchar*型に変換、そのための関数c_str()
		writeWrite,						//保存するデータ（文字列）
									//const char*型に変換し、
									//文字列を取得する関数に送り返り値で文字サイズを取得
		(DWORD)allDataCounter,		//書き込む文字数(サイズ)；文字数を出す関数＝strlen
		&dwBytes,                //書き込んだサイズを入れる変数(頭から保存して、dwBytesに文字数、書き込んだデータのサイズが足されるので、　dwBytesには、次の書き込む位置が入ることになる。
								 //次に同じ関数を呼んでも、書き込み位置はいじらずとも追記できるようになる。)
		NULL);                   //オーバーラップド構造体（今回は使わない）


								 //ハンドル閉じる
	CloseHandle(hFile);


	//メモリ解放時のヒープコラプションについて
	//https://dixq.net/forum/viewtopic.php?t=15579
	delete[] writeWrite;

}

//引数：書き込み全データ（char*（要素を動的確保済み））
//引数：書き込み全データに書き込んだデータバイト数（書き込んだ要素数になる）
//引数：自身のメンバの持っているデータ配列ポインタ
//引数：自身のメンバの持っているデータ配列のデータバイト数
//引数：CSVの元データにアクセスするためのポインタ
void FishMetaAI::GetAllPriorityCsvData(char* writeWrite, int* counter, 
	char  str[], int charSize, std::vector<std::vector<std::string>> * csvData)
{
	for (int m = 0;
		m < pCsvReader_[FISH_COST_GROUND_ENV].GetHeight();
		m++)
	{

		//このクラスで扱っている地面番号のCSVデータ
		//自身のCSVデータを書き込み用のデータに代入
		if (m == (groundNumber_ + 1))
			//メンバの地面の番号は０オリジン
			//CSVにおける番号は　１行目から（つまり１オリジンととらえることも可能）
		{
			//上記で取得した
			//データをそのままサイズを書き込む

			//書き込みを始める要素のアドレスから
			//自身のデータを持っている配列の先頭アドレス
			//配列のサイズ
			memcpy(&writeWrite[(*counter)], str, charSize);


			(*counter) += charSize;

			//strの文字列で\nと　値の間の,がついている。





		}
		else
			//地面の番号と同様でない。＝CSVのデータをそのまま書き込む
		{

			int width = pCsvReader_[FISH_COST_GROUND_ENV].GetWidth();
			for (int n = 0;
				n < width;
				n++)
			{
				//一行取得
				//[m] １２行あるうちの
				//[m] １２行あるうちの

				//ポインタなので、
				//実態にアクセスして、そのVectorの要素

				//csvDataから取得する文字列は、一番後ろに\0がついている
				//これがあることで、データの最後という判断がされてしまう
				//そのため、stringに、\0を抜いた文字列（文字列と言っていいのかわからないが、）をコピー
				std::string test;

				std::string test_recv = (*csvData)[m][n];

				//文字数取得
				int size = strlen(test_recv.c_str());

				//文字数で取得できるのは、文字列最後の\０を抜いた数、
				//そのためサイズ＋１をすると、\０までの文字列が入る
				memcpy((void*)test.c_str(), (void*)test_recv.c_str(), size);

				memcpy(&writeWrite[(*counter)], (void*)test.c_str(), size);


				(*counter) += size;





				//引数：文字を追加したい文字配列のポインタと
				//引数：書き込む添え字位置ポインタ
				//引数：書き込むchar
				AddChar(writeWrite, counter , ',');


				//最後の要素なら
				if (n == width - 1)
				{
					//if(m == pCsvReader_[FISH_COST_GROUND_ENV].GetHeight() - 1)
					{ 
					AddChar(writeWrite, counter, '\n');
					}
				}



			}
		}


	}
	//最後に￥０はつけない。
		//なぜなら、￥０がついていると、CSVREADERにてエラーが出る
	//それだと、文字列として不完全だが、
		//書き込んだ最後の位置をカウンターに手持っているので、
		//CSVに書き込むときも、そのカンターまで書き込むという処理にしているので、問題なく、最後の文字まで書き込める。
	//AddChar(writeWrite, counter, '\0');
}

void FishMetaAI::GetAllHeightCsvData(char * writeWrite, int * allDataCounter,
	char str[], int myDataCounter, std::vector<std::vector<std::string>>* csvData)
{
	for (int m = 0;
		m < pCsvReader_[FISH_COST_GROUND_ENV].GetHeight();
		m++)
	{

		//このクラスで扱っている地面番号のCSVデータ
		//自身のCSVデータを書き込み用のデータに代入
		if (m == (groundNumber_ + 1))
			//メンバの地面の番号は０オリジン
			//CSVにおける番号は　１行目から（つまり１オリジンととらえることも可能）
		{
			//上記で取得した
			//データをそのままサイズを書き込む


			//書き込みを始める要素のアドレスから
			//自身のデータを持っている配列の先頭アドレス
			//配列のサイズ
			memcpy(&writeWrite[(*allDataCounter)], str, myDataCounter);


			(*allDataCounter) += myDataCounter;

			//strの文字列で\nと　値の間の,がついている。





		}
		else
			//地面の番号と同様でない。＝CSVのデータをそのまま書き込む
		{

			int width = pCsvReader_[FISH_COST_GROUND_ENV].GetWidth();
			for (int n = 0;
				n < width;
				n++)
			{
				std::string test;

				//1単語ずつ、1セルデータずつ取得
				std::string test_recv = (*csvData)[m][n];

				//文字数取得
				int size = strlen(test_recv.c_str());

				//文字数で取得できるのは、文字列最後の\０を抜いた数、
				//そのためサイズ＋１をすると、\０までの文字列が入る
					//文字列をそのままほしいのであれば、＋１で文字列にコピー（memcpy）すれば、文字列が取得できる（ちなみにsize分しかコピーしないと、￥０（文字の終わり）がわからないので、文字化けを起こす）

				//ここでは￥０前の文字まで取得し、
					//連続してデータを取得する
				memcpy((void*)test.c_str(), (void*)test_recv.c_str(), size);

				memcpy(&writeWrite[(*allDataCounter)], (void*)test.c_str(), size);


				(*allDataCounter) += size;





				//引数：文字を追加したい文字配列のポインタと
				//引数：書き込む添え字位置ポインタ
				//引数：書き込むchar
				AddChar(writeWrite, allDataCounter, ',');


				//最後の要素なら
				if (n == width - 1)
				{
					AddChar(writeWrite, allDataCounter, '\n');
				}



			}
		}


	}
	//文字列の終了のため￥0を代入。これを最後に入れないと文字化けする
	//AddChar(writeWrite, allDataCounter, '\0');

}

void FishMetaAI::AddChar(char* writeWrite, int* counter , char addChar)
{
	writeWrite[(*counter)] = addChar;
	(*counter)++;
}

//配列の先頭アドレスを受け取るので
//第一引数は配列の参照渡しのようなもの。→元データを扱うものになる。
	//コピーして受け取りたいときは、配列名[要素数]で受け取る？
	//どちらにしても、str　とすればポインタになる、しかし、そのポインタが指しているのは、元のデータ化、コピー先かで違ってくる。
	
void FishMetaAI::GetMyPriorityData(char  str[], int charSize)
{
	//文字列に変換
	std::string s1 = std::to_string(groundNumber_);

	//文字列の先頭１バイト取得＝intの値の先頭１バイトを文字として代入
	str[0] = s1[0];
	str[1] = ',';

	int currentChar = 0;
	for (int i = 2; i < charSize - 1; i++)
	{
		//先頭1バイトの取得
		//先頭一バイトにて、優先度の文字を取得
		str[i] = data_[FISH_COST_GROUND_ENV][currentChar][0];

		currentChar++;
		i++;
		//カンマの代入
		str[i] = ',';
	}
	//改行
	str[charSize - 1] = '\n';

	//上記で現在扱っているデータを一行のデータとして取得完了した。
}

BOOL FishMetaAI::MapOpen()
{
	return FALSE;
}

BOOL FishMetaAI::MapSaveAs()
{
	return FALSE;
}

void FishMetaAI::SetAjustmentInfoScene(AjustmentInfoScene * pPointa)
{
	pAjustmentInfoScne_ = pPointa;

	//Controllerの位置を変更する関数を持っているクラスのポインタをもらってはじめて、
		//ダイアログの初期化を行う。
		//ダイアログの初期化を行って、ダイアログの初期値を設定する。その時に、Controllerの位置を変更する処理も呼び込まなければいけない。


}

/*
void FishMetaAI::SetFunctionPointa(void(AjustmentInfoScene::*functionPointa)(int))
{
	//関数ポインタの受け渡し
	changePosFunc_ = functionPointa;

}
*/


FISH  FishMetaAI::ShotRay(RayCastData* rayData)
{
	//２次元から、３次元へ直すための行列を取得する。

	//ビューポート行列
		//ビューポート行列は、これまで、Direct３Dに作ってもらっていたため、
		//どこにも存在していない状態なので、作成する。
	float w = Direct3D::scrWidth / 2.0f;
	float h = Direct3D::scrHeight / 2.0f;
	XMMATRIX vp = {
		w, 0, 0, 0,
		0, -h, 0, 0,
		0, 0, 1, 0,
		w, h, 0, 1
	};
	//プロジェクション行列、ビューはカメラから

	//逆行列を作成
	//ビューポート行列の逆行列
	XMMATRIX invVp = XMMatrixInverse(nullptr, vp);
	//プロジェクション行列の逆行列
	XMMATRIX invPro = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
	//ビュー行列の逆行列
	XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());

	//ベクトル
	XMVECTOR mousePosFront = Input::GetMousePosition();
	mousePosFront.vecZ = 0.0f;
	XMVECTOR mousePosBack = Input::GetMousePosition();
	mousePosBack.vecZ = 1.0f;

	//３つの行列で変換することで、
	//ワールドの座標とする。
		//ビューポートからワールドへ
	mousePosFront = XMVector3TransformCoord(
		mousePosFront,
		invVp * invPro * invView
	);

	mousePosBack = XMVector3TransformCoord(
		mousePosBack,
		invVp * invPro * invView
	);


	//ワールドにおける座標が出せたので、
		//レイの方向を、
		//マウス開始位置から終了位置までに延ばす。(カメラの視錐台の始まりから終わりまで伸びるレイほうこう)
	XMVECTOR rayDir = mousePosBack - mousePosFront;
	
	//正規化
	rayDir = XMVector3Normalize(rayDir);


	TestBlock* pTest =  (TestBlock*)Instantiate<TestBlock>(this);
	pTest->transform_.position_ = mousePosFront;
	pTest->SetDirection(rayDir);







	//引数データを、レイのデータとする。
	{
		//当たる判定	
			//スタート
		rayData->start = mousePosFront;	//マウス開始位置
		//方向
		rayData->dir = rayDir;	//マウスクリック方向へ、（マウス開始位置から、クリック方向へ（世界の終了まで））

		//当たり判定を実行
		//全ての魚オブジェクトとの衝突判定をおこなう
			//魚オブジェクトは、頂点数６つの２ポリゴンのみなので、重くはならないと思う。

		for (int i = 0; i < FISH_MAX; i++)
		{
			pFishBase[i]->RayCast(rayData);	//関数先で、レイのスタート地点、方向などをモデル事のTransform値によって変形するが、
											//当たり判定終了後に元のデータへ戻すので連続でほかのオブジェクトとの値り判定を行っても問題なし

			//衝突していたら、終了。
			if (rayData->hit)
			{
				//現在見ているオブジェクトをFISH型に変形して返す
				return (FISH)i;
			}
		}
	}


	//どれとも衝突しなかったとき、
	//MAX値を返す。（誰とも衝突しなかった判定）
	return FISH_MAX;


}

void FishMetaAI::SetComboBox(FISH fishType)
{
	//ダイアログプロシージャが呼ばれたときに処理させようとしたが、
	//ダイアログプロシージャは、ダイアログのコントロールに何かされたときに呼ばれるものなので、任意のタイミングで呼べるわけではない。
	{
	//	//魚のタイプセット
	//	currentFish = fishType;

	//	//ダイアログ（コンボボックス）をダイアログプロシージャにて変更を行うフラグを立てる
	//	changeComboBox = true;
	}







	//現在所有している情報
	//を、コンボボックスにセットする

	//どこかでそのデータを持っておく必要あり。


	//魚のタイプセット
	currentFish = fishType;


	//コンボボックスが現在選択している値を取得したい
	//ダイアログプロシージャにメッセージを送る

		//コントロールの選択。ボタンの選択の強制選択。
		//第１引数：（コントロールハンドル）コンボボックスのコントロールハンドル（それぞれボタンごとの番号）
		//第２引数：（メッセージ）コンボボックスの初期値を選択。
					//コンボボックスを開いたときに、あらかじめ選択されているもの
		//第３引数：（情報A）初期値とする、（選択している状態にする）値の番号（追加順で０から）
		//第４引数：（情報B）なし
	SendMessage(
		GetDlgItem(hDlg_, TYPE_COMBO),
		CB_SETCURSEL, fishType, 0);


	//上記の魚のオブジェクトに
	//カメラ（コントローラークラス）を移動させる
		//その際、自身から移動を行いたくないので、AjustmentInfoSceneクラスにその作業を頼む
		//×その際、自身クラスに持っておいた関数ポインタからその処理をする関数を呼び出す
		//AjustmentInfoSceneクラスのポインタから関数にアクセス
	pAjustmentInfoScne_->ChangeControllerPos(fishType);


	


	//優先度をセット
	//（メンバに持っている現在のデータを設定）
		//データから設定する文字列を取得し、それをもとに、コンボボックスの値を変更
	GetPriority(fishType);



	//高さをセット
	//（メンバに持っている現在のデータを設定）
		//データから設定する文字列を取得し、それをもとに、コンボボックスの値を変更
	GetHeight(fishType);



}

void FishMetaAI::GetHeight(FISH fishType)
{
	if (fishType == -1)
	{
		fishType = FISH_BLUE_FINTREVALLY;
	}

	//高さ
	//fishType順
		//文字列のポインタを取得し、
		// atofでfloat型に変形
	float height = atof(data_[FISH_HEIGHT_GROUND_ENV][fishType].c_str());

	

	SPAWN_HEIGHT spawnHeight = HEIGHT_0_POINT_0;

	//小数をswitchで判断に使用することができない
	{
		if (height == 0.0f) { spawnHeight = HEIGHT_0_POINT_0;  }
		else if (height == 0.1f) { spawnHeight = HEIGHT_0_POINT_1; }
		else if (height == 0.2f) { spawnHeight = HEIGHT_0_POINT_2; }
		else if (height == 0.3f) { spawnHeight = HEIGHT_0_POINT_3; }
		else if (height == 0.4f) { spawnHeight = HEIGHT_0_POINT_4; }
		else if (height == 0.5f) { spawnHeight = HEIGHT_0_POINT_5; }
		else if (height == 0.6f) { spawnHeight = HEIGHT_0_POINT_6; }
		else if (height == 0.7f) { spawnHeight = HEIGHT_0_POINT_7; }
		else if (height == 0.8f) { spawnHeight = HEIGHT_0_POINT_8; }
		else if (height == 0.9f) { spawnHeight = HEIGHT_0_POINT_9; }
		else if (height == 1.0f) { spawnHeight = HEIGHT_1_POINT_0; }
	}


	

	//優先度をセット
	SendMessage(
		GetDlgItem(hDlg_, HEIGHT_COMBO),
		CB_SETCURSEL, spawnHeight, 0);


}

void FishMetaAI::GetPriority(FISH fishType)
{
	if (fishType == -1)
	{
		fishType = FISH_BLUE_FINTREVALLY;
	}

	//COST
	//fishType順
	std::string priority = data_[FISH_COST_GROUND_ENV][fishType];

	//優先度が入っている文字列の先頭１バイトを取得
	char priorityCopy = priority[0];

	PRIORITY_RANK rank = D_RANK;


	switch (priorityCopy)
	{
	case 'S' : 
		rank = S_RANK;
		break;
	case 'A':
		rank = A_RANK;
		break;
	case 'B':
		rank = B_RANK;
		break;
	case 'C':
		rank = C_RANK;
		break;
	case 'D':
		rank = D_RANK;
		break;

	default:
		break;
	}

	//優先度をセット
	SendMessage(
		GetDlgItem(hDlg_, PRIORITY_COMBO),
		CB_SETCURSEL, rank, 0);


}
