#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Engine/GameObject.h"
#include "FishBase.h"

enum CSV_INFO
{
	FISH_TYPE_INFO,
	FISH_COST_GROUND_ENV,
	FISH_HEIGHT_GROUND_ENV,

	CSV_MAX,
	

};


//魚の情報をもろもろ管理するクラス
//魚にアクセスし、ゲームシーン内での操作を指揮させる

//またCSV情報を管理するクラス


//ダイアログのコンボボックスに
	//登録順で宣言
enum FISH
{
	FISH_BLUE_FINTREVALLY,
	FISH_BLUE_HEADTILE_FISH,
	FISH_BLUE_TANG,
	FISH_BUTTERFLY_FISH,
	FISH_CROWN_FISH,
	FISH_MOORISHIDOL,
	FISH_RAY_FISH,
	FISH_SCOLOPSIS_BILINEAT,
	FISH_SPOTTED_GARDEN_EEL,
	FISH_WHALE_SHARK,

	FISH_MAX,

};



enum PRIORITY_RANK
{
	S_RANK,
	A_RANK,
	B_RANK,
	C_RANK,
	D_RANK,

	PRIORITY_MAX,
};

enum SPAWN_HEIGHT
{
	HEIGHT_0_POINT_0 ,
	HEIGHT_0_POINT_1,
	HEIGHT_0_POINT_2,
	HEIGHT_0_POINT_3,
	HEIGHT_0_POINT_4,
	HEIGHT_0_POINT_5,
	HEIGHT_0_POINT_6,
	HEIGHT_0_POINT_7,
	HEIGHT_0_POINT_8,
	HEIGHT_0_POINT_9,
	HEIGHT_1_POINT_0,

	HEIGHT_MAX,

};


class CsvReader;
class AjustmentInfoScene;



//■■シーンを管理するクラス
class FishMetaAI : public GameObject
{
	//ダイアログハンドル
	HWND hDlg_;

	//地面番号取得
	int groundNumber_;



	//魚オブジェクト
	//FishBaseのポインタを複数個数取得
		//FishBase* p[10]	FishBaseのポインタを１０個
		//FishBase* p = new FishBase[10];	FishBaseの１つのポインタに先頭アドレスを持たせて、１０個の要素を持つ配列を確保
	FishBase* pFishBase[FISH_MAX];


	//CSVファイル読み込み
	CsvReader* pCsvReader_;
		//CSVファイルのデータを、CSVに持っているので、
		//その情報を、最終的に保存するようにすれば、簡易的にファイルかも可能では？


	//std::vector<std::string> data_;
		//上記で一行分の文字列を表せる
	//そのため、１単語を一要素として、
		//カンマ,　までの単語を１要素に取得。そして、その単語（文字列）を書き換えていく。
		//GetStringにて一行から取得。
		//CSVファイルの書き込みルールを参照して、一行からデータを取得していく。。


	//文字列データ
		//CSVファイルごと、書き換え中のデータを所有しておく
		//地面番号を取得するので、その地面番号から、CSVファイル内の必要部分を選択し、そのデータ分だけ取得
	std::vector<std::vector<std::string>> data_;
	//データを書き換えるにはどうするか？
		//strcpy , memcpy　などを使って、書き換えたい場所のアドレスを取得して、
				//その部分のアドレスを書き換える。
		//だが、書き換えるとき、
				//データを壊しかねないので、慎重に



	


	//レイ発射
	//戻値：衝突したオブジェクトの番号（enum宣言時の番号）
	FISH ShotRay(RayCastData* rayData);

	//コンボボックスをセットする
	void SetComboBox(FISH fishType);

	void GetHeight(FISH fishType);
	void SetHeight(FISH fishType , SPAWN_HEIGHT currentHeight);

	void GetPriority(FISH fishType);
	void SetPriority(FISH fishType , PRIORITY_RANK currentPriority);

	//現在見ている魚
	FISH currentFish;
	//コンボボックスの値を変更するかのフラグ（Windowsプログラム外で、ダイアログプロシージャを呼ぶことが不可能なので、
											//フラグを立てておき、そのフラグが立っているとき、ダイアログプロシージャ内で操作を行う。）
	bool changeComboBox;



	/*
	//関数のポインタ
	//親クラスのコントローラークラスの位置を変更させる関数のポインタ
		//本来であれば、親クラスをインクルードして関数にアクセスすればよいのだが、、余計な情報を持たせないために関数のポインタだけもらうようにする。（それに、親が必ずしも、その関数を持っているとも限らない。）
	//AjustmentInfoScene::のchangePosFunc　というポインタ名で
	//返り値型はvoid 
	//引数群は　int 
	void(AjustmentInfoScene::*changePosFunc_)(int);
		//このポインタへ,関数のポインタを受け取れば、
		//後にそのポインタから関数を呼び出すことが可能
		//関数を呼び出すとき、func() と呼び出すが、　func という名前だけ書くとポインタになるように、
			//changePosFuncだけではポインタなので、 changePosFunc()とすれば、関数呼び出しになる。
	*/

	AjustmentInfoScene* pAjustmentInfoScne_;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	FishMetaAI(GameObject* parent);

	~FishMetaAI();



	//初期化
	void Initialize() override;

	void GetCsvString();

	void GetCurrentGroundNum();

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;




	//ダイアログハンドルを取得し、
		//ダイアログのコンボボックスの初期化を行う
	//初期化を行う関数
	void InitDialog(HWND hDlg);


	//偽物
	//ダイアログプロシージャもどき
	//MainCppが本物
	BOOL DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

	

	//マップの配置情報を保存
		//テキストエディタとして保存
	BOOL MapSave();

	void SaveHeight();

	void GetMyHeightData(int* counter, int charSize, char  str[]);

	void SavePriority();


	//引数：書き込み全データ（char*（要素を動的確保済み））
	//引数：書き込み全データに書き込んだデータバイト数（書き込んだ要素数になる）
	//引数：自身のメンバの持っているデータ配列
	//引数：自身のメンバの持っているデータ配列のデータバイト数
	//引数：CSVの元データにアクセスするためのポインタ
	void GetAllPriorityCsvData(char * writeWrite, int* counter, 
		char  str[], int charSize, std::vector<std::vector<std::string>> * csvData);

	//引数：書き込み全データ（char*（要素を動的確保済み））
	//引数：書き込み全データに書き込んだデータバイト数（書き込んだ要素数になる）
	//引数：自身のメンバの持っているデータ配列
	//引数：自身のメンバの持っているデータ配列のデータバイト数
	//引数：CSVの元データにアクセスするためのポインタ
	void GetAllHeightCsvData(char * writeWrite, int* allDataCounter,
		char  str[], int myDataCounter, std::vector<std::vector<std::string>> * csvData);


	void AddChar(char* writeWrite, int* counter, char addChar);



	void GetMyPriorityData(char  str[], int charSize);

	//マップのロード
		//テキストエディタから読み込んで、マップの情報を更新する。
	BOOL MapOpen();

	//ファイルに名前を付けて保存
	BOOL MapSaveAs();


	/*
	//コントローラーの位置を変更させる関数
	//その関数のポインタを取得する
		//AjustmentInfoSceneクラスの関数が、これに当たるので、そのポインタをもらって、アクセスする
	//引数：関数ポインタ　（戻り値型void (関数ポインタを受け取るポインタ)(関数ポインタの引数型（引数2つの時2つ書く）)）
				//関数呼び出し時は、func()という関数ならば、funcと入れるだけ、　（関数の引数（）なしは、ポインタになる）
	void SetFunctionPointa(void(AjustmentInfoScene::*functionPointa)(int));
	*/

	//Controllerの位置を変更させる関数を持つクラスの取得
	void SetAjustmentInfoScene(AjustmentInfoScene* pPointa);


};
