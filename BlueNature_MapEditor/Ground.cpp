#include "Ground.h"
#include "Engine/Model.h"






//コンストラクタ
Ground::Ground(GameObject * parent)
	: GameObject(parent, "Ground"),
	hModel_(-1)
{
}

//初期化
void Ground::Initialize()
{
	hModel_ = Model::Load("Assets/CommonGround/Ground2.fbx");
	assert(hModel_ >= 0);

	transform_.rotate_.vecX = -90;
	transform_.scale_ = XMVectorSet(100 , 100 , 100, 0);
	transform_.position_.vecY = -40;

	


}

//更新
void Ground::Update()
{
}

//描画
void Ground::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void Ground::Release()
{
}