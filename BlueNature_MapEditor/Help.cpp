#include "Help.h"
#include "Engine/Image.h"

//コンストラクタ
Help::Help(GameObject * parent)
	: GameObject(parent, "Help"),
	hImage_(-1)
{
}

//初期化
void Help::Initialize()
{
	std::string fileName = "Assets/2DTexture/Help.jpg";
	

	hImage_ = Image::Load(fileName);
	assert(hImage_ > -1);	//-1より大きいはずだ


	transform_.position_ = XMVectorSet(1.0f - 0.13f, -1.0f + 0.15f, 0, 0);
	transform_.scale_ = XMVectorSet(0.2f, 0.2f, 0.1f, 0);


}

//更新
void Help::Update()
{



}

//描画
void Help::Draw()
{
	Image::SetTransform(hImage_, transform_);
	Image::Draw(hImage_);

}

//開放
void Help::Release()
{
}