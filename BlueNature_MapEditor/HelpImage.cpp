#include "HelpImage.h"
#include "Engine/Image.h"

//コンストラクタ
HelpImage::HelpImage(GameObject * parent)
	: GameObject(parent, "HelpImage"),
	hImage_(-1)
{
}

void HelpImage::Load(std::string fileName)
{

	hImage_ = Image::Load(fileName);
	assert(hImage_ > -1);	//-1より大きいはずだ

	//画像は、画面サイズに拡大しているので、
	//サイズ、位置調整の必要なし



}

//初期化
void HelpImage::Initialize()
{



}

//更新
void HelpImage::Update()
{



}

//描画
void HelpImage::Draw()
{
	//まだロードしていない時は行わない
	if (hImage_ != -1)
	{
		Image::SetTransform(hImage_, transform_);
		Image::Draw(hImage_);
	}
}

//開放
void HelpImage::Release()
{
}