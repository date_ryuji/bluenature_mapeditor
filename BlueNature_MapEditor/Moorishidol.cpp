#include "Moorishidol.h"

//コンストラクタ
Moorishidol::Moorishidol(GameObject * parent)
	: FishBase::FishBase(parent, "Moorishidol")
{


}
Moorishidol::~Moorishidol()
{
	//３Dポリゴン用の動的確保したポインタの解放
	BaseRelease();
}

//初期化
void Moorishidol::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();


}

//更新
void Moorishidol::Update()
{
}

//開放
void Moorishidol::Release()
{
}