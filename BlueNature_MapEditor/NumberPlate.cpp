#include "NumberPlate.h"
#include "Engine/Image.h"

//コンストラクタ
NumberPlate::NumberPlate(GameObject * parent)
	: GameObject(parent, "NumberPlate"),
	hImage_(-1)
{
}

//初期化
void NumberPlate::Initialize()
{
	//地面番号の取得
	int groundNumber = -1;
	{
		HANDLE hFile;        //ファイルのハンドル
		hFile = CreateFile(
			"Assets/currentGroundNumber.txt",                 //ファイル名
			GENERIC_READ,           //アクセスモード（読み込み用）
			0,                      //共有（なし）
			NULL,                   //セキュリティ属性（継承しない）
			OPEN_EXISTING,           //作成方法	//ファイルを開く（存在してなければエラー）
			FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
			NULL);                  //拡張属性（なし）


		//ファイルのサイズを取得
			//１文字１バイトなので、そのサイズ分サイズを取得して、（バイトの数を取得）
		DWORD fileSize = GetFileSize(hFile, NULL);

		//読み込んだデータを入れる配列
		//ファイルのサイズ分メモリを確保
			//上記で取得したバイト分、charポインタの配列で取得
		char* data;
		data = new char[fileSize];

		DWORD dwBytes = 0; //読み込み位置

		//ファイル読み込み
		ReadFile(
			hFile,     //ファイルハンドル
			data,      //データを入れる変数
			fileSize,  //読み込むサイズ
			&dwBytes,  //読み込んだサイズ
			NULL);     //オーバーラップド構造体（今回は使わない）


		//ファイルの読み込みはもうしないので閉じる
		CloseHandle(hFile);




		//取得した文字を数値に変換
		groundNumber = atoi(data);
	}
	if (groundNumber < 0 || groundNumber > 8)
	{
		return;
	
	}

	

	//地面番号の値をファイルのパスにする
	std::string fileName = "Assets/Ground/GroundTex" + std::to_string(groundNumber) + ".jpg";


	hImage_ = Image::Load(fileName);
	assert(hImage_ > -1);	//-1より大きいはずだ


	//位置、拡大率調整
		//移動値は、０，０，０が画面の中心
		//x値　１移動によって　画面の右端に移動
		//x値　ー１移動によって　画面の左端に移動

		//つまり、画面の端に、画像の端をくっつけるように移動させる方法
			//右端に１移動（画像の中心点が画面の右端にくっつくように
			//そのうえで、画像の横幅分、左に移動させる

		//画像の右端が画面の右端とそろうように移動したいのであれば、
			//左に移動する、その移動量（ワールド座標）が分かればよい。

		
		
	transform_.position_ = XMVectorSet(1.0f - 0.13f, 1.0f - 0.15f, 0, 0);
	transform_.scale_ = XMVectorSet(0.2f, 0.2f, 0.1f, 0);


}

//更新
void NumberPlate::Update()
{



}

//描画
void NumberPlate::Draw()
{
	Image::SetTransform(hImage_, transform_);
	Image::Draw(hImage_);

}

//開放
void NumberPlate::Release()
{
}