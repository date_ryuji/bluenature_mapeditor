#include "RayFish.h"

//コンストラクタ
RayFish::RayFish(GameObject * parent)
	: FishBase::FishBase(parent, "RayFish")
{


}
RayFish::~RayFish()
{
	//３Dポリゴン用の動的確保したポインタの解放
	BaseRelease();
}

//初期化
void RayFish::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();



}

//更新
void RayFish::Update()
{
}

//開放
void RayFish::Release()
{
}