#include "Scolopsisbilineat.h"

//コンストラクタ
Scolopsisbilineat::Scolopsisbilineat(GameObject * parent)
	: FishBase::FishBase(parent, "Scolopsisbilineat")
{


}
Scolopsisbilineat::~Scolopsisbilineat()
{
	//３Dポリゴン用の動的確保したポインタの解放
	BaseRelease();
}

//初期化
void Scolopsisbilineat::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();



}

//更新
void Scolopsisbilineat::Update()
{
}

//開放
void Scolopsisbilineat::Release()
{

}