#pragma once
#include "FishBase.h"

//■■シーンを管理するクラス
class Scolopsisbilineat : public FishBase
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Scolopsisbilineat(GameObject* parent);
	~Scolopsisbilineat() override;

	//初期化
	void Initialize() override;

	//更新
	void Update() override;


	//開放
	void Release() override;
};