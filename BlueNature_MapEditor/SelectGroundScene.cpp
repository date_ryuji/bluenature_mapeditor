#include "SelectGroundScene.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"	//シーン切り替えを行う
#include "Engine/Camera.h"

#include "Controller.h"


#include "WholeMap.h"

#include "Help.h"
#include "HelpImage.h"





//コンストラクタ
SelectGroundScene::SelectGroundScene(GameObject * parent)
	: GameObject(parent, "GroundSelectScene"),
	currentFlag(true),
	pWholeMap_(nullptr),
	pController_(nullptr),
	pHelp_(nullptr),
	pHelpImage_(nullptr)
{
}

//初期化
void SelectGroundScene::Initialize()
{
	//Camera::SetPosition(0.0f, 10.0f, 0.0f);
	//Camera::SetTarget(0.0f, 10.0f, 0.0f);


	pWholeMap_ = (WholeMap*)Instantiate<WholeMap>(this);
	pController_ = (Controller*)Instantiate<Controller>(this);
	pController_->transform_.position_ = XMVectorSet(0, -0.2f, 5, 0);
	pController_->SetMaxXAndMinX(0, 0);	//max値が０min値が０＝実質移動不可


	//pController->transform_.position_ = XMVectorSet(0, 10, 0,0);
	//pController->transform_.rotate_ = XMVectorSet(90, 0, 0, 0);

	pHelp_ = (Help*)Instantiate<Help>(this);
	pHelpImage_ = (HelpImage*)Instantiate<HelpImage>(this);
	//画像のロード（クラスは共通で、ほかのシーンでも使用したいので、引数にてロードファイルを変更する）
	pHelpImage_->Load("Assets/2DTexture/GroundSceneInfo_1.jpg");
	//フラグの上下の設定
		//フラグを下す
	ChangeStatus(false, pHelpImage_);



}

void SelectGroundScene::ChangeStatus(bool flag , GameObject* pGameObject)
{
	if (flag)
	{
		pGameObject->EnableUpdate();
		pGameObject->EnableDraw();
	}
	else
	{

		pGameObject->DisableUpdate();
		pGameObject->DisableDraw();
	}
}

//更新
void SelectGroundScene::Update()
{
	if (currentFlag)
	{
		if (Input::IsMouseButtonDown(0))
		{
			//地面プレートとの当たり判定
			int groundNumber = -1;	//０オリジン

			groundNumber = pWholeMap_->RayCastGround();


			if (groundNumber != -1 && groundNumber != 9)
			{
				//当たった地面プレートの番号をテキストで保存
				//次のシーンで、どの地面の情報化を判断するときは、
					//そのテキストを読み込む
				SaveCurrentGround(groundNumber);


				SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
				pSceneManager->ChangeScene(SCENE_ID_AJUST_INFO);
			}


		}
	}

	if (Input::IsKeyDown(DIK_H))
	{
		//子供のHelpImage以外のオブジェクトの、
		//Update , Drawのフラグを下すOR上げる
		ChangeChildStatus();


	}



}

void SelectGroundScene::SaveCurrentGround(int groundNumber)
{
	//ファイルを作成
		//からのファイルを作り、　hfileにハンドルを入れる。
	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		"Assets/currentGroundNumber.txt",                 //ファイル名
		GENERIC_WRITE,           //アクセスモード（書き込み用）
									//開くのか、書き込むのかGENERIC_WRITE
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		CREATE_ALWAYS,           //作成方法
									/*
									・作成方法

										〇新しくファイルを作る（同名のファイルがあるとエラー）：CREATE_NEW （新規保存）

										〇新しくファイルを作る（同名のファイルがあると上書き）：CREATE_ALWAYS (上書き保存)

										□ファイルを開く（その名前のファイルがなければエラー）：OPEN_EXISTING （）

										□ファイルを開く（その名前のファイルがなければ作る）   ：OPEN_ALWAYS


									*/

		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）



	//文字列　string = const char*

	//書き込み用の文字列の作成

	std::string str = std::to_string(groundNumber);


	//ファイルにデータを書き込む
//そのどこから保存するかの、位置
	DWORD dwBytes = 0;  //書き込み位置（０＝一番頭から）
	WriteFile(
		hFile,						//ファイルハンドル

		//const char*型で、データを取得するので、
			//文字列をconstchar*型に変換、そのための関数c_str()
		str.c_str(),						//保存するデータ（文字列）
		//const char*型に変換し、
			//文字列を取得する関数に送り返り値で文字サイズを取得
		(DWORD)std::strlen(str.c_str()),		//書き込む文字数(サイズ)；文字数を出す関数＝strlen
		&dwBytes,                //書き込んだサイズを入れる変数(頭から保存して、dwBytesに文字数、書き込んだデータのサイズが足されるので、　dwBytesには、次の書き込む位置が入ることになる。
									//次に同じ関数を呼んでも、書き込み位置はいじらずとも追記できるようになる。)
		NULL);                   //オーバーラップド構造体（今回は使わない）


	//ハンドル閉じる
	CloseHandle(hFile);



}

//描画
void SelectGroundScene::Draw()
{
}

//開放
void SelectGroundScene::Release()
{
}

void SelectGroundScene::ChangeChildStatus()
{
	ChangeStatus(currentFlag, pHelpImage_);

	if (currentFlag)
	{
		currentFlag = false;
	}
	else
	{
		currentFlag = true;
	}

	ChangeStatus(currentFlag, pWholeMap_);
	ChangeStatus(currentFlag, pController_);
	ChangeStatus(currentFlag, pHelp_);
	
}
