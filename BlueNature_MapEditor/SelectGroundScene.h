#pragma once
#include <windows.h>
#include "Engine/GameObject.h"



class WholeMap;
class Controller;
class Help;
class HelpImage;

//■■シーンを管理するクラス
class SelectGroundScene : public GameObject
{

	WholeMap* pWholeMap_;
	Controller* pController_;
	Help* pHelp_;
	HelpImage* pHelpImage_;


	bool currentFlag;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SelectGroundScene(GameObject* parent);

	//初期化
	void Initialize() override;


	//更新
	void Update() override;

	void SaveCurrentGround(int groundNumber);

	//描画
	void Draw() override;

	//開放
	void Release() override;



	//シーン内の子供オブジェクトの、
	//Update、Drawのフラグを下す、上げる
	void ChangeChildStatus();

	void ChangeStatus(bool flag , GameObject* pGameObject);



};