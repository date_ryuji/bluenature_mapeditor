#include "SpottedGardenEel.h"

//コンストラクタ
SpottedGardenEel::SpottedGardenEel(GameObject * parent)
	: FishBase::FishBase(parent, "SpottedGardenEel")
{


}
SpottedGardenEel::~SpottedGardenEel()
{
	//３Dポリゴン用の動的確保したポインタの解放
	BaseRelease();
}

//初期化
void SpottedGardenEel::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();



}

//更新
void SpottedGardenEel::Update()
{
}

//開放
void SpottedGardenEel::Release()
{
}