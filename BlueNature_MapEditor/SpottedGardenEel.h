#pragma once

#include "FishBase.h"

//■■シーンを管理するクラス
class SpottedGardenEel : public FishBase
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SpottedGardenEel(GameObject* parent);
	~SpottedGardenEel() override;


	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//解放
	void Release() override;
};