#include "TestBlock.h"
#include "Engine/Model.h"

//コンストラクタ
TestBlock::TestBlock(GameObject * parent)
	: GameObject(parent, "TestBlock"),
	hModel_(-1),
	direction_(XMVectorSet(0,0,1,0))
{
}

//初期化
void TestBlock::Initialize()
{
	hModel_ = Model::Load("Assets/3DModel/Fish/fish_Base.fbx");
	assert(hModel_ > -1);

	transform_.scale_ = XMVectorSet(0.2f, 0.2f, 0.2f, 0);
	//transform_.rotate_ = XMVectorSet(0, -90, 0, 0);
	//transform_.position_ = XMVectorSet(0, 10.0f, 0, 0);

}

//更新
void TestBlock::Update()
{
	direction_ = XMVector3Normalize(direction_);
	transform_.position_ += direction_ * 0.5f;
	

	if (transform_.position_.vecZ > 10)
	{
		KillMe();
	}

}

//描画
void TestBlock::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);

}

//開放
void TestBlock::Release()
{
}

void TestBlock::SetDirection(XMVECTOR direction)
{
	direction_ = direction;
}
