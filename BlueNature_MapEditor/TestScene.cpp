#include "TestScene.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"	//シーン切り替えを行う


//コンストラクタ
TestScene::TestScene(GameObject * parent)
	: GameObject(parent, "TestScene")
{
}

//初期化
void TestScene::Initialize()
{

}

//更新
void TestScene::Update()
{



}

//描画
void TestScene::Draw()
{
}

//開放
void TestScene::Release()
{
}