#include "WhaleShark.h"

//コンストラクタ
WhaleShark::WhaleShark(GameObject * parent)
	: FishBase::FishBase(parent, "WhaleShark")
{


}
WhaleShark::~WhaleShark()
{
	//３Dポリゴン用の動的確保したポインタの解放
	BaseRelease();
}

//初期化
void WhaleShark::Initialize()
{
	//３Dポリゴンの初期化
	InitQuad();




}

//更新
void WhaleShark::Update()
{
}

//開放
void WhaleShark::Release()
{

}