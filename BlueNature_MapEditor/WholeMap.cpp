#include "WholeMap.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"

#include "Controller.h"

//コンストラクタ
WholeMap::WholeMap(GameObject * parent)
	: GameObject(parent, "WholeMap")
{
	for (int i = 0; i < G_MAX; i++)
	{
		hModel_[i] = -1;
	}

}

//初期化
void WholeMap::Initialize()
{
	//ファイル名宣言
	std::string fileName[G_MAX] =
	{
		"Assets/Ground/Ground0.fbx",
		"Assets/Ground/Ground1.fbx",
		"Assets/Ground/Ground2.fbx",
		"Assets/Ground/Ground3.fbx",
		"Assets/Ground/Ground4.fbx",
		"Assets/Ground/Ground5.fbx",
		"Assets/Ground/Ground6.fbx",
		"Assets/Ground/Ground7.fbx",
		"Assets/Ground/Ground8.fbx",
		"Assets/Ground/Ground9.fbx",

	};

	Transform trans;
	

	//ロード
	for (int i = 0; i < G_MAX; i++)
	{
		hModel_[i] = Model::Load(fileName[i]);
		assert(hModel_[i] > -1);	//−１より大きいはず

	}

	


}

//更新
void WholeMap::Update()
{



}

//描画
void WholeMap::Draw()
{
	int x = 0;
	int y = 1;

	//全モデルの描画
	for (int i = 0; i < G_MAX - 1; i++)
	{
		Transform trans;
		trans.rotate_.vecX = 90;
		trans.rotate_.vecY = 180;

		if (i % 3 == 0)
		{
			x = 0;
			y--;
		}
		trans.position_.vecX = (float)x;
		trans.position_.vecY = (float)y;

		x++;



		//Transformの計算をし直す必要あり
		trans.Calclation();

		Model::SetTransform(hModel_[i] , trans);
		Model::Draw(hModel_[i]);

	}


}

//開放
void WholeMap::Release()
{
}

int WholeMap::RayCastGround()
{
	//２次元から、３次元へ直すための行列を取得する。

	//ビューポート行列
		//ビューポート行列は、これまで、Direct３Dに作ってもらっていたため、
		//どこにも存在していない状態なので、作成する。
	float w = Direct3D::scrWidth / 2.0f;
	float h = Direct3D::scrHeight / 2.0f;
	XMMATRIX vp = {
		w, 0, 0, 0,
		0, -h, 0, 0,
		0, 0, 1, 0,
		w, h, 0, 1
	};
	//プロジェクション行列、ビューはカメラから

	//逆行列を作成
	//ビューポート行列の逆行列
	XMMATRIX invVp = XMMatrixInverse(nullptr, vp);
	//プロジェクション行列の逆行列
	XMMATRIX invPro = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
	//ビュー行列の逆行列
	XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());

	//ベクトル
	XMVECTOR mousePosFront = Input::GetMousePosition();
	mousePosFront.vecZ = 0;
	XMVECTOR mousePosBack = Input::GetMousePosition();
	mousePosBack.vecZ = 1;

	//３つの行列で変換することで、
	//ワールドの座標とする。
		//ビューポートからワールドへ
	mousePosFront = XMVector3TransformCoord(
		mousePosFront,
		invVp * invPro * invView
	);

	mousePosBack = XMVector3TransformCoord(
		mousePosBack,
		invVp * invPro * invView
	);


	//ワールドにおける座標が出せたので、
		//レイの方向を、
		//マウス開始位置から終了位置までに延ばす。
	XMVECTOR rayDir = mousePosBack - mousePosFront;
	//XMVECTOR rayDir = mousePosFront - mousePosBack;
	//正規化
	rayDir = XMVector3Normalize(rayDir);


	RayCastData rayData;

	//当たる判定	
	//スタート
	rayData.start = mousePosFront;	//マウス開始位置
	rayData.dir = rayDir;	//マウスクリック方向へ、（マウス開始位置から、クリック方向へ（世界の終了まで））
	

	for (int i = 0; i < G_MAX - 1; i++)
	{
		Model::RayCast(hModel_[i] , &rayData);
		if (rayData.hit)
		{
			return i;
		}
	}

	//誰にも当たらなかった。
	return -1;

}
