#pragma once
#include "Engine/GameObject.h"


//■■シーンを管理するクラス
class WholeMap : public GameObject
{
	enum GROUND
	{
		G0,
		G1,
		G2,
		G3,
		G4,
		G5,
		G6,
		G7,
		G8,
		G9,

		G_MAX,
	};

	int hModel_[G_MAX];

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	WholeMap(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//当たり判定実行
		//返り値に、あたった地面の番号を返す。
	int RayCastGround();

};