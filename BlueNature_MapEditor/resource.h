//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// MapEditor.rc で使用
//
#define IDD_DIALOG1                     101
#define IDR_MENU1                       103
#define FISH_INFO                       105
#define RADIO_UP                        1002
#define RADIO_DOWN                      1003
#define RadioChangeType                 1004
#define RADIO_TYPE                      1004
#define IDC_COMBO1                      1005
#define TYPE_COMBO                      1005
#define COMBO                           1006
#define FISH_TYPE                       1007
#define FISH_PRIORITY                   1008
#define PRIORITY_COMBO                  1009
#define FISH_HEIGHT                     1010
#define HEIGHT_                         1011
#define HEIGHT_COMBO                    1011
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_40003                        40003
#define ID_40004                        40004
#define ID_MENUE_NEW                    40005
#define ID_MENUE_OPEN                   40006
#define ID_MENUE_SAVE                   40007
#define ID_40008                        40008
#define ID_MENU_NAMESAVE                40009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40010
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
